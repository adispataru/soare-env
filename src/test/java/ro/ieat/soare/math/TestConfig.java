package ro.ieat.soare.math;

/**
 * Created by adispataru on 4/15/2016.
 */
//@Configuration
//@EnableAutoConfiguration
//@ComponentScan({"ro.ieat.soare.playing", "ro.ieat.soare.coalitions", "ro.ieat.soare.repositories", "ro.ieat.soare.scheduling", "ro.ieat.soare"})
public class TestConfig{

    private static final int numberOfResources = 10;
    public static final int historySize = 4;

//    @Bean
//    Environment env(){
//        return Environment.initializeIfEmpty().assignErrorJournal();
//    }
//
////    @Bean
////    EventBus createEventBus(Environment env){
////        return EventBus.create(env, Environment.THREAD_POOL);
////    }
//
//    @Autowired
//    private InitialTaskUsagePublisher publisher;
//
////    @Autowired
////    private CoalitionReceiver coalitionReceiver;
//
//    @Autowired
//    private CoalitionClassifier coalitionClassifier;
//
//    public void run(String... strings) throws Exception {

//        eventBus.on($("Resources"), receiver);
//        eventBus.on($("ro.ieat.soare.coalitions"), coalitionReceiver);
//        coalitionClassifier.instantiateClassifier(2700L);
//        publisher.publishInitialTaskUsage(2700L);
//        for(long t = 3300; t < 6300; t+= 300) {
//            System.out.println("Time " + t);
//            publisher.publishInitialTaskUsage(t);
//
//        publisher.scheduleJobs(3000L);


//    }

//    public static void main(String[] args) throws InterruptedException {
//        ApplicationContext app = SpringApplication.run(TestConfig.class, args);
//        //app.getBean(CountDownLatch.class).await(1, TimeUnit.SECONDS);
//        app.getBean(Environment.class).shutdown();
//    }

}
