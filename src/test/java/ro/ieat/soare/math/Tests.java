package ro.ieat.soare.math;

import org.junit.Test;
import org.springframework.context.annotation.Profile;
import ro.ieat.soare.math.statistics.Histogram;
import ro.ieat.soare.math.statistics.distance.DistributionDistance;
import ro.ieat.soare.math.statistics.distance.PropertyDistance;
import ro.ieat.soso.core.coalitions.Resource;

import java.util.Map;
import java.util.TreeMap;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by adispataru on 6/2/2016.
 */
//@RunWith(SpringJUnit4ClassRunner.class)
//@SpringApplicationConfiguration(TestConfig.class)
//@ContextConfiguration(classes = {TestConfig.class})
@Profile("test")
public class Tests {


    @Test
    public void testBhattacharyyaDistance(){
        Histogram<Long> h = new Histogram<>();
        Histogram<Long> h2 = new Histogram<>();
        h2.add(4L);
        h.add(1L);
        h.add(1L);
        h.add(1L);
        h.add(1L);
        h.add(2L);
        h.add(2L);
        h.add(3L);
        h.add(3L);
        Map<Long, Double> dist = h.getProbabilityDistribution();
        Map<Long, Double> dist2 = h2.getProbabilityDistribution();

        Double self = DistributionDistance.computeBhattacharyyaDistance(dist, dist);
        Double selfKL = DistributionDistance.computeKLDivergence(dist, dist);
        Double selfSimilarity = applyGaussianKernel(self);
        assertTrue("Self distance must be 0", self < 0.000000001);
        assertTrue("Self distance must be 0", selfKL < 0.000000001);
        System.out.println("Distribution self-distance " + self);
        System.out.println("Distribution self-similarity " + selfSimilarity);
        System.out.println("Distribution self-distance KL " + selfKL);

        Double disjoint = DistributionDistance.computeBhattacharyyaDistance(dist, dist2);
        Double disjointKL = DistributionDistance.computeKLDivergence(dist, dist2);
        Double disjointSimilarity = applyGaussianKernel(disjoint);
        System.out.println("Disjoint Distribution distance " + disjoint);
        System.out.println("Disjoint Distribution similarity " + disjointSimilarity);
        System.out.println("Disjoint Distribution distance KL " + disjointKL);
        assertTrue("Disjoint distance must be infinite", disjoint.isInfinite());
        assertTrue("Disjoint distance must be infinite", disjointKL.isInfinite());

        Map<Long, Histogram<Long>> trainingSet = new TreeMap<>();
        trainingSet.put(1L, h);
        trainingSet.put(2L, h2);

        h = new Histogram<>();
        h.add(1L);
        h.add(2L);
        h2 = new Histogram<>();
        h2.add(1L);
        h2.add(2L);
        h2.add(3L);
        h2.add(3L);
        trainingSet.put(3L, h);
        trainingSet.put(4L, h2);
        Double half = DistributionDistance.computeBhattacharyyaDistance(h.getProbabilityDistribution(), h2.getProbabilityDistribution());
        Double halfKL = DistributionDistance.computeKLDivergence(h.getProbabilityDistribution(), h2.getProbabilityDistribution());
        Double halfSimilarity = applyGaussianKernel(half);
        Double halfKLSimilarity = applyGaussianKernel(halfKL);
        System.out.println("Half Distribution distance " + half);
        System.out.println("Half Distribution similarity " + halfSimilarity);
        System.out.println("Half Distribution distance KL " + halfKL);
        System.out.println("Half Distribution similarity KL " + halfKLSimilarity);


    }

    private Double applyGaussianKernel(Double half) {
        double sigma = 0.6;
        return Math.exp(-1 * (Math.pow(half, 2) / Math.pow(sigma, 2)));
    }

    @Test
    public void testResourcePropertiesSimilarity(){
        Resource m1 = new Resource(1, 0.5, 0.5, 21);
        Resource m2 = new Resource(1, 0.25, 0.5, 22);
        Resource m3 = new Resource(1, 0.25, 0.25, 23);
        Resource m4 = new Resource(1, 0.5, 0.75, 24);

        System.out.println("Self- similarity" + applyGaussianKernel(PropertyDistance.computePropertiesDistance(m1, m1)));
        System.out.println("Close similarity" + applyGaussianKernel(PropertyDistance.computePropertiesDistance(m1, m2)));
        System.out.println("Far similarity" + applyGaussianKernel(PropertyDistance.computePropertiesDistance(m1, m3)));
        System.out.println("Self distance" + PropertyDistance.computePropertiesDistance(m4, m4));
    }


}
