package ie.ucc.cl.sosm;

import ie.ucc.cl.sosm.components.PRouter;
import ie.ucc.cl.sosm.components.SOSMFramework;
import ie.ucc.cl.sosm.model.ResourceOffer;
import ie.ucc.cl.sosm.model.metrics.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.ieat.soare.io.Util;
import ro.ieat.soare.math.statistics.Histogram;
import ro.ieat.soare.repositories.JobRepository;
import ro.ieat.soare.repositories.ResourceRepository;
import ro.ieat.soare.repositories.util.DataAggregator;
import ro.ieat.soso.core.coalitions.Resource;
import ro.ieat.soso.core.config.Configuration;
import ro.ieat.soso.core.jobs.Job;
import ro.ieat.soso.core.jobs.TaskConstraint;
import ro.ieat.soso.core.jobs.TaskHistory;
import ro.ieat.soso.core.jobs.TaskUsage;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Logger;

/**
 * Created by adispataru on 04-Aug-16.
 */
@Service
public class SOSMExperiments {

    @Autowired
    ResourceRepository resourceRepository;

    @Autowired
    DataAggregator aggregator;

    @Autowired
    JobRepository jobRepository;

    // TUNABLE PARAMETERS
    private double[] weights = {1, 1, 1, 1};

    private static final Logger LOG = Logger.getLogger("SOSM Experiment");


    public void experiment(){
        Long endTime = 2700L;
        Long finalTime = 6300L;
        List<IMetric> frameworkMetrics = new ArrayList<>();
        frameworkMetrics.add(new TaskThroughput());
        frameworkMetrics.add(new PowerEfficiency(1, 0.1));
        frameworkMetrics.add(new ComputationalEfficiency());
        frameworkMetrics.add(new ManagementEfficiency());
        LOG.info("Bootstrapping SOSM");
        SOSMFramework framework = bootStrapSOSM(endTime, frameworkMetrics);
        LOG.info("Done.");


        LOG.info("Updating telemetry");
        List<TaskUsage> taskUsageList = aggregator.computeTasksUsage((endTime - Configuration.STEP) * Configuration.TIME_DIVISOR ,
                endTime * Configuration.TIME_DIVISOR);
        taskUsageList.forEach(t -> SOSMFramework.telemetry.put(t.getResourceId(), t));
        LOG.info("Done.");

        LOG.info("Updating weights, metrics, capacities, attributes");
        framework.setWeights(weights);
        framework.updateMetrics();
        framework.updateCapacities();
        framework.updateAttributes();
        Util.writeSOSMFrameworkMetrics(endTime, framework);
        LOG.info("Done.");




        endTime = endTime + Configuration.STEP;
        Map<Long, ResourceOffer> scheduledPrescriptions = new TreeMap<>();

        while (endTime <= finalTime) {
            List<Job> evaluationSet = jobRepository.findBySubmitTimeBetween((endTime - Configuration.STEP)  * Configuration.TIME_DIVISOR - 1, endTime * Configuration.TIME_DIVISOR);
            SOSMPlanner planner = new SOSMPlanner(framework);
            LOG.info("Planning jobs at time " + endTime);
            for (Job j : evaluationSet) {
                ResourceOffer resourceOffer = planner.planJob(j);
                if (resourceOffer != null) {
                    scheduledPrescriptions.put(j.getJobId(), resourceOffer);
                }
            }

            LOG.info("Updating telemetry");
            taskUsageList = aggregator.computeTasksUsageWithOwn((endTime - Configuration.STEP)  * Configuration.TIME_DIVISOR,
                    endTime * Configuration.TIME_DIVISOR, scheduledPrescriptions);
            taskUsageList.forEach(t -> SOSMFramework.telemetry.put(t.getResourceId(), t));
            LOG.info("Done.");

            framework.updateMetrics();
            framework.updateCapacities();
            framework.updateAttributes();
            Util.writeSOSMLayout(endTime, framework.getpRouterList());
            Util.writeSuccessInformation(endTime, planner.sentTasks, planner.scheduledTasks, planner.sentJobs, planner.scheduledJobs);
            Util.writeSOSMFrameworkMetrics(endTime, framework);

            endTime += Configuration.STEP;
        }
        LOG.info("Done.");


    }

    private SOSMFramework bootStrapSOSM(long time, List<IMetric> frameworkMetrics){
        List<Resource> resources = resourceRepository.findAll();
        Map<Long, Resource> resourceMap = new TreeMap<>();
        resources.stream().map(m -> resourceMap.put(m.getId(), m));
        Long startTime = -1L;
//        finishTime *= Configuration.TIME_DIVISOR;

        Long jobStartTime = (time - 4 * Configuration.STEP) * Configuration.TIME_DIVISOR - 1;
        Long endTime = time * Configuration.TIME_DIVISOR + 1;

        Histogram<String> attributeHistogram = aggregator.createAttributeHistogram(startTime, endTime);
        Map<Long, Histogram<Long>> jobSizeHistogram = aggregator.createJobSizeHistogramPerMachine(jobStartTime, endTime);

        SOSMBootStrapper sosmBootStrapper = new SOSMBootStrapper();
        List<PRouter> pRouters =  sosmBootStrapper.bootStrapSOSM(resources, attributeHistogram, jobSizeHistogram);

        Util.writeSOSMLayout(time, pRouters);

        SOSMFramework framework = new SOSMFramework("Experimental Framework", frameworkMetrics);
        framework.setpRouterList(pRouters);

        return framework;
    }

    private void checkJobRequests(){
        Long endTime = 1L;
        Long finalTime = 15000L;
        while(endTime <= finalTime) {
            List<Job> evaluationSet = jobRepository.findBySubmitTimeBetween(endTime * Configuration.TIME_DIVISOR - 1, (endTime + 300) * Configuration.TIME_DIVISOR);
            AtomicInteger different = new AtomicInteger(0);
            AtomicInteger capacities = new AtomicInteger(0);
            AtomicInteger differentAttrs = new AtomicInteger(0);
            AtomicInteger attrs = new AtomicInteger(0);
            AtomicInteger singleattrs = new AtomicInteger(0);
            AtomicInteger singleTasks = new AtomicInteger(0);
            evaluationSet.forEach(j -> {
                capacities.addAndGet(j.getTaskHistory().size());
                if (j.getTaskHistory().size() > 1) {
//                j.getTaskHistory().keySet().forEach(System.out::println);
                    TaskHistory first = j.getTaskHistory().get(0L);
                    for (Long key : j.getTaskHistory().keySet()) {
                        if (key.equals(first.getTaskIndex()))
                            continue;
                        TaskHistory other = j.getTaskHistory().get(key);
                        if (other.getRequestedCPU() != first.getRequestedCPU() || other.getRequestedMemory() != first.getRequestedMemory())
                            different.incrementAndGet();
                        for(TaskConstraint tc : first.getConstraints()){
                            boolean found = false;
                            for(TaskConstraint tc2 : other.getConstraints()){
                                if(tc.getAttributeName().equals(tc2.getAttributeName())){
                                    found = true;
                                    if(!tc.getAttributeValue().equals(tc2.getAttributeValue())){
                                        differentAttrs.incrementAndGet();
                                    }
                                }
                            }
                            if(!found)
                                differentAttrs.incrementAndGet();
                            attrs.incrementAndGet();
                        }
                    }
                }else{
                    singleattrs.addAndGet(j.getTaskHistory().get(0L).getConstraints().size());
                    singleTasks.incrementAndGet();
                }

            });
            LOG.info("Different capacities: " + different.get() + " out of " + capacities.get());
            LOG.info("Different attributes: " + differentAttrs.get() + " out of " + attrs.get());
            LOG.info("Single task attributes: " + singleattrs.get() + " for " + singleTasks.get() + " tasks.");
            endTime += Configuration.STEP;
        }
    }
}
