package ie.ucc.cl.sosm;

import ie.ucc.cl.sosm.components.PRouter;
import ie.ucc.cl.sosm.components.PSwitch;
import ie.ucc.cl.sosm.components.VRackManager;
import ie.ucc.cl.sosm.model.Characteristics;
import ro.ieat.soare.clustering.Cluster;
import ro.ieat.soare.clustering.similarity.*;
import ro.ieat.soare.clustering.startegies.ClusteringStrategy;
import ro.ieat.soare.clustering.startegies.DBSCANClusteringStrategy;
import ro.ieat.soare.math.statistics.Histogram;
import ro.ieat.soso.core.coalitions.Resource;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * Created by adispataru on 02-Aug-16.
 */
public class SOSMBootStrapper {
    public static final Logger LOG = Logger.getLogger("SOSM Bootstrapper");
    private final ClusteringStrategy clusteringStrategy = new DBSCANClusteringStrategy();

    public List<PRouter> bootStrapSOSM(List<Resource> Resources,
                                                  Histogram<String> attrDistr,
                                                  Map<Long, Histogram<Long>> resourceDistribution){


        List<PRouter> pRouterList = new ArrayList<>();
        SimilarityStrategyType str = SimilarityStrategyType.ATTRIBUTE;

        SimilarityStrategy similarityStrategy = getSimilarityStrategy(str, Resources, attrDistr, resourceDistribution);

        float[][] similarityMatrix = similarityStrategy.createSimilarityMatrix();
        Map<Long, Integer> firstMatrixIdMap = similarityStrategy.getMatrixIdMap();

        float epsR = similarityStrategy.getEpsilon();
        int minPts = 2;

        double delta = 1.0;
        float eps = (float) (epsR * delta);

        LOG.info("Clustering by strategy: " + str.name());
        List<Cluster> firstClusters = clusteringStrategy.createClusters(similarityMatrix, eps, minPts);
        LOG.info("Created " + firstClusters.size() + " clusters");

        SimilarityStrategyType str2 = SimilarityStrategyType.ATTRIBUTE;
        LOG.info("Clustering by " + str2.name() + " inside " + str.name());

        String filename = String.format("%s-%s-%.4f", str.name(), str2.name(), eps);
        int step = 0;
//        Util.writeClusteringStep(firstClusters, similarityMatrix, step, System.currentTimeMillis() - startTime,
//                filename);

        for (Cluster c : firstClusters) {
            long startTime = System.currentTimeMillis();
            //inside each cluster, apply jobSize based clustering
            List<Resource> currentResources = Resources.stream().filter(m -> c.getPoints().contains(firstMatrixIdMap.get(m.getId())))
                    .collect(Collectors.toList());

            similarityStrategy = getSimilarityStrategy(str2, currentResources, attrDistr, resourceDistribution);

            float[][] similarityMatrix2 = similarityStrategy.createSimilarityMatrix();
            float eps2 = similarityStrategy.getEpsilon();


            List<Cluster> clusterList = clusteringStrategy.createClusters(similarityMatrix2, eps2, minPts);

            Map<Long, Integer> secondMatrixIdMap = similarityStrategy.getMatrixIdMap();

            LOG.info("Created " + clusterList.size() + " clusters");

            PRouter pRouter = createPRouterFromClusters(clusterList, secondMatrixIdMap, currentResources);
            pRouter.setResourceSize(c.getPoints().size());

            pRouterList.add(pRouter);


        }

        return pRouterList;
    }

    private PRouter createPRouterFromClusters(List<Cluster> clusterList, Map<Long, Integer> matrixIdMap, List<Resource> currentResources) {

        Resource[] idList = new Resource[currentResources.size()];
        currentResources.forEach(m -> idList[matrixIdMap.get(m.getId())] = m);
        List<VRackManager> vRMs = new ArrayList<>();
        VRackManager vrman = new VRackManager(4);
        clusterList.stream().filter(c -> c.getPoints().size() == 1).forEach( c ->
            c.getPoints().forEach(i -> vrman.addResource(idList[i]))
        );
        vrman.setName(vrman.getName() + " noisy");

        vRMs.add(vrman);


        clusterList.stream().filter(c -> c.getPoints().size() > 1).forEach(c -> {
            VRackManager vrm = new VRackManager(4);
            c.getPoints().forEach(p -> vrm.addResource(idList[p]));
            vRMs.add(vrm);
        });


        PRouter pRouter = new PRouter(4);
        pRouter.setCharacteristics(new Characteristics(idList[0].getCpu(), idList[0].getMemory()));
        int total = vRMs.size();
        if(total < 10){
            PSwitch pSwitch = new PSwitch(4);
            vRMs.forEach(v -> {
                pSwitch.addUnderlyingSOSMComponent(v);
                pSwitch.setResourceSize(pSwitch.getResourceSize() + v.getResourceSize());

            });
            pRouter.addUnderlyingSOSMComponent(pSwitch);

            return pRouter;
        }else{
            int numpSwithces = Math.toIntExact(Math.round(Math.sqrt(total)));
            for(int i = 0; i < numpSwithces; i++){
                PSwitch pSwitch = new PSwitch(4);
                for(int j = i * numpSwithces; j < i * numpSwithces + numpSwithces && j < vRMs.size(); j++){
                    pSwitch.addUnderlyingSOSMComponent(vRMs.get(j));
                    pSwitch.setResourceSize(pSwitch.getResourceSize() + vRMs.get(j).getResourceSize());
                }
                pRouter.addUnderlyingSOSMComponent(pSwitch);
            }
        }

        return pRouter;
    }

    private SimilarityStrategy getSimilarityStrategy(SimilarityStrategyType str, List<Resource> Resources,
                                                     Histogram<String> attrDistr,
                                                     Map<Long, Histogram<Long>> ResourceDistribution){
        SimilarityStrategy similarityStrategy;


        switch (str){
            case ATTRIBUTE:
                similarityStrategy = new AttributeSimilarityStrategy(attrDistr, Resources);
                break;
            case PROPERTY:
                similarityStrategy = new PropertiesSimilarityStrategy(Resources);
                break;
            case JOBSIZE:
                similarityStrategy = new JobSizeSimilarityStrategy(ResourceDistribution, Resources);
                break;
            default:
                similarityStrategy = new PropertiesSimilarityStrategy(Resources);
                break;

        }
        return similarityStrategy;
    }
}
