package ie.ucc.cl.sosm.model.metrics;

/**
 * Created by adispataru on 04-Aug-16.
 */
public class ComputationalEfficiency implements IMetric {

    public static final String NAME = "ComputationalEfficiency";

    @Override
    public double assess(MetricParameters params) {
        return 1 - 0.2*(params.utilization/ params.optimalCapacity);
    }
}
