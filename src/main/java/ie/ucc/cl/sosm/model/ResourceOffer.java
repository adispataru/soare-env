package ie.ucc.cl.sosm.model;

import java.util.Map;
import java.util.TreeMap;

/**
 * Created by adispataru on 02-Aug-16.
 */
public class ResourceOffer {
    Map<Long, Long> taskResourceMapping = new TreeMap<Long, Long>();

    public Map<Long, Long> getTaskResourceMapping() {
        return taskResourceMapping;
    }

    public void setTaskResourceMapping(Map<Long, Long> taskResourceMapping) {
        this.taskResourceMapping = taskResourceMapping;
    }
}
