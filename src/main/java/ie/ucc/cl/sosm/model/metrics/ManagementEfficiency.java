package ie.ucc.cl.sosm.model.metrics;

/**
 * Created by adispataru on 04-Aug-16.
 */
public class ManagementEfficiency implements IMetric {
    public static final String NAME = "ManagementEfficiency";

    @Override
    public double assess(MetricParameters params) {
//        System.out.println("Nu: " + nUtilised + "\tNt: " + nTotal + "\tNbar: " + nOptimal);
        double end = 2 * params.capacity/params.optimalCapacity -2;
        end = Math.floor(end);
        double integ = integrate(0, end);
        double result = 1 - 2/Math.sqrt(Math.PI)* integ;
//        System.out.println("Integral = "  + integ + "; result = " + result);
        return result;
    }

    static double f(double x) {
        return Math.exp(-1*x * x);
    }

    /**
     * Integration method using Simpson's rule
     * @param a integral start
     * @param b integral end
     * @return an approximation of the integration of the function defined as f(x) over the interval [a,b].
     */
    private static double integrate(double a, double b) {
        int N = 1000;                    // precision parameter
        double h = (b - a) / (N - 1);     // step size

        // 1/3 terms
        double sum = 1.0 / 3.0 * (f(a) + f(b));

        // 4/3 terms
        for (int i = 1; i < N - 1; i += 2) {
            double x = a + h * i;
            sum += 4.0 / 3.0 * f(x);
        }

        // 2/3 terms
        for (int i = 2; i < N - 1; i += 2) {
            double x = a + h * i;
            sum += 2.0 / 3.0 * f(x);
        }

        return sum * h;
    }
}
