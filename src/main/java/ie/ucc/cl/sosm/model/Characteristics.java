package ie.ucc.cl.sosm.model;

/**
 * Created by adispataru on 05-Aug-16.
 */
public class Characteristics {
    private double cpu;
    private double memory;

    public Characteristics(double cpu, double memory) {
        this.cpu = cpu;
        this.memory = memory;
    }

    public double getCpu() {
        return cpu;
    }

    public void setCpu(double cpu) {
        this.cpu = cpu;
    }

    public double getMemory() {
        return memory;
    }

    public void setMemory(double memory) {
        this.memory = memory;
    }
}
