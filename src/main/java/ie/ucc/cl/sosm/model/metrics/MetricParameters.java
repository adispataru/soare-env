package ie.ucc.cl.sosm.model.metrics;

/**
 * Created by adispataru on 16-Aug-16.
 */
public class MetricParameters {
    public double utilization;
    public double capacity;
    public double optimalCapacity;
    public double performanceIndex;
}
