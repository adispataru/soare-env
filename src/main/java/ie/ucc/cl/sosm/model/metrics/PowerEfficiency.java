package ie.ucc.cl.sosm.model.metrics;

/**
 * Created by adispataru on 04-Aug-16.
 */
public class PowerEfficiency implements IMetric {

    public static final String NAME = "PowerEfficiency";
    //relative power efficiency
    private final double P;
    //relative power efficiency when idle
    private final double pIdle;

    public PowerEfficiency(double p, double pi) {
        P = p;
        pIdle = pi;
    }

    @Override
    public double assess(MetricParameters params) {
        return pIdle*(params.capacity - params.utilization)/
                (P * params.utilization + pIdle*(params.capacity - params.utilization));
    }
}
