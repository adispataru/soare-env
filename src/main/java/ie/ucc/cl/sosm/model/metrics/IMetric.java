package ie.ucc.cl.sosm.model.metrics;

import ie.ucc.cl.sosm.components.VRackManager;

/**
 * Created by adispataru on 04-Aug-16.
 */
public interface IMetric {
    public double assess(MetricParameters params);
}
