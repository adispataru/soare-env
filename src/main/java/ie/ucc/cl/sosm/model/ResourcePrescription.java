package ie.ucc.cl.sosm.model;

import ro.ieat.soso.core.jobs.Job;
import ro.ieat.soso.core.jobs.TaskHistory;

/**
 * Created by adispataru on 02-Aug-16.
 */
public class ResourcePrescription {
    private Job job;
    private double totalRequestedCPU;
    private double totalRequestedMemory;
    private double parallelSize;
//    private Map<String, List<String>> constraints;


    public ResourcePrescription(Job job){
        this.job = job;
        totalRequestedCPU = 0;
        totalRequestedMemory = 0;
        parallelSize = 0;
//        constraints = new TreeMap<>();
        for(TaskHistory taskHistory : job.getTaskHistory().values()){
            totalRequestedCPU += taskHistory.getRequestedCPU();
            totalRequestedMemory += taskHistory.getRequestedMemory();
            if(taskHistory.isParallel())
                parallelSize += 1;
//            for(TaskConstraint tc : taskHistory.getConstraints()){
//                constraints.putIfAbsent(tc.getAttributeName(), new ArrayList<>());
//                constraints.get(tc.getAttributeName()).add(tc.getAttributeValue());
//            }
        }
    }

    public Job getJob() {
        return job;
    }

    public void setJob(Job job) {
        this.job = job;
    }

    public double getTotalRequestedCPU() {
        return totalRequestedCPU;
    }

    public void setTotalRequestedCPU(double totalRequestedCPU) {
        this.totalRequestedCPU = totalRequestedCPU;
    }

    public double getTotalRequestedMemory() {
        return totalRequestedMemory;
    }

    public void setTotalRequestedMemory(double totalRequestedMemory) {
        this.totalRequestedMemory = totalRequestedMemory;
    }

    public double getParallelSize() {
        return parallelSize;
    }

    public void setParallelSize(double parallelSize) {
        this.parallelSize = parallelSize;
    }

//    public Map<String, List<String>> getConstraints() {
//        return constraints;
//    }
//
//    public void setConstraints(Map<String, List<String>> constraints) {
//        this.constraints = constraints;
//    }
}
