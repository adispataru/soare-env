package ie.ucc.cl.sosm.model.metrics;

/**
 * Created by adispataru on 04-Aug-16.
 */
public class TaskThroughput implements IMetric {

    public static final String NAME = "TaskThroughput";

    @Override
    public double assess(MetricParameters params) {

//        System.out.println("Nu: " + nUtilised + "\tNt: " + nTotal + "\tNbar: " + nOptimal);
        return params.performanceIndex * (params.capacity - params.utilization) / params.capacity;

    }
}
