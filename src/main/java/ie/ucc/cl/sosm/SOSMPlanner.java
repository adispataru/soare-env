package ie.ucc.cl.sosm;

import ie.ucc.cl.sosm.components.PRouter;
import ie.ucc.cl.sosm.components.SOSMFramework;
import ie.ucc.cl.sosm.model.Characteristics;
import ie.ucc.cl.sosm.model.ResourceOffer;
import ie.ucc.cl.sosm.model.ResourcePrescription;
import ro.ieat.soare.math.statistics.distance.PropertyDistance;
import ro.ieat.soso.core.jobs.Job;
import ro.ieat.soso.core.jobs.TaskHistory;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import static ro.ieat.soare.scheduling.JobPlanner.componentSatisfies;

/**
 * Created by adispataru on 04-Aug-16.
 */
public class SOSMPlanner {

    private SOSMFramework framework;
    AtomicLong sentJobs = new AtomicLong(0);
    AtomicLong scheduledJobs = new AtomicLong(0);
    AtomicLong sentTasks = new AtomicLong(0);
    AtomicLong scheduledTasks = new AtomicLong(0);

    public SOSMPlanner(SOSMFramework framework){
        this.framework = framework;
    }

    public ResourceOffer planJob(Job job){
        sentJobs.incrementAndGet();
        sentTasks.addAndGet(job.getTaskHistory().size());
        ResourcePrescription rp = new ResourcePrescription(job);
//        System.out.println("------------------------------");
//        System.out.println("Prescription" + rp.getJob().getJobId() + " : " + rp.getTotalRequestedCPU() + ", " + rp.getTotalRequestedMemory());
        List<PRouter> pRouters = selectPRoutersForCharacteristics(rp);

        int i = 0;
        for(PRouter pRouter : pRouters) {
            if(i > 0){
                System.out.println("Trial: " + i);
            }
            ResourceOffer result = pRouter.findResources(rp);
            if(result != null){
                pRouter.updateMetrics(false);
                pRouter.updateCumulativeCapacity();
                pRouter.updateCumulativeAttributes();
                scheduledJobs.incrementAndGet();
                scheduledTasks.addAndGet(job.getTaskHistory().size());
                return result;
            }
            i++;
        }
        return null;
    }

    private List<PRouter> selectPRoutersForCharacteristics(ResourcePrescription rp) {
        List<PRouter> result = new ArrayList<>();
        double distances[] = new double[framework.getpRouterList().size()];
        double minDistance = Double.MAX_VALUE;
        Characteristics characteristics = new Characteristics(rp.getTotalRequestedCPU(), rp.getTotalRequestedMemory());
        for(PRouter p : framework.getpRouterList()){
            if(componentSatisfies(p, rp.getJob().getTaskHistory().get(0L))){
                if(p.getParallelCapability() > rp.getParallelSize()) {
                    double distance = PropertyDistance.computeCharacteristicsDistance(characteristics, p.getCharacteristics());
                    if (distance > 0) {
                        int i = result.size();
                        int j = 0;
                        while (j < i && distance > distances[j])
                            j++;
                        result.add(j, p);
                        //adjust distance array
                        double val = distances[j];
                        distances[j] = distance;
                        while (j <= i){
                            double aux = distances[j];
                            distances[j] = val;
                            val = aux;
                            j++;
                        }
                    }
                }
            }
        }

//        if(result != null) {
//            System.out.println("+Selected: " + result.getName() + " out of " + candidates + " candidates");
//            System.out.println(rp.getTotalRequestedCPU() + " <-> " + result.getCharacteristics().getCpu());
//            System.out.println(rp.getTotalRequestedMemory() + " <-> " + result.getCharacteristics().getMemory());
//        }
        return result;
    }


}
