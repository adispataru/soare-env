package ie.ucc.cl.sosm.components;

import ie.ucc.cl.sosm.model.ResourceOffer;
import ie.ucc.cl.sosm.model.ResourcePrescription;
import ie.ucc.cl.sosm.model.metrics.MetricParameters;
import ro.ieat.soso.core.coalitions.Resource;
import ro.ieat.soso.core.jobs.*;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import static ro.ieat.soare.scheduling.JobPlanner.satisfies;

/**
 * Created by adispataru on 02-Aug-16.
 */
public class VRackManager extends SOSMComponent {

    private static final AtomicInteger counter = new AtomicInteger(0);
    private List<Resource> resources;
    private final double availabilityThreshold = 0.8;
    private ResourceSelectionStrategy selectionStrategy = new SimpleSelection();
    private Map<Long, Boolean> exchangeableResources;

    public VRackManager(int objectiveDimensions) {
        super(objectiveDimensions);
        this.name = "VRackManager " + counter.incrementAndGet();
        this.resources = new ArrayList<>();
        exchangeableResources = new TreeMap<>();
    }

    public VRackManager(double[] metrics, double[] weights) {
        super(metrics, weights);
        this.name = "VRackManager " + counter.incrementAndGet();
        this.resources = new ArrayList<>();
        exchangeableResources = new TreeMap<>();
    }

    private boolean isResourceAvailable(Resource r){
        return r.getUsagePrediction().getCpu() < availabilityThreshold * r.getCpu() &&
                r.getUsagePrediction().getMemory() < availabilityThreshold * r.getMemory();
    }

    private boolean isResourceExchangeable(Resource r){
        return exchangeableResources.get(r.getId()) && isResourceAvailable(r);
    }

    public List<Resource> getResources() {
        return resources;
    }

    @Override
    public int getResourceSize(){
        resourceSize = resources.size();
        return resourceSize;
    }


    @Override
    public void updateMetrics(boolean telemetric){
        final double[] nUtilized = {0, 0};
        final AtomicInteger size = new AtomicInteger(0);
        if(telemetric) {
            resources.forEach(r -> {
                TaskUsage t = SOSMFramework.telemetry.get(r.getId());
                if (t != null) {
                    r.setUsagePrediction(t);
//                    nUtilized[0] += t.getCpu() / r.getCpu();
                    nUtilized[0] += 1;
                    nUtilized[1] += t.getMemory() / r.getMemory();
                    if(isResourceAvailable(r)){
                        size.getAndIncrement();
                    }
                } else {
                    r.setUsagePrediction(new TaskUsage());
                }
            });
        }else{
            resources.forEach(r -> {
//                nUtilized[0] += r.getUsagePrediction().getCpu() / r.getCpu();
                if(isResourceAvailable(r)) {
                    nUtilized[0] += 1;
                    nUtilized[1] += r.getUsagePrediction().getMemory() / r.getMemory();
                    size.incrementAndGet();
                }
            });
        }

        this.parallelCapability = size.get();
        MetricParameters metricParameters = new MetricParameters();
        metricParameters.capacity = this.resources.size();
        metricParameters.utilization = nUtilized[0];
        //Idea: Set nBar as the maximum job size inside this vrack
        metricParameters.optimalCapacity = computeNBar();
        metricParameters.performanceIndex = resources.get(0).getCpu();
        for(int i = 0; i < SOSMFramework.metrics.size(); i++){
            metrics[i] = SOSMFramework.metrics.get(i).assess(metricParameters);
        }
    }

//    @Override
//    public void updateCumulativeCapacity(){
//        this.cumulativeCapacity = new double[]{0,0};
//        resources.forEach(r -> {
//            TaskUsage t = SOSMFramework.telemetry.get(r.getId());
//            if(t != null) {
//                cumulativeCapacity[0] += r.getCpu() - t.getCpu();
//                cumulativeCapacity[1] += r.getMemory() - t.getMemory();
//            }else{
//                cumulativeCapacity[0] += r.getCpu();
//                cumulativeCapacity[1] += r.getMemory();
//            }
//        });
//    }

    @Override
    public void updateCumulativeCapacity(){
        this.cumulativeCapacity = new double[]{0,0};
        List<Resource> availableResources = resources.stream().filter(this::isResourceAvailable)
                .collect(Collectors.toList());
        availableResources.forEach(r -> {
            cumulativeCapacity[0] += r.getCpu() - r.getUsagePrediction().getCpu();
            cumulativeCapacity[1] += r.getMemory() - r.getUsagePrediction().getMemory();
        });
    }

    private double computeNBar(){
        double nv = this.parent.getCooperative().size();
        AtomicInteger sum = new AtomicInteger(0);
        this.parent.getCooperative().forEach(s -> {
            VRackManager v = (VRackManager) s;
            sum.getAndAdd(v.getResources().size());
        });
        return (1/nv) * sum.get();
    }

    public void addResource(Resource r){
        resources.add(r);
        exchangeableResources.put(r.getId(), true);
    }


    @Override
    public ResourceOffer findResources(ResourcePrescription rp){
        ResourceOffer resourceOffer = new ResourceOffer();
        List<Resource> availableResources = resources.stream().filter(this::isResourceAvailable)
                .collect(Collectors.toList());
        int prescritptionSize = rp.getJob().getTaskHistory().size();

//        if(prescritptionSize > availableResources.size()){
//            // NOT ENOUGH RESOURCES: TRIGGER REORGANIZATION
//            System.out.println("Need for reorganization (not enough resources) at " + this.name + " with " + availableResources.size() + " resources");
//            System.out.println("Prescription total cpu:  " + rp.getTotalRequestedMemory());
//            System.out.println("Prescription total memory:  " + rp.getTotalRequestedMemory());
//            System.out.println("Prescription size: " + rp.getJob().getTaskHistory().size());
//
//
//        }else{
            //SELECT out of available resources based on optimization principles.
            resourceOffer = selectionStrategy.selectResourcesForJob(rp, availableResources);
            if(resourceOffer == null){
                availableResources = resources.stream().filter(this::isResourceAvailable)
                        .collect(Collectors.toList());
                System.out.println("Need for reorganization (not enough satisfiable) at " + this.name + " with "
                        + availableResources.size() + " available resources");
                System.out.println("Prescription total cpu:  " + rp.getTotalRequestedMemory());
                System.out.println("Prescription total memory:  " + rp.getTotalRequestedMemory());
                System.out.println("Prescription size: " + rp.getJob().getTaskHistory().size()
                        + "\nAttribute requests: " + rp.getJob().getTaskHistory().get(0L).getConstraints().size());

                TaskHistory probe = rp.getJob().getTaskHistory().get(0L);
                List<Resource> satisfiable = getSatisfiableResources(probe, availableResources);

                if(satisfiable.size() > prescritptionSize / 2){
                    // acquire resources from neighbors
                    System.out.println("Acquiring resources from cooperative... Have " + satisfiable.size());

                    List[] borrowed = new List[this.parent.cooperative.size()];
                    int totalBorrowed = borrowResources(prescritptionSize, probe, satisfiable, borrowed);

                    if(satisfiable.size() + totalBorrowed >= prescritptionSize){
                        //collect resources and return handles
                        for(int i = 0; i < borrowed.length; i++){
                            if(borrowed[i] != null) {
                                satisfiable.addAll(borrowed[i]);
                                System.out.println("Borrowed: " + borrowed[i].size());
                                for (int j = 0; j < borrowed[i].size(); j++){
                                    this.addResource((Resource) borrowed[i].get(j));
                                }
                                ((VRackManager)this.parent.cooperative.get(i)).removeResources(borrowed[i]);
                            }
                        }
                        System.out.println("Satisfiable: " + satisfiable.size());
                        availableResources = resources.stream().filter(this::isResourceAvailable)
                                .collect(Collectors.toList());
                        System.out.println("Available now: " + availableResources.size());
                        resourceOffer = selectionStrategy.selectResourcesForJob(rp, availableResources);

                    }else {

                        // if after the acquiring process there aren't enough to satisfy, then try merging the VRMs
                        resourceOffer = getResourceOfferByMerging(rp, prescritptionSize, probe, satisfiable);
                    }
                }else{
                    //form new vRack with resources acquired from neighbors and these satisfiable
                    System.out.println("Creating new VRM by acquiring resources from cooperative...");

                    List[] borrowed = new List[this.parent.cooperative.size()];
                    int totalBorrowed = borrowResources(prescritptionSize, probe, satisfiable, borrowed);

                    if(satisfiable.size() + totalBorrowed >= prescritptionSize){
                        //collect resources and create new VRM
                        this.removeResources(satisfiable);
                        for(int i = 0; i < borrowed.length; i++){
                            if(borrowed[i] != null) {
                                satisfiable.addAll(borrowed[i]);
                                ((VRackManager)this.parent.cooperative.get(i)).removeResources(borrowed[i]);
                            }
                        }
                        VRackManager vrm = new VRackManager(this.metrics.length);
                        satisfiable.forEach(vrm::addResource);
                        this.parent.addUnderlyingSOSMComponent(vrm);
                        return vrm.findResources(rp);
//                        resourceOffer = selectionStrategy.selectResourcesForJob(rp, satisfiable);

                    }else {
                        // if after the acquiring process there aren't enough to satisfy, then try merging the VRMs
                        resourceOffer = getResourceOfferByMerging(rp, prescritptionSize, probe, satisfiable);
                    }

                }

            }

            if(resourceOffer != null) {
                //resource is accommodated
                //add requested cpu and memory to known usage about resource
                boolean exchangeable = resourceOffer.getTaskResourceMapping().size() == 1;
                resourceOffer.getTaskResourceMapping().forEach((tid, id ) -> {
                    Resource r = resources.get(findById(id));
                    r.getUsagePrediction().addCpu(rp.getJob().getTaskHistory().get(tid).getRequestedCPU());
                    r.getUsagePrediction().addMemory(rp.getJob().getTaskHistory().get(tid).getRequestedMemory());

                    exchangeableResources.put(r.getId(), exchangeable);

                });
            }
//        }
        return resourceOffer;
    }

    public ResourceOffer getResourceOfferByMerging(ResourcePrescription rp, int prescritptionSize, TaskHistory probe, List<Resource> satisfiable) {
        List<Resource> availableResources;
        ResourceOffer resourceOffer;
        System.out.println("Cannot find enough exchangeable resources. Trying to merge VRMs to satisfy prescription");
        int mergeCandidate = mergeResources(prescritptionSize, probe, satisfiable);
        if(mergeCandidate > 0){
            this.mergeWithVRM(mergeCandidate);
        }
        availableResources = resources.stream().filter(this::isResourceAvailable)
                .collect(Collectors.toList());
        resourceOffer = selectionStrategy.selectResourcesForJob(rp, availableResources);
        return resourceOffer;
    }

    private int borrowResources(int prescritptionSize, TaskHistory probe, List<Resource> satisfiable, List[] borrowed) {
        int totalBorrowed = 0;
        for(int i = 0; i < this.parent.cooperative.size(); i++){
            VRackManager v = (VRackManager) this.parent.cooperative.get(i);
            if(v.getName().equals(this.name))
                continue;
            List<Resource> probedResources = v.probeForExchangeableSatisfiableResources(probe);
            borrowed[i] = probedResources;
            totalBorrowed += probedResources.size();
            if(satisfiable.size() + totalBorrowed >= prescritptionSize){
                break;
            }

        }
        return totalBorrowed;
    }

    public void mergeWithVRM(int position){
        VRackManager vrm = (VRackManager) this.parent.cooperative.get(position);
        vrm.getResources().forEach(r -> {
            this.resources.add(r);
            this.exchangeableResources.put(r.getId(), vrm.isResourceExchangeable(r));
        });
        vrm.getResources().clear();
        List<SOSMComponent> coop = this.parent.cooperative;
        int index = -1;
        for(int i = 0; i < coop.size(); i++){
            if(coop.get(i).getName().equals(vrm.getName())){
                index = i;
                break;
            }
        }
        coop.remove(index);
        vrm.setParent(null);
    }

    private int mergeResources(int prescritptionSize, TaskHistory probe, List<Resource> satisfiable) {
        int index = -1;
        int size = Integer.MAX_VALUE;
        for(int i = 0; i < this.parent.cooperative.size(); i++){
            VRackManager v = (VRackManager) this.parent.cooperative.get(i);
            if(v.getName().equals(this.name))
                continue;
            List<Resource> probedResources = v.probeForSatisfiableResources(probe);
            if(satisfiable.size() + probedResources.size() >= prescritptionSize){
                if(probedResources.size() < size) {
                    size = probedResources.size();
                    index = i;
                }
            }

        }
        return index;
    }



    private int findById(Long id){
        for(int i = 0; i < resources.size(); i++){
            Resource r = resources.get(i);
            if (r.getId().equals(id))
                return i;
        }
        System.out.print("Not found resource: " + id);
        return -1;
    }

    public void removeResources(List<Resource> toRemove){
        this.resources.removeAll(toRemove);
        toRemove.forEach(r -> exchangeableResources.remove(r.getId()));
    }


    @Override
    public void updateCumulativeAttributes(){
        cummulativeAttributes = new TreeMap<>();
        List<Resource> availableResources = resources.stream().filter(this::isResourceAvailable)
                .collect(Collectors.toList());
        for(Resource r : availableResources){
            Map<String, String> atts = r.getAttributes();
            for(String k : atts.keySet()){
                if(cummulativeAttributes.containsKey(k)){
                    cummulativeAttributes.get(k).add(atts.get(k));
                }else{
                    cummulativeAttributes.put(k, new ArrayList<>());
                    cummulativeAttributes.get(k).add(atts.get(k));
                }
            }
        }
    }

    public List<Resource> probeForExchangeableSatisfiableResources(TaskHistory th){
        List<Resource> exchangeable = resources.stream().filter(this::isResourceExchangeable)
                .collect(Collectors.toList());
        List<Resource> result = new ArrayList<>();
        for(Resource m : exchangeable){
            if(satisfies(m, th))
                result.add(m);
        }
        return result;
    }

    public List<Resource> probeForSatisfiableResources(TaskHistory th){
        List<Resource> exchangeable = resources.stream().filter(this::isResourceAvailable)
                .collect(Collectors.toList());
        List<Resource> result = new ArrayList<>();
        for(Resource m : exchangeable){
            if(satisfies(m, th))
                result.add(m);
        }
        return result;
    }

    private List<Resource> getSatisfiableResources(TaskHistory th, List<Resource> resources) {
        List<Resource> result = new ArrayList<>();
        for(Resource m : resources){
            if(satisfies(m, th))
                result.add(m);
        }
        return result;
    }


    private interface ResourceSelectionStrategy{
        ResourceOffer selectResourcesForJob(ResourcePrescription rp, List<Resource> resources);
    }

    private class SimpleSelection implements ResourceSelectionStrategy{
        private double epsilon = 0.0001;

        @Override
        public ResourceOffer selectResourcesForJob(ResourcePrescription rp, List<Resource> resources) {

            ResourceOffer offer = null;
            Job job = rp.getJob();
            int jobSize = job.getTaskHistory().size();
            TaskHistory th = job.getTaskHistory().get(0L);
            List<Resource> satisfiableResources = getSatisfiableResources(th, resources);
            System.out.println("Satisfiable resources: " + satisfiableResources.size() + " for size = " + jobSize);

            if(satisfiableResources.size() == jobSize){
                //job is successfully scheduled
                offer = new ResourceOffer();
                Map<Long, Long> taskResourceMapping = new HashMap<>();
                for(int i = 0; i < satisfiableResources.size(); i++){
                    taskResourceMapping.put((long) i, satisfiableResources.get(i).getId());
                }
                offer.setTaskResourceMapping(taskResourceMapping);
            }

            if(satisfiableResources.size() > job.getTaskHistory().size()){

                offer = new ResourceOffer();
                Map<Long, Long> taskResourceMapping = new HashMap<>();

                satisfiableResources.sort(this::compare);
                Collections.reverse(satisfiableResources);

                int i = 0;
                for(Long key : job.getTaskHistory().keySet()){
                    taskResourceMapping.put(key, satisfiableResources.get(i).getId());
                    i++;
                }
                offer.setTaskResourceMapping(taskResourceMapping);
            }


            return offer;
        }

        private int compare(Resource o1, Resource o2){
            TaskUsage t1 = o1.getUsagePrediction();
            TaskUsage t2 = o2.getUsagePrediction();
            if(Math.abs(t1.getCpu() - t2.getCpu()) < epsilon){
                //equal cpu usage; return in comparison with memory
                return Double.compare(t1.getMemory(), t2.getMemory());
            }
            return Double.compare(t1.getCpu(), t2.getCpu());
        }
    }

}
