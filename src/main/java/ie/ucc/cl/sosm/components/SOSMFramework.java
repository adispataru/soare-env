package ie.ucc.cl.sosm.components;

import ie.ucc.cl.sosm.model.metrics.IMetric;
import ro.ieat.soso.core.jobs.TaskUsage;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by adispataru on 04-Aug-16.
 */
public class SOSMFramework {
    private String name;
    private List<PRouter> pRouterList;
    private List<SOSMComponent> leaves;
    static List<IMetric> metrics;
    public static Map<Long, TaskUsage> telemetry;

    public SOSMFramework(String name, List<IMetric> metricFunctions){
        this.name = name;
        pRouterList = new ArrayList<>();
        leaves = new ArrayList<>();
        metrics = metricFunctions;
        telemetry = new TreeMap<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<PRouter> getpRouterList() {
        return pRouterList;
    }

    public void addPRouter(PRouter p){
        pRouterList.add(p);
    }

    public void setpRouterList(List<PRouter> pRouterList) {
        this.pRouterList = pRouterList;
    }

    public List<SOSMComponent> getLeaves() {
        return leaves;
    }

    public void addLeaf(SOSMComponent c){
        leaves.add(c);
    }

    public void setLeaves(List<SOSMComponent> leaves) {
        this.leaves = leaves;
    }

    public void updateMetrics(){
        pRouterList.forEach(s -> s.updateMetrics(true));
    }

    public void updateCapacities(){
        pRouterList.forEach(SOSMComponent::updateCumulativeCapacity);
    }

    public void updateAttributes(){
        pRouterList.forEach(SOSMComponent::updateCumulativeAttributes);
    }

    public void setWeights(double[] weights){
        for(PRouter p : pRouterList){
            p.setWeightsRecusively(weights);
        }
    }

}
