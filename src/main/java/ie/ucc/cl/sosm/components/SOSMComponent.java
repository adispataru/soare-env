package ie.ucc.cl.sosm.components;

import ie.ucc.cl.sosm.model.ResourceOffer;
import ie.ucc.cl.sosm.model.ResourcePrescription;
import ro.ieat.soare.io.Util;

import java.util.*;

import static ro.ieat.soare.scheduling.JobPlanner.componentSatisfies;

/**
 * Created by adispataru on 02-Aug-16.
 */
public abstract class SOSMComponent {
    protected double[] metrics;
    protected double[] cumulativeCapacity;
    protected Map<String, List<String>> cummulativeAttributes;
    protected double[] weights;
    protected double suitabilityIndex;
    protected int resourceSize = 0;
    protected SOSMComponent parent;
    protected List<SOSMComponent> cooperative;
    protected String name;
    protected int parallelCapability = 0;

    public SOSMComponent(int objectiveDimensions){
        metrics = new double[objectiveDimensions];
        weights = new double[objectiveDimensions];
        cooperative = new ArrayList<>();
        cumulativeCapacity = new double[2];
        cummulativeAttributes = new TreeMap<>();
    }

    public SOSMComponent(double[] metrics, double[] weights){
        this.metrics = metrics;
        this.weights = weights;
        this.cooperative = new ArrayList<>();
        cumulativeCapacity = new double[2];
        cummulativeAttributes = new TreeMap<>();
    }


    public void updateMetrics(boolean telemetric){
        Iterator<SOSMComponent> iterator = cooperative.iterator();
        while (iterator.hasNext()){
            SOSMComponent c = iterator.next();
            if(c.getResourceSize() == 0){
                c.setParent(null);
                iterator.remove();
            }else {
                c.updateMetrics(telemetric);
            }
        }
        //TODO ask about metrics averaging
        double[] avgMetrics = new double[metrics.length];
        cooperative.forEach(s -> {
            if(s.getParallelCapability() > this.parallelCapability)
                this.parallelCapability = s.getParallelCapability();
            double[] ms = s.getMetrics();
            for(int i =0; i < avgMetrics.length; i++){
                avgMetrics[i] += ms[i];
            }
        });
        for(int i =0; i < avgMetrics.length; i++){
            avgMetrics[i] /= cooperative.size();
        }
        this.metrics = avgMetrics;
    }

    public ResourceOffer findResources(ResourcePrescription rp){
        double maxSI = 0;
        int maxSIIndex = 0;
        for(int i = 0; i < cooperative.size(); i++){
            SOSMComponent coop = cooperative.get(i);

            //filter based on cumulative capacity
            if(rp.getTotalRequestedCPU() < coop.getCumulativeCapacity()[0] &&
                    rp.getTotalRequestedMemory() < coop.getCumulativeCapacity()[1]) {
                //filter based on cumulative attributes
                if(componentSatisfies(coop, rp.getJob().getTaskHistory().get(0L))) {
                    //filter based on parallel size
                    if(coop.getParallelCapability() > rp.getParallelSize()) {
                        //keep the one with the maximum SI
                        if (coop.computeAndGetSuitabilityIndex() > maxSI) {
                            maxSI = coop.getSuitabilityIndex();
                            maxSIIndex = i;
                        }
                    }
                }
            }
        }

        Util.writePrescriptionPath(this.name, cooperative.get(maxSIIndex).getName(), maxSI, rp);
        System.out.println("*Selected: " + cooperative.get(maxSIIndex).getName());
        ResourceOffer result = cooperative.get(maxSIIndex).findResources(rp);

        if(result == null){
            //trigger reorganization
        }

        return result;

    }

    public void propagateWeights(double[] newWeights){
        for(int i = 0; i < newWeights.length; i++){
            weights[i] = (weights[i] + newWeights[i]) / 2;
        }
        cooperative.forEach( sosmComponent -> sosmComponent.propagateWeights(weights));
    }


    public double[] getMetrics() {
        return metrics;
    }

    public void setMetrics(double[] metrics) {
        this.metrics = metrics;
    }

    public double[] getWeights() {
        return weights;
    }

    public void setWeights(double[] weights) {
        this.weights = weights;
    }

    public void setWeightsRecusively(double[] weights) {
        this.weights = weights;
        for(SOSMComponent sc : cooperative){
            sc.setWeightsRecusively(weights);
        }
    }

    public double computeAndGetSuitabilityIndex(){
        double result = 0;
        for(int i = 0; i < metrics.length; i++){
            result += weights[i] * metrics[i];
        }
        this.suitabilityIndex = result;
        return suitabilityIndex;
    }

    public double getSuitabilityIndex() {
        return suitabilityIndex;
    }


    public List<SOSMComponent> getCooperative() {
        return cooperative;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void addUnderlyingSOSMComponent(SOSMComponent sc){
        this.cooperative.add(sc);
        sc.setParent(this);
    }

    public int getResourceSize() {
        return resourceSize;
    }

    public void setResourceSize(int resourceSize) {
        this.resourceSize = resourceSize;
    }

    public SOSMComponent getParent() {
        return parent;
    }

    public void setParent(SOSMComponent parent) {
        this.parent = parent;
    }

    public double[] getCumulativeCapacity() {
        return cumulativeCapacity;
    }

    public void updateCumulativeCapacity(){
        cumulativeCapacity = new double[]{0, 0};
        for(SOSMComponent sc : cooperative){
            sc.updateCumulativeCapacity();
            for(int i = 0; i < cumulativeCapacity.length; i++){
                cumulativeCapacity[i] += sc.getCumulativeCapacity()[i];
            }
        }
    }

    public Map<String, List<String>> getCummulativeAttributes() {
        return cummulativeAttributes;
    }

    public void updateCumulativeAttributes(){
        cummulativeAttributes = new TreeMap<>();
        for(SOSMComponent sc : cooperative){
            sc.updateCumulativeAttributes();
            Map<String, List<String>> ca = sc.getCummulativeAttributes();
            for(String k : ca.keySet()){
                if(cummulativeAttributes.containsKey(k)){
                    cummulativeAttributes.get(k).addAll(ca.get(k));
                }else{
                    cummulativeAttributes.put(k, ca.get(k));
                }
            }
        }
    }

    public void updateAll(boolean telemetric){
        this.updateMetrics(telemetric);
        this.updateCumulativeCapacity();
        this.updateCumulativeAttributes();
    }

    public int getParallelCapability() {
        return parallelCapability;
    }
}
