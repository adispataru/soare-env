package ie.ucc.cl.sosm.components;

import ie.ucc.cl.sosm.model.Characteristics;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by adispataru on 02-Aug-16.
 */
public class PRouter extends SOSMComponent {

    private static final AtomicInteger counter = new AtomicInteger(0);
    private Characteristics characteristics;

    public PRouter(int objectiveDimensions) {
        super(objectiveDimensions);
        this.name = "pRouter " + counter.incrementAndGet();
    }

    public PRouter(double[] metrics, double[] weights) {
        super(metrics, weights);
        this.name = "pRouter " + counter.incrementAndGet();
    }


    public Characteristics getCharacteristics() {
        double cpu = cumulativeCapacity[0];
        double mem = cumulativeCapacity[1];
        characteristics = new Characteristics(cpu, mem);
        return characteristics;
    }

    public void setCharacteristics(Characteristics characteristics) {
        this.characteristics = characteristics;
    }
}
