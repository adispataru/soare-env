package ie.ucc.cl.sosm.components;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by adispataru on 02-Aug-16.
 */
public class PSwitch extends SOSMComponent {

    private static final AtomicInteger counter = new AtomicInteger(0);

    public PSwitch(int objectiveDimensions) {
        super(objectiveDimensions);
        this.name = "pSwitch " + counter.incrementAndGet();
    }

    public PSwitch(double[] metrics, double[] weights) {
        super(metrics, weights);
        this.name = "pSwitch " + counter.incrementAndGet();
    }

}
