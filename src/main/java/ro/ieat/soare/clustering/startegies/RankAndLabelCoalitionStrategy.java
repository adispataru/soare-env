package ro.ieat.soare.clustering.startegies;

import ro.ieat.soare.clustering.reasoning.ResourceAbstraction;
import ro.ieat.soso.core.coalitions.Coalition;
import ro.ieat.soso.core.coalitions.Resource;

import java.util.*;
import java.util.logging.Logger;

/**
 * Created by adrian on 2/10/16.
 */
public class RankAndLabelCoalitionStrategy {

    private final Map<Long, Long> ResourceMaxTaskMapping;
    private Map<Integer, Map<Integer, Double>> similarityMap;
    private static Long clusterCounter = 1L;
    private double alpha = 0.01;
    private double gamma = 1.0;
    private Double dropProbability = 0.1;
    private int maxSize;
    private static int antCounter = 0;
    private final Logger LOG = Logger.getLogger(RankAndLabelCoalitionStrategy.class.toString());

    public RankAndLabelCoalitionStrategy(Map<Long, Long> ResourceMaxTaskMapping, int maxSize){
        this.ResourceMaxTaskMapping = ResourceMaxTaskMapping;
        this.maxSize = maxSize;
    }


    public List<Coalition> createCoalitions(List<Resource> Resources){
        similarityMap = new TreeMap<>();
        List<Rankable> rankableResources = new ArrayList<>();
        for(Resource m : Resources){
            Rankable a = new Rankable(m);
            rankableResources.add(a);
        }

        LOG.info("Learning threshold");
        List<Rankable> ranked = rankLearnThreshold(rankableResources, maxSize);
        LOG.info("Done.");

//        LOG.info("Random meeting ants: " + ranked.size());
////        Map<Long, List<Ant>> clusters = randomMeetAnts(rankableResources);
//        LOG.info("Done.");


        LOG.info("Creating ro.ieat.soare.coalitions");
        List<Coalition> result = new ArrayList<>();
        Iterator<Rankable> iterator = ranked.iterator();
        List<Resource> freeResources = new ArrayList<>();
        while(iterator.hasNext()){
            double capacity = 0.0;
            Coalition c = new Coalition();
            c.setId(0);
            c.setConfidenceLevel(1.0);
            List<Resource> ResourceList = new ArrayList<>();
            while (capacity < 1 && iterator.hasNext()){
                Rankable rankable = iterator.next();
                Resource m = rankable.data;

                if(capacity + rankable.threshold <= 1) {
                    ResourceList.add(m);
                    capacity += rankable.threshold;
                }
                else{
                    freeResources.add(m);
                    if(ResourceList.size() > 0) {
                        c.setResources(ResourceList);
                        result.add(c);
                        LOG.info("Resources in coalition: " + c.getResources().size());
                    }
                    break;
                }
            }

        }

        for(Resource m : freeResources){
            Coalition c = new Coalition();
            c.setId(0);
            c.setConfidenceLevel(1.0);
            List<Resource> ResourceList = new ArrayList<>();
            ResourceList.add(m);
            c.setResources(ResourceList);
            result.add(c);
        }
        LOG.info("Created " + result.size() + " ro.ieat.soare.coalitions!");
        return result;
    }

    public void learnThreshold(List<? extends ResourceAbstraction> resourceAbstractionList) {

        List<Rankable> rankableList = new ArrayList<>();
        resourceAbstractionList.forEach(r ->{
            if(r instanceof Rankable)
                rankableList.add((Rankable) r);
        });

        rankLearnThreshold(rankableList, maxSize);
    }

    protected Double adjustThresholdAndValue(ResourceAbstraction a, ResourceAbstraction resourceAbstraction) {
        return adjustThresholdAndValue((Rankable) a, (Rankable) resourceAbstraction);
    }


    private List<Rankable> rankLearnThreshold(List<Rankable> resources, int maxSize){

        List<Rankable> result = new ArrayList<>();
        for (Rankable a : resources) {

            adjustThresholdAndValue(a, maxSize);
            int j = 0;
            while (j < result.size()) {
                if (a.threshold < result.get(j).threshold)
                    j++;
                else {
                    result.add(j, a);
                    break;
                }
            }
            if (j == result.size()) {
                result.add(a);
            }
        }
        return result;

    }

    private void adjustThresholdAndValue(Rankable a, int maxSize) {
        if(ResourceMaxTaskMapping.get(a.data.getId()) != null) {
            Long resourceValue = ResourceMaxTaskMapping.get(a.data.getId());
            a.threshold = (double) 1 / resourceValue;
            a.value = resourceValue / (double) maxSize;
        }else{
            a.threshold = 1.0;
        }
    }


    private class Rankable extends ResourceAbstraction {
        Resource data;
        Long label;
        Double threshold;
        Long meetingCounter;
        //nest size parameter
        Double m;
        //acceptance degree
        Double value;
        Integer id;

        Rankable(Resource m){
            this.id = antCounter++;
            this.data = m;
            label = 0L;
            threshold = .0;

            meetingCounter = 0L;
            this.m = .0;
            this.value = .0;

        }

        public boolean equals(Object o){
            if(o instanceof Rankable){
                Rankable a = (Rankable) o;
                return a.data.getId().equals(this.data.getId());
            }
            return false;
        }

        void reset(){
            label = 0L;
        }

    }


}
