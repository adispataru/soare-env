package ro.ieat.soare.clustering.startegies;

import ro.ieat.soare.clustering.Cluster;
import ro.ieat.soare.clustering.reasoning.ResourceAbstraction;
import ro.ieat.soare.clustering.similarity.SimilarityStrategy;
import ro.ieat.soso.core.coalitions.Coalition;
import ro.ieat.soso.core.coalitions.Resource;

import java.util.*;
import java.util.logging.Logger;

/**
 * Created by adrian on 2/10/16.
 */
public class AntColonyClusteringStrategy implements ClusteringStrategy {

//    private final Map<Long, Long> ResourceMaxTaskMapping;
    private float[][] similarityMatrix;
    private static Long clusterCounter = 1L;
    private double alpha = 0.01;
    private double gamma = 1.0;
    private Double dropProbability = 0.1;
    private static int antCounter = 0;
    private final Logger LOG = Logger.getLogger(AntColonyClusteringStrategy.class.toString());
    private long labelCounter = 0;

//    public AntColonyClusteringStrategy(Map<Long, Long> ResourceMaxTaskMapping){
//        this.ResourceMaxTaskMapping = ResourceMaxTaskMapping;
//    }

    @Override
    public List<Cluster> createClusters(float[][] similarityMatrix, double eps, int minPoints) {
        this.similarityMatrix = similarityMatrix;
        return null;
    }


    public List<Coalition> createCoalitions(List<Resource> Resources){
        if (Resources.size() < 1)
            return new ArrayList<Coalition>();
        Map<Long, List<Ant>> clusters = createClusters(Resources);
        LOG.info("Creating ro.ieat.soare.coalitions from clusters...");
        return computeCoalitions(clusters);
    }

    public Map<Long, Long> createClassifier(List<Resource> Resources){
        if (Resources.size() < 1)
            return new TreeMap<>();
        Map<Long, List<Ant>> clusters = createClusters(Resources);
        LOG.info("Creating classifier from clusters...");
        return computeClassifier(clusters);
    }

    private Map<Long, List<Ant>> createClusters(List<Resource> Resources) {

        List<Ant> ants = new ArrayList<>();
        for(Resource m : Resources){
            Ant a = new Ant(m);
            ants.add(a);
        }

        LOG.info("Learning threshold");
        antLearnThreshold(ants);
        LOG.info("Done.");

        LOG.info("Random meeting ants: " + ants.size());
        Map<Long, List<Ant>> clusters = randomMeetAnts(ants);
        LOG.info("Done.");
        Map<Long, Double> nClusters = new TreeMap<>();
        Map<Long, Double> averageMPlus = new TreeMap<>();
        List<Ant> freeAnts = new ArrayList<>();

        LOG.info("Processing clusters..." + clusters.size());
        for(Long label : clusters.keySet()){
            Double mPlus = .0;
            for(int i = 0; i < clusters.get(label).size(); i++){
                Ant ant = clusters.get(label).get(i);
                mPlus += ant.mPlus;
                for(int j = i + 1; j < clusters.get(label).size(); j++){
                    if(clusters.get(label).get(j).data.getId().equals(ant.data.getId())){
                        clusters.get(label).remove(j);
                        --j;
                    }

                }
            }
            mPlus /= clusters.get(label).size();

            Double nCluster = 1.0 * clusters.get(label).size()/ ants.size();
            Double acceptanceProbability = alpha * mPlus + (1 - alpha) * nCluster;
            if(acceptanceProbability < dropProbability){
                //freeAnts.addAll(clusters.get(label));
                //clusters.put(label, new ArrayList<>());
            }else {
                nClusters.put(label, nCluster);
                averageMPlus.put(label, mPlus);
            }
        }
        LOG.info("Processing free ants: " + freeAnts.size());
        for(Ant ant : freeAnts){
            ant.reset();
            Double maxSim = Double.MIN_VALUE;
            Integer maxSimAnt = 0;
            for(int antId = 0; antId < similarityMatrix.length; antId++){
                if(similarityMatrix[antId][antId] > maxSim){
                    maxSim = (double) similarityMatrix[antId][antId];
                    maxSimAnt = antId;
                }
            }
            Long label = ants.get(maxSimAnt).label;
            if(!clusters.containsKey(label))
                clusters.put(label, new ArrayList<>());
            clusters.get(label).add(ant);
            ant.label = label;
        }
        return clusters;
    }

    private Map<Long, Long> computeClassifier(Map<Long, List<Ant>> clusters){
        Map<Long, Long> result = new TreeMap<>();

        for(Long label : clusters.keySet()){
                for (Ant ant : clusters.get(label)) {
                    result.put(ant.data.getId(), label);
                }
        }

        return result;
    }

    private List<Coalition> computeCoalitions(Map<Long, List<Ant>> clusters) {
        List<Coalition> result = new ArrayList<>();


        for(Long label : clusters.keySet()){
            if(!label.equals(0L)) {
                int size = clusters.get(label).size();
                LOG.info("Cluster " + label + "; size: " + size);
                int processed = 0;
                while (processed < size - label) {
                    Coalition c = new Coalition();
                    c.setId(0);
                    c.setConfidenceLevel(1.0);
                    List<Resource> ResourceList = new ArrayList<>();
                    LOG.severe("Processed label size: " + processed + " " + label + " " + size);
                    int i;
                    for (i = processed; i < processed + label; i++) {
                        ResourceList.add(clusters.get(label).get(i).data);
                    }

                    c.setResources(ResourceList);
                    LOG.info("Coalition info: "  + c.getResources().size());
                    result.add(c);;
                    processed = i;
                }
                if(processed < size){
                    Coalition c = new Coalition();
                    c.setId(0);
                    c.setConfidenceLevel(1.0);
                    List<Resource> ResourceList = new ArrayList<>();
                    LOG.severe("Processed label size: " + processed + " " + label + " " + size);
                    int i = 0;

                    for (i = 0; i < clusters.get(label).size(); i++) {

                        boolean contains = false;
                        for(Resource m : ResourceList){
                            if(m.getId().equals(clusters.get(label).get(i).data.getId())) {
                                contains = true;
                                break;
                            }
                        }
                        if(!contains)
                            ResourceList.add(clusters.get(label).get(i).data);
                    }

                    int first = ResourceList.size();
                    //add Resources 'till cluster is full
                    for(int j = 0; j < clusters.get(0L).size() && ResourceList.size() < label; j++) {
                        //add Resource to coalition if cpu is at least as the first one's
                        for(int k = 0; k < first; k++) {
                            if(clusters.get(0L).get(j).data.getId().equals(ResourceList.get(k).getId()))
                                break;
                            if (clusters.get(0L).get(j).data.getCpu() >= ResourceList.get(k).getCpu()) {
                                ResourceList.add(clusters.get(0L).remove(j).data);
                                --j;
                                i++;
                                break;
                            }
                        }
                    }


                    c.setResources(ResourceList);
                    LOG.info("Resources in coalition: " + c.getResources().size());
                    result.add(c);;
                    processed += i;
                }

            }
        }
        for(Ant ant : clusters.get(0L)){
            Coalition c = new Coalition();
            c.setId(0);
            c.setConfidenceLevel(1.0);
            List<Resource> ResourceList = new ArrayList<>();
            ResourceList.add(ant.data);
            c.setResources(ResourceList);
            result.add(c);
        }
        LOG.info("Created " + result.size() + " ro.ieat.soare.coalitions!");
        return result;
    }

    public void learnThreshold(List<? extends ResourceAbstraction> resourceAbstractionList) {

        List<Ant> antList = new ArrayList<>();
        resourceAbstractionList.forEach(r ->{
            if(r instanceof Ant)
                antList.add((Ant) r);
        });

        antLearnThreshold( antList);
    }


    private Map<Long, List<Ant>> randomMeetAnts(List<Ant> ants) {
        Random r = new Random();
        int total = ants.size()*100;
        Map<Long, List<Ant>> clusters = new TreeMap<>();
        int acc = 0, rej = 0;
        for (int i = 0; i < total; i++){
            Integer first = r.nextInt(ants.size());
            //select ant to meet. it should have computed a similarity
            int second = r.nextInt(similarityMatrix[first].length);

            //Actual meeting
            Ant firstAnt = ants.get(first);
            Ant secondAnt = ants.get(second);
            firstAnt.meetingCounter ++;
            secondAnt.meetingCounter ++;
            float similarity = similarityMatrix[first][second];
            if(similarity > firstAnt.threshold && similarity > secondAnt.threshold){
                applyAcceptRules(firstAnt, secondAnt, clusters);
                acc ++;
            }else{
                applyRejectRules(firstAnt, secondAnt, clusters);
                rej++;
            }

        }
        LOG.info(String.format("Accept: %d/Reject: %d/Total: %d", acc, rej, total));
        for(Ant a : ants){
            clusters.putIfAbsent(a.label, new ArrayList<>());
            if(!clusters.get(a.label).contains(a)){
                clusters.get(a.label).add(a);
            }
        }
        return clusters;
    }

    private void applyRejectRules(Ant firstAnt, Ant secondAnt, Map<Long, List<Ant>> clusters) {
        if(!firstAnt.label.equals(0L) && secondAnt.label.equals(firstAnt.label)){
            if(firstAnt.mPlus < secondAnt.mPlus){
                moveAnt(firstAnt, secondAnt, clusters);
            }
            else{
                moveAnt(secondAnt, firstAnt, clusters);
            }
        }
    }

    private void moveAnt(Ant firstAnt, Ant secondAnt, Map<Long, List<Ant>> clusters) {
        clusters.get(firstAnt.label).remove(firstAnt);
        firstAnt.mPlus = .0;
        firstAnt.m = .0;
        firstAnt.label = 0L;
        secondAnt.m = increase(secondAnt.m);
        secondAnt.mPlus = decrease(secondAnt.mPlus);
    }

    private void applyAcceptRules(Ant firstAnt, Ant secondAnt, Map<Long, List<Ant>> clusters) {
        if(firstAnt.label.equals(0L) && secondAnt.label.equals(0L)){
            Long label = labelCounter++;
            firstAnt.label = label;
            secondAnt.label = label;
            if(!clusters.containsKey(label)) {
                clusters.put(label, new ArrayList<>());
            }
            clusters.get(label).add(firstAnt);
            clusters.get(label).add(firstAnt);
            return;
        }
        if(firstAnt.label.equals(0L) && !secondAnt.label.equals(0L)){
            firstAnt.label = secondAnt.label;
            clusters.get(secondAnt.label).add(firstAnt);
            return;
        }
        if(!firstAnt.label.equals(0L) && secondAnt.label.equals(0L)){
            secondAnt.label = firstAnt.label;
            clusters.get(firstAnt.label).add(secondAnt);
            return;
        }
        if(!firstAnt.label.equals(0L) && !secondAnt.label.equals(0L)){
            if(firstAnt.label.equals(secondAnt.label)){
                firstAnt.m = increase(firstAnt.m);
                secondAnt.m = increase(secondAnt.m);
                firstAnt.mPlus = increase(firstAnt.mPlus);
                secondAnt.mPlus = increase(secondAnt.mPlus);
            }
            else{
                if(firstAnt.m < secondAnt.m){
                    clusters.get(firstAnt.label).remove(firstAnt);
                    firstAnt.label = secondAnt.label;
                    clusters.get(secondAnt.label).add(firstAnt);
                    firstAnt.m = decrease(firstAnt.m);
                    secondAnt.m = decrease(secondAnt.m);
                }
                else{
                    clusters.get(secondAnt.label).remove(secondAnt);
                    secondAnt.label = firstAnt.label;
                    clusters.get(firstAnt.label).add(secondAnt);
                    firstAnt.m = decrease(firstAnt.m);
                    secondAnt.m = decrease(secondAnt.m);
                }

            }
        }


    }

    private Double decrease(Double m){
        return (1 - alpha) * m;
    }

    private Double increase(Double m){
        return (1 - alpha) * m + m;
    }

    private void antLearnThreshold(List<Ant> ants){
        int sampleSize = 500;

        for(int i = 0; i < ants.size(); i++){
            Ant a = ants.get(i);
            Double maxSimilarity = Double.MIN_VALUE;
            Double average = .0;
            int index = new Random().nextInt(ants.size() - sampleSize);
            for(int j = 0; j < sampleSize; j++){
                float similarity = similarityMatrix[i][j];

                average += similarity;
                if(similarity > maxSimilarity)
                    maxSimilarity = (double) similarity;
            }
            average /= sampleSize;
            a.threshold = (maxSimilarity + average) / 2;
        }

    }

//    private Double computeSimilarity(Ant a, Ant ant) {
//        Double similarity = .0;
////        LOG.info(a.data.getId() + " first id.");
////        LOG.info(ant.data.getId() + " second id.");
////        LOG.info(ResourceMaxTaskMapping.size() + " size.");
//        Long firstMax = 0L;
//        if(ResourceMaxTaskMapping.get(a.data.getId()) != null)
//            firstMax = ResourceMaxTaskMapping.get(a.data.getId());
//        Long secondMax = 0L;
//         if(ResourceMaxTaskMapping.get(ant.data.getId()) != null)
//             secondMax = ResourceMaxTaskMapping.get(ant.data.getId());
//        similarity = 1 - gamma * Math.abs(firstMax - secondMax)/ Math.max(firstMax, secondMax);
//        return similarity;
//    }

    private class Ant extends ResourceAbstraction {
        Resource data;
        Long label;
        Double threshold;
        Long meetingCounter;
        //nest size parameter
        Double m;
        //acceptance degree
        Double mPlus;
        Integer id;

        Ant(Resource m){
            this.id = antCounter++;
            this.data = m;
            label = 0L;
            threshold = .0;
            meetingCounter = 0L;
            this.m = .0;
            mPlus = .0;

        }

        public boolean equals(Object o){
            if(o instanceof Ant){
                Ant a = (Ant) o;
                return a.data.getId().equals(this.data.getId());
            }
            return false;
        }

        void reset(){
            label = 0L;
        }

    }
}
