package ro.ieat.soare.clustering.startegies;

import ro.ieat.soare.clustering.Cluster;

import java.util.*;
import java.util.logging.Logger;

/**
 * Created by adispataru on 6/3/2016.
 * This class implements {@link ClusteringStrategy} using the DBSCAN algorithm.
 */
public class DBSCANClusteringStrategy implements ClusteringStrategy {

    private static final Logger LOG = Logger.getLogger("DBSCAN");
    /**
     * This method creates the clusters using DBSCAN algorithm
     * @param similarityMatrix - the similarity matrix used for clustering
     * @param eps the threshold for grouping based on the similarity matrix
     * @param minPts the minimum number neighbors in a cluster
     * @return a {@link List< Cluster >} with the identified clusters.
     */
    @Override
    public List<Cluster> createClusters(float[][] similarityMatrix, double eps, int minPts) {

        List<Cluster> result = dbScanMatrix(similarityMatrix, (float) eps, minPts);
        LOG.info("Created " + result.size() + "clusters");

        //cluster
        return result;
    }

    private List<Cluster> dbScanMatrix(float[][] similarityMatrix, float eps, int minPts){
        Cluster[] clusters = new Cluster[similarityMatrix.length];
        boolean[] visited = new boolean[similarityMatrix.length];
        boolean[] noise = new boolean[similarityMatrix.length];
        Map<Integer, Long> classifier = new HashMap<>(similarityMatrix.length);
        int clusterCounter = 0;
        for(int i = 0; i < similarityMatrix.length; i++){
            if(visited[i])
                continue;
//            System.out.println("Clustering " + i + " out of " + similarityMatrix.length);
            visited[i] = true;
            Queue<Integer> epsNeigh = getEpsilonNeighborhoodMatrix(similarityMatrix[i], eps);
            if(epsNeigh.size() < minPts){
//                LOG.info("P is noise " + i);
                noise[i] = true;
            }else{
                Cluster c = new Cluster();
                expandClusterMatrix(i, epsNeigh, c, eps, minPts, visited, classifier, similarityMatrix);
                clusters[clusterCounter] = c;
                clusterCounter++;
            }
        }
        List<Cluster> result = new ArrayList<>();
        for (Cluster cluster1 : clusters) {
            if (cluster1 != null) {
                result.add(cluster1);
            } else {
                break;
            }
        }

        //add noisy points to their own cluster;
        for(int i = 0; i < noise.length; i++){
            if(noise[i]){
                Cluster cluster = new Cluster();
                cluster.getPoints().add(i);
                result.add(cluster);
            }
        }
        return result;
    }


//    private List<Cluster> dbScan(Map<Long, List<ResourceSimilarity>> similarityMap, float eps, int minPts) {
//        List<Cluster> clusters = new ArrayList<>();
//        Map<Long, Boolean> visited = new TreeMap<>();
//        Map<Long, Boolean> noise = new TreeMap<>();
//        Map<Long, Long> classifier = new TreeMap<>();
//
//
//        int i = 0;
//        for(Long key : similarityMap.keySet()){
//            LOG.info("Clustering " + i + " out of " + similarityMap.size());
//            if(visited.get(key) != null)
//                continue;
//            visited.put(key, true);
//            List<Long> epsNeigh = getEpsilonNeighborhood(key, similarityMap.get(key), eps);
//            if(epsNeigh.size() < minPts){
//                LOG.info("P is noise " + key);
//                noise.put(key, true);
//            }else{
//                Cluster<Long> c = new Cluster<>();
//                clusters.add(c);
//                expandCluster(key, epsNeigh, c, eps, minPts, visited, classifier, similarityMap);
//            }
//            i++;
//
//        }
//        return clusters;
//    }
//
//    private void expandCluster(Long key, List<Long> epsNeigh, Cluster<Long> c, float eps, int minPts,
//                               Map<Long, Boolean> visited, Map<Long, Long> classifier, Map<Long, List<ResourceSimilarity>> similarityMap) {
//
//
//        c.getPoints().add(key);
//        classifier.put(key, c.getId());
//        for (int i = 0; i < epsNeigh.size(); i++){
//            Long p = epsNeigh.get(i);
//            if(visited.get(p) == null){
//                visited.put(p, true);
//                List<Long> newNeigh = getEpsilonNeighborhood(p, similarityMap.get(p), eps);
//                if(newNeigh.size() > minPts){
//                    epsNeigh.addAll(newNeigh);
//                }
//
//
//            }else{
//                if(classifier.get(p) == null){
//                    c.getPoints().add(p);
//                    classifier.put(p, c.getId());
//                }
//            }
//        }
//    }

    private void expandClusterMatrix(int id, Queue<Integer> epsNeigh, Cluster c, float eps, int minPts,
                               boolean[] visited, Map<Integer, Long> classifier, float[][] similarityMap) {


        c.getPoints().add(id);

        classifier.put(id, c.getId());
        while (!epsNeigh.isEmpty()){
            //            Long cp = idList.get(i);
            int p = epsNeigh.remove();

            if(!visited[p]){
                visited[p] = true;
                Queue<Integer> newNeigh = getEpsilonNeighborhoodMatrix(similarityMap[p], eps);
                if(newNeigh.size() > minPts){
                    epsNeigh = mergeQueues(epsNeigh, newNeigh);
                }


            }else{
                if(classifier.get(p) == null){
                    c.getPoints().add(p);
                    classifier.put(p, c.getId());
                }
            }
        }
    }

    private Queue<Integer> getEpsilonNeighborhoodMatrix(float[] similarityArray, float eps) {
        Queue<Integer> result = new LinkedList<>();
        for(int j = 0; j < similarityArray.length; j++){
            if(similarityArray[j] > eps){
                result.add(j);
            }
        }
        return result;
    }

    private Queue<Integer> mergeQueues(Queue<Integer> q1, Queue<Integer> q2){
        Set<Integer> queueSet = new HashSet<>(q1.size() + q2.size());
        queueSet.addAll(q1);
        queueSet.addAll(q2);
        return new LinkedList<>(queueSet);
    }

}
