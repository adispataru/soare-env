package ro.ieat.soare.clustering.startegies;

import ro.ieat.soare.clustering.Cluster;

import java.util.List;

/**
 * Created by adispataru on 25-Jun-16.
 */
public interface ClusteringStrategy {
    /**
     * This method creates the clusters using DBSCAN algorithm
     * @param similarityMatrix - the similarity matrix used for clustering
     * @param eps the threshold for grouping based on the similarity matrix
     * @param minPoints the minimum number neighbors in a cluster
     * @return a {@link List< Cluster >} with the identified clusters.
     */
    List<Cluster> createClusters(float[][] similarityMatrix, double eps, int minPoints);
}
