package ro.ieat.soare.clustering;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.ieat.soare.clustering.ensemble.*;
import ro.ieat.soare.clustering.similarity.SimilarityStrategyType;
import ro.ieat.soare.clustering.startegies.DBSCANClusteringStrategy;
import ro.ieat.soare.io.Util;
import ro.ieat.soare.math.statistics.Histogram;
import ro.ieat.soare.repositories.JobRepository;
import ro.ieat.soare.repositories.ResourceRepository;
import ro.ieat.soare.repositories.TaskUsageRepository;
import ro.ieat.soare.repositories.util.DataAggregator;
import ro.ieat.soare.scheduling.JobPlanner;
import ro.ieat.soso.core.coalitions.Resource;
import ro.ieat.soso.core.config.Configuration;
import ro.ieat.soso.core.jobs.Job;
import ro.ieat.soso.core.jobs.ScheduledJob;
import ro.ieat.soso.core.jobs.TaskUsage;

import java.util.*;
import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * Created by adispataru on 5/12/2016.
 */
@Service
public class CoalitionClassifier {
    @Autowired
    JobRepository jobRepository;

    @Autowired
    ResourceRepository resourceRepository;

    @Autowired
    DataAggregator aggregator;

    @Autowired
    TaskUsageRepository taskUsageRepository;


    private Map<Long, Long> classifier = new TreeMap<>();
    private static final Logger LOG =Logger.getLogger("Coalition classifier");

//    public void instantiateMultiStepClustering(long time){
//
//
//        List<Resource> Resources = resourceRepository.findAll();
//        List<Cluster> propClusters = createPropertiesDBSCANClusters(Resources);
//        List<Cluster> secondHandClusters = new ArrayList<>();
//
//        Long startTime = -1L;
//        Long endTime = time * Configuration.TIME_DIVISOR + 1;
//
//        Map<String, Histogram<String>> values = aggregator.createAttributeHistogram(startTime, endTime);
//        LOG.info("Attribute histogram size: " + values.size());
//        for(Cluster c : propClusters){
//            //inside each cluster, apply attribute based clustering
//            LOG.info("Clustering " + c.getId() + " of size " + c.getPoints().size() + " based on attributes");
//            List<Resource> currentResources = Resources.stream().filter(m -> c.getPoints().contains(m.getId()))
//                    .collect(Collectors.toList());
//            List<Cluster> clusterList = createAttributeDBSCANClusters(values, currentResources);
//            LOG.info("Created " + clusterList.size() + " clusters");
//            secondHandClusters.addAll(clusterList);
//        }
//
//    }

//    public void bootStrapSOSM(long time){
//        List<Resource> resources = resourceRepository.findAll();
//        Map<Long, Resource> resourceMap = new TreeMap<>();
//        resources.stream().map(m -> resourceMap.put(m.getId(), m));
//        Long startTime = -1L;
////        finishTime *= Configuration.TIME_DIVISOR;
//
//        Long jobStartTime = (time - 4 * Configuration.STEP) * Configuration.TIME_DIVISOR;
//        Long endTime = time * Configuration.TIME_DIVISOR + 1;
//
//        Histogram<String> attributeHistogram = aggregator.createAttributeHistogram(startTime, endTime);
//        Map<Long, Histogram<Long>> jobSizeHistogram = aggregator.createJobSizeHistogramPerMachine(jobStartTime, endTime);
////        List<Job> evaluationSet = jobRepository.findBySubmitTimeBetween(endTime - 2, endTime + 300* Configuration.TIME_DIVISOR);
//
//        SOSMBootStrapper sosmBootStrapper = new SOSMBootStrapper();
//        List<PRouter> pRouters =  sosmBootStrapper.bootStrapSOSM(resources, attributeHistogram, jobSizeHistogram);
//
//        Util.writeSOSMLayout(pRouters);
//    }

    public void instantiateMultiStepClustering(long time, long finishTime){


        List<Resource> Resources = resourceRepository.findAll();
        Map<Long, Resource> ResourceMap = new TreeMap<>();
        Resources.stream().map(m -> ResourceMap.put(m.getId(), m));
        Long startTime = -1L;
        finishTime *= Configuration.TIME_DIVISOR;
        Long jobStartTime = (time - 4 * Configuration.STEP) * Configuration.TIME_DIVISOR;
        Long endTime = time * Configuration.TIME_DIVISOR + 1;

        Histogram<String> values = aggregator.createAttributeHistogram(startTime, endTime);
        Map<Long, Histogram<Long>> jobSizeHistogram = aggregator.createJobSizeHistogramPerMachine(jobStartTime, endTime);
        List<Job> evaluationSet = jobRepository.findBySubmitTimeBetween(endTime - 2, endTime + 300* Configuration.TIME_DIVISOR);

        MultiStepClusterEnsemble msce = new MultiStepClusterEnsemble(new DBSCANClusteringStrategy(), evaluationSet);
        SimilarityStrategyType str = SimilarityStrategyType.ATTRIBUTE;
        msce.addSimilarityStrategyStep(str);
        CoalitionDivision cDivision = msce.createClustersByEnsemble(Resources, values, jobSizeHistogram);

        endTime = endTime + 600*Configuration.TIME_DIVISOR;
        Histogram<Integer> jobSizeOutputHistogram = new Histogram<>();
        while (endTime < finishTime) {
            //assess usages first
            Map<Long, TaskUsage> usages = new TreeMap<>();
            long lowTime = endTime - 300 * Configuration.TIME_DIVISOR - 1;

//            Stream<TaskUsage> tasksStream = Streams.from(taskUsageRepository.findByStartTimeGreaterThanAndEndTimeLessThan(lowTime, time));
//            tasksStream.filter(t -> t.getResourceId() != 0).groupBy(TaskUsage::getResourceId).flatMap(longTaskUsageGroupedStream ->
//                    longTaskUsageGroupedStream.buffer()
//                            .map((taskUsages ->
//                                    computeTaskUsage(lowTime, taskUsages, ResourceMap)))).consume(tu -> usages.put(tu.getResourceId(), tu));
//
//            Map<Integer, Double> coalitionsLoad = new TreeMap<>();
//            Map<Integer, Double> coalitionsMaxLoad = new TreeMap<>();
//            int i = 0;
//            for(CoalitionPool cp : cDivision.getPools()){
//                for(Coalition c : cp.getAll()){
//                    if(c.getId() == 0)
//                        c.setId(++i);
//
//                    double avgLoad = 0;
//                    double maxLoad = 0;
//                    for(Resource m : c.getResources()){
//                        double load = usages.get(m.getId()) != null ? usages.get(m.getId()).getCpu() : 0;
//                        avgLoad += load;
//                        if(maxLoad < load)
//                            maxLoad = load;
//                    }
//                    System.out.println("Avg load: " + avgLoad + " msize: " + c.getResources().size());
//                    avgLoad /= c.getResources().size();
//                    coalitionsLoad.put(i, avgLoad);
//                    coalitionsMaxLoad.put(i, maxLoad);
//                }
//
//            }

//            Util.writeCoalitionStatus(endTime, coalitionsLoad, coalitionsMaxLoad);
            Util.writeCoalitionStatus(lowTime, cDivision);
            AtomicLong sentJobs = new AtomicLong(0);
            AtomicLong scheduledJobs = new AtomicLong(0);
            AtomicLong sentTasks = new AtomicLong(0);
            AtomicLong scheduledTasks = new AtomicLong(0);

            JobPlanner jobPlanner = new JobPlanner();

            evaluationSet = jobRepository.findBySubmitTimeBetween(lowTime, endTime);

            evaluationSet.stream().forEach(job -> {
                int size = job.getTaskHistory().size();
                sentJobs.getAndIncrement();
                sentTasks.getAndAdd(size);
                jobSizeOutputHistogram.add(size);
                List<CoalitionPool> pools = cDivision.getPoolOfSizeAtLeast(size);
                ScheduledJob scheduledJob = null;
                for (CoalitionPool pool : pools) {
                    switch (str) {
                        case ATTRIBUTE:
                            scheduledJob = jobPlanner.scheduledJobByAttribute(job, pool);
                            break;
                        case JOBSIZE:
                            scheduledJob = jobPlanner.scheduleJobUsingSize(job, pool, true);
                    }
                    if (scheduledJob != null)
                        break;
                }


                if (scheduledJob != null) {
                    scheduledJobs.getAndIncrement();
                    scheduledTasks.getAndAdd(size);
                }

            });


            double success =  ((double)scheduledJobs.get())/sentJobs.get();
            double taskSuccess =  ((double)scheduledTasks.get())/sentTasks.get();


            LOG.info("Job success: " + success);
            LOG.info("Task success: " + taskSuccess);
            Util.writeSuccessInformation(endTime, sentTasks, scheduledTasks, sentJobs, scheduledJobs);
            Util.writeCoalitionStatus(endTime, cDivision);
            jobPlanner.write(endTime);
            jobPlanner.reset();

            endTime += 300 * Configuration.TIME_DIVISOR;
        }

        Util.writeJobSizeHistogram(jobSizeOutputHistogram);

//        writePropClustersToFile(secondHandClusters);

    }

//    public void instantiateMultiStepClusteringFour(long time){
//
//
//        List<Resource> Resources = resourceRepository.findAll();
//        List<Cluster> propClusters = createPropertiesDBSCANClusters(Resources);
//        List<Cluster> secondHandClusters = new ArrayList<>();
//
//        Long startTime = -1L;
//        Long endTime = time * Configuration.TIME_DIVISOR + 1;
//
//        Map<Long, Histogram<Long>> jobSizeHistogram = aggregator.createJobSizeHistogramPerMachine(startTime, endTime);
//
//        for(Cluster c : propClusters){
//            //inside each cluster, apply jobSize based clustering
//            LOG.info("Clustering " + c.getId() + " of size " + c.getPoints().size() + " based on job Size");
//            List<Resource> currentResources = Resources.stream().filter(m -> c.getPoints().contains(m.getId()))
//                    .collect(Collectors.toList());
//            List<Cluster> clusterList = createJobSizeDBSCANClusters(jobSizeHistogram, currentResources);
//            LOG.info("Created " + clusterList.size() + " clusters");
//            secondHandClusters.addAll(clusterList);
//        }
//
//    }

    public void instantiateJobSizeClassifier(Long endTime, List<Resource> Resources){

        LOG.info("Instantiating classifier");
        Long startTime = -1L;
        long start = System.currentTimeMillis();
        Map<Long, Histogram<Long>> ResourceMapping = aggregator.createJobSizeHistogramPerMachine(startTime, endTime);


//        createLabels(ResourcePrefferedSizeMapping);
        createJobSizeDBSCANClusters(ResourceMapping, Resources);
        long elapsed = System.currentTimeMillis() - start;

//        latch.await();
        LOG.info("Elapsed time = " + elapsed + " ms for " + ResourceMapping.size() + " Resource  mappings");
        LOG.info("Average time per Resource = " + elapsed/(double) ResourceMapping.size());
    }

    public void instantiateJobSizeClassifier(Long time){

        LOG.info("Instantiating classifier");
//        Long startTime = (1500 - 1) * Configuration.TIME_DIVISOR;
        Long startTime = -1L;
        long start = System.currentTimeMillis();
        long endTime = time * Configuration.TIME_DIVISOR;
        Map<Long, Histogram<Long>> ResourceMapping = aggregator.createJobSizeHistogramPerMachine(startTime, endTime);
//        Util.writeResourceDistribution(ResourceMapping);

        Set<Long> necessary = ResourceMapping.keySet();
        List<Resource> Resources = resourceRepository.findAll().stream().filter(m -> necessary.contains(m.getId()))
                .collect(Collectors.toList());
        createJobSizeDBSCANClusters(ResourceMapping, Resources);
        long elapsed = System.currentTimeMillis() - start;

        LOG.info("Elapsed time = " + elapsed + " ms for " + ResourceMapping.size() + " Resource  mappings");
        LOG.info("Average time per Resource = " + elapsed/(double) ResourceMapping.size());
    }


    public void instantiatePropClassifier(long time){
        List<Resource> Resources = resourceRepository.findAll();
        createPropertiesDBSCANClusters(Resources);
    }

    public void instantiateAttrClassifier(long time){
        Long endTime = time + 1;
        Long startTime = -1L ;

        Histogram<String> values = aggregator.createAttributeHistogram(endTime, startTime);

//        LOG.info("Constraint attributes histogram of size: " + attributes.getHistogram().size());
//        System.out.println(attributes);
//        for(String s : values.keySet()){
//            LOG.info("Value histogram for Attribute: " + s);
//            System.out.println(values.get(s));
//        }
//        createAttributeDBSCANClusters(values, resourceRepository.findAllTurnedOnAtTime(time));
    }




    private List<Cluster> createJobSizeDBSCANClusters(Map<Long, Histogram<Long>> ResourceMapping, List<Resource> Resources){
        SimpleClusterEnsemble sce = new SimpleClusterEnsemble(new DBSCANClusteringStrategy());
        List<Cluster> clusters = sce.createClustersBasedOnJobSize(ResourceMapping, Resources);
        clusters.stream().forEach(c -> {
//            LOG.info("Cluster: " + c.getId());
//            LOG.info("Size: " + c.getPoints().size());
//            Util.writeClusterToFile(c, ResourceMapping);
        });
        return clusters;
    }

    private List<Cluster> createAttributeDBSCANClusters(Histogram<String> attrDistr, List<Resource> Resources){

        SimpleClusterEnsemble sce = new SimpleClusterEnsemble(new DBSCANClusteringStrategy());
        //        for(Cluster c : clusters){
////            writeAttrClusterToFile(c, attrDistr);
//            LOG.info("Cluster: " + c.getId());
//            LOG.info("Size: " + c.getPoints().size());
//        }
        return sce.createClusterBasedOnAttributes(attrDistr, Resources);
    }

    private List<Cluster> createPropertiesDBSCANClusters(List<Resource> Resources){
        SimpleClusterEnsemble sce = new SimpleClusterEnsemble(new DBSCANClusteringStrategy());

        return sce.createClusterBasedOnProperties(Resources);

    }



    public Long classifyResource(Long ResourceId){
        Long l =  classifier.get(ResourceId);
        return l != null ? l : 0L;
    }

    public static TaskUsage computeTaskUsage(Long startTime, List<TaskUsage> usageList, Map<Long, Resource> Resource){
        final int sampleSize = 30;

        TaskUsage[] total = new TaskUsage[sampleSize];
        for(int i = 0; i < sampleSize; i++){
            total[i] = new TaskUsage();
            total[i].setResourceId(usageList.get(0).getResourceId());
        }
        Double cpuCapacity = Resource.get(usageList.get(0).getResourceId()).getCpu();
        long overcommited = 0;
        for(TaskUsage taskUsage : usageList){
            long offset = 0;
            long thisTaskStart = (taskUsage.getStartTime() - startTime) / Configuration.TIME_DIVISOR;
            long thisTaskEnd = (taskUsage.getEndTime() - startTime) / Configuration.TIME_DIVISOR;
            int over = 0;
            for(long i = thisTaskStart + offset; i < thisTaskEnd + offset && i < sampleSize; i++){
                total[(int) i].addTaskUsage(taskUsage);
                if(total[(int) i].getCpu() > cpuCapacity)
                    over += 1;
            }

            overcommited += over;

        }


        TaskUsage result = new TaskUsage();
        for(int i = 0; i < sampleSize; i++){
            result.addTaskUsage(total[i]);
            if(result.getResourceId() == null)
                result.setResourceId(total[i].getResourceId());
        }
        result.divide(sampleSize);
        int i = usageList.size() - 1;
        long ocommited = 0;
//        while (result.getCpu() > cpuCapacity && i >= 0) {
//
////            if (usageList.get(i).getCpu() > 0.1) {
//            result.substractTaskUsage(usageList.get(i));
//            ocommited++;
////            }
//
//            i--;
//        }
//
//        if(overcommited > 0) {
//            LOG.info("Overcommited: " + overcommited);
//        }
//        if(ocommited > 0) {
//            LOG.info("Substracting Overcommited: " + ocommited);
//        }
//        avgLoad += result.getCpu()/cpuCapacity;
        return result;
    }

}
