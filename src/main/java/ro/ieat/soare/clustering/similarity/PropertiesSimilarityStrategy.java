package ro.ieat.soare.clustering.similarity;

import ro.ieat.soare.io.Util;
import ro.ieat.soare.math.statistics.distance.DistributionDistance;
import ro.ieat.soare.math.statistics.distance.PropertyDistance;
import ro.ieat.soso.core.coalitions.Resource;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Logger;


/**
 * Created by adispataru on 6/3/2016.
 * This class is responsible for computing the similarity matrix of a set of Resources based on their properties.
 * By properties we refer to CPU and MEM.
 */
public class PropertiesSimilarityStrategy implements SimilarityStrategy {

    private static final Logger LOG = Logger.getLogger("PropertiesSimilarity");
    private Map<Long, Integer> matrixIdMap;
    private List<Resource> Resources;
    private double averageSimilarity;
    private static final String similarityFilepath = "./data/props-similarity.bin";

    public PropertiesSimilarityStrategy(List<Resource> Resources){
        this.Resources = Resources;
        this.averageSimilarity = 0.0;
    }


    public float[][] createSimilarityMatrix(){

        File f = new File(similarityFilepath);
        if(f.exists()) {
            long start = System.currentTimeMillis();
            float[][] result = Util.readPrecomputedSimilarityMatrix(f, Resources.size());
            LOG.info("Total time for read(ms): " + (System.currentTimeMillis() - start));
            return result;
        }

        LOG.info("Computing similarity");
        int length = Resources.size();
        LOG.info("Matrix Size: " + length);
        matrixIdMap = new HashMap<>(length);
        long start = System.currentTimeMillis();
        int counter = 0;
        for(Resource m : Resources){
            matrixIdMap.put(m.getId(), counter);
            counter++;
        }

        float[][] similarityMatrix = new float[length][];
        for(int s = 0; s < length; s++){
            similarityMatrix[s] = new float[length];
        }


        int cores = Runtime.getRuntime().availableProcessors();
        LOG.info("Fixing number of threads equal to number of cores = " + cores);
        ExecutorService executorService = Executors.newFixedThreadPool(cores);
        AtomicInteger sims = new AtomicInteger(0);
        for(int ii = 0; ii < length; ii++) {
            final int i = ii;
            executorService.execute(() -> {
                for (int j = 0; j < length; j++) {
                    double distance = PropertyDistance.computePropertiesDistance(Resources.get(i),
                            Resources.get(j));

                    similarityMatrix[i][j] = (float) DistributionDistance.applyGaussianKernel(distance);
//                similarityMatrix[i][j] = distance;
                    averageSimilarity += similarityMatrix[i][j];
                    sims.getAndIncrement();
                }
                if (length > 20 && i % (length / 4) == 0) {
                    LOG.info(i / (float) length + "%");
                }
            });
        }

        executorService.shutdown();
        try {
            executorService.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        averageSimilarity /= sims.get();
        long total = System.currentTimeMillis() - start;
        LOG.info("Total time for computing(ms): " + total);
        //writeComputedSimilarityMatrix(similarityMatrix);

        return similarityMatrix;
    }


    public double getAverageSimilarity() {
        return averageSimilarity;
    }

    @Override
    public Map<Long, Integer> getMatrixIdMap() {
        return matrixIdMap;
    }

    @Override
    public float getEpsilon(){
        return (float) 0.99;
    }
}
