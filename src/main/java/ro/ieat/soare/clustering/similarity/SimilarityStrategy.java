package ro.ieat.soare.clustering.similarity;

import java.util.Map;

/**
 * Created by adispataru on 25-Jun-16.
 */
public interface SimilarityStrategy {
    float[][] createSimilarityMatrix();
    double getAverageSimilarity();
    Map<Long, Integer> getMatrixIdMap();
    float getEpsilon();
}
