package ro.ieat.soare.clustering.similarity;

/**
 * Created by adispataru on 26-Jun-16.
 */
public enum SimilarityStrategyType {
    ATTRIBUTE,
    PROPERTY,
    JOBSIZE
}
