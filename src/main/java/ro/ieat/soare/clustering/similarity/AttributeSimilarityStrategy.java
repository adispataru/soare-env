package ro.ieat.soare.clustering.similarity;

import reactor.rx.Streams;
import ro.ieat.soare.clustering.similarity.SimilarityStrategy;
import ro.ieat.soare.math.statistics.Histogram;
import ro.ieat.soare.math.statistics.distance.AttributeDistance;
import ro.ieat.soso.core.coalitions.Resource;

import java.io.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Logger;

import static ro.ieat.soare.io.Util.readPrecomputedSimilarityMatrix;


/**
 * Created by adispataru on 6/3/2016.
 */
public class AttributeSimilarityStrategy implements SimilarityStrategy {

    private final Histogram<String> attributesDistribution;
    private static final Logger LOG = Logger.getLogger("Attribute Similarity");
    private Map<Long, Integer> matrixIdMap;
    private List<Resource> Resources;
    private double averageSimilarity;
    private static final String similarityFilepath = "./data/similarity.bin";
    private double maxSim;

    public AttributeSimilarityStrategy(Histogram<String> attrsDistribution, List<Resource> Resources){
        this.attributesDistribution = attrsDistribution;
        this.Resources = Resources;
        this.averageSimilarity = 0.0;
    }


    public float[][] createSimilarityMatrix(){

        File f = new File(similarityFilepath);
        if(f.exists()) {
            long start = System.currentTimeMillis();
            float[][] result = readPrecomputedSimilarityMatrix(f, Resources.size());
            long total = System.currentTimeMillis() - start;
            LOG.info("Total time for read(ms): " + total);
            return result;
        }

        LOG.info("Computing attribute similarity");
        int length = Resources.size();
        LOG.info("Matrix Size: " + length);
        matrixIdMap = new HashMap<>(length);
        long start = System.currentTimeMillis();
        int counter = 0;
        for(Resource m : Resources){
            matrixIdMap.put(m.getId(), counter);
            counter++;
        }

        float[][] similarityMatrix = new float[length][];
        for(int s = 0; s < length; s++){
            similarityMatrix[s] = new float[length];
        }


        AtomicInteger sims = new AtomicInteger(0);
        maxSim = Double.MIN_VALUE;
        AtomicInteger ind = new AtomicInteger(0);
        int cores = Runtime.getRuntime().availableProcessors();
        LOG.info("Fixing number of threads equal to number of cores = " + cores);
        ExecutorService executorService = Executors.newFixedThreadPool(cores);

//        Streams.from(Resources).consume( m -> {
//            int i = ind.getAndIncrement();
        for(int ii = 0; ii < length; ii++) {
            final int i = ii;
            executorService.execute(() -> {
                        for (int j = 0; j < length; j++) {
                            float similarity = AttributeDistance.computeAttributesSimilarity(Resources.get(i).getAttributes(),
                                    Resources.get(j).getAttributes(), attributesDistribution);

                            similarityMatrix[i][j] = similarity;
                            averageSimilarity += similarity;
                            if (maxSim < similarity)
                                maxSim = similarity;
                            sims.getAndIncrement();
                        }
                        if (length > 4 && i % (length / 4) == 0) {
                            LOG.info(i / (float) length + "%");
                        }
                    }
            );
        }
        executorService.shutdown();
        try {
            executorService.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        averageSimilarity /= sims.get();
        long total = System.currentTimeMillis() - start;
        LOG.info("Total time for computing(ms): " + total);
        //writeComputedSimilarityMatrix(similarityMatrix);

        return similarityMatrix;
    }

    public double getAverageSimilarity() {
        return averageSimilarity;
    }

    public Map<Long, Integer> getMatrixIdMap() {
        return matrixIdMap;
    }

    @Override
    public float getEpsilon() {
        return (float) ( 1.15*((averageSimilarity + maxSim)/2));
    }
}
