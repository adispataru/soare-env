package ro.ieat.soare.clustering.similarity;

import ro.ieat.soare.math.statistics.Histogram;
import ro.ieat.soare.math.statistics.distance.DistributionDistance;
import ro.ieat.soso.core.coalitions.Resource;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Logger;

/**
 * Created by adispataru on 6/3/2016.
 */
public class JobSizeSimilarityStrategy implements SimilarityStrategy {

    private static final Logger LOG = Logger.getLogger("Job Size Similarity");
    private final Map<Long, Histogram<Long>> resourcesDistribution;
    private List<Resource> resources;
    private double averageSimilarity;
    private Map<Long, Integer> matrixIdMap;

    public JobSizeSimilarityStrategy(Map<Long, Histogram<Long>> resourcesDistribution, List<Resource> currentResources) {
        this.resourcesDistribution = resourcesDistribution;
        this.resources = currentResources;
        averageSimilarity = 0.0;
    }


    public float[][] createSimilarityMatrix(){

        LOG.info("Computing job size similarity");
        int length = resources.size();
        LOG.info("Matrix Size: " + length);


        float[][] similarityMatrix = new float[length][];
        for(int s = 0; s < length; s++){
            similarityMatrix[s] = new float[length];
        }
        matrixIdMap = new TreeMap<>();

        long start = System.currentTimeMillis();
        final AtomicInteger sims = new AtomicInteger(0);
        final AtomicInteger notFound = new AtomicInteger(0);
        final AtomicInteger ind = new AtomicInteger(0);

        int cores = Runtime.getRuntime().availableProcessors();
        LOG.info("Fixing number of threads equal to number of cores = " + cores);
        ExecutorService executorService = Executors.newFixedThreadPool(cores);

        for(Resource m : resources) {
            Long mId = m.getId();
            int i = ind.getAndIncrement();
            matrixIdMap.put(resources.get(i).getId(), i);
            executorService.execute(() -> {
                for (int j = 0; j < length; j++) {
                    Long mId2 = resources.get(j).getId();
                    if (resourcesDistribution.get(mId) != null && resourcesDistribution.get(mId2) != null) {
                        double distance = DistributionDistance
                                .computeKLDivergence(resourcesDistribution.get(mId).getProbabilityDistribution(),
                                        resourcesDistribution.get(mId2).getProbabilityDistribution());
                        similarityMatrix[i][j] = (float) DistributionDistance.applyGaussianKernel(distance);
                        averageSimilarity += similarityMatrix[i][j];
                    } else {
                        notFound.getAndIncrement();
                        similarityMatrix[i][j] = 0;
                    }
                    sims.getAndIncrement();
                }
            });
            if(length > 4 &&  i % (length / 4) == 0 ){
                LOG.info(i / (float) length + "%");
            }

        }
        executorService.shutdown();
        try {
            executorService.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        notFound.set(notFound.get()/length);
        LOG.info("Records not found " + notFound);
        averageSimilarity /= sims.get();

        long total = System.currentTimeMillis() - start;
        LOG.info("Total time for computing(ms): " + total);

        return similarityMatrix;
    }

    public double getAverageSimilarity() {
        return averageSimilarity;
    }

    public void setAverageSimilarity(double averageSimilarity) {
        this.averageSimilarity = averageSimilarity;
    }

    @Override
    public Map<Long, Integer> getMatrixIdMap() {
        return matrixIdMap;
    }

    @Override
    public float getEpsilon() {
        return (float) (averageSimilarity);
    }
}
