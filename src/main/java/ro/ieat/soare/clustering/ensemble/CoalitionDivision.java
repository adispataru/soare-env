package ro.ieat.soare.clustering.ensemble;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by adispataru on 05-Jul-16.
 */
public class CoalitionDivision {
    private List<CoalitionPool> pools;

    public List<CoalitionPool> getPoolOfSizeAtLeast(int size){
        return pools.stream().filter(p -> p.getTotalSize() > size).collect(Collectors.toList());
    }

    public void addCoalitionPool(CoalitionPool c){
        pools.add(c);
    }

    public CoalitionDivision(){
        pools = new ArrayList<>();
    }

    public List<CoalitionPool> getPools() {
        return pools;
    }

    public void setPools(List<CoalitionPool> pools) {
        this.pools = pools;
    }

}
