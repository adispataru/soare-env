package ro.ieat.soare.clustering.ensemble;

import ro.ieat.soare.clustering.Cluster;
import ro.ieat.soare.clustering.similarity.*;
import ro.ieat.soare.clustering.startegies.ClusteringStrategy;
import ro.ieat.soare.io.Util;
import ro.ieat.soare.math.statistics.Histogram;
import ro.ieat.soare.math.statistics.distance.DistributionDistance;
import ro.ieat.soare.scheduling.JobPlanner;
import ro.ieat.soare.scheduling.JobScheduler;
import ro.ieat.soso.core.coalitions.Coalition;
import ro.ieat.soso.core.coalitions.Resource;
import ro.ieat.soso.core.jobs.Job;
import ro.ieat.soso.core.jobs.ScheduledJob;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * Created by adispataru on 26-Jun-16.
 */
public class MultiStepClusterEnsemble {
    private ClusteringStrategy clusteringStrategy;
    private List<SimilarityStrategyType> similarityStrategies;
    private static final Logger LOG = Logger.getLogger("TwoStepClustering");
    private final List<Job> evaluationSet;
    private final Histogram<Integer> jobRequestHistogram;
    private Map<Long, Histogram<Long>> ResourceDistribution;

    public MultiStepClusterEnsemble(ClusteringStrategy cs, List<Job> evaluationSet){
        this.clusteringStrategy = cs;
        this.similarityStrategies = new ArrayList<>(2);
        this.evaluationSet = evaluationSet;
        this.jobRequestHistogram = new Histogram<>();
        evaluationSet.forEach(job -> {
            int size = job.getTaskHistory().size();
            jobRequestHistogram.add(size);
        });
    }

    public CoalitionDivision createClustersByEnsemble(List<Resource> Resources,
                                                  Histogram<String> attrDistr,
                                                  Map<Long, Histogram<Long>> resourceDistribution){

        long startTime = System.currentTimeMillis();

        this.ResourceDistribution = resourceDistribution;
        CoalitionDivision coalitionDivision = new CoalitionDivision();

//        for(int i = 0; i < similarityStrategies.size() - 1; i++) {
//            SimilarityStrategyType str = similarityStrategies.get(i);
        SimilarityStrategyType str = SimilarityStrategyType.PROPERTY;
            LOG.info("Clustering by strategy: " + str.name());
            SimilarityStrategy similarityStrategy = getSimilarityStrategy(str, Resources, attrDistr, resourceDistribution);

            float[][] similarityMatrix = similarityStrategy.createSimilarityMatrix();
            Map<Long, Integer> firstMatrixIdMap = similarityStrategy.getMatrixIdMap();

            float epsR = similarityStrategy.getEpsilon();
            int minPts = 2;

//            for(double delta = 0.8; delta <= 1.1; delta += 0.1) {
            double delta = 1.0;
                float eps = (float) (epsR * delta);

                List<Cluster> firstClusters = clusteringStrategy.createClusters(similarityMatrix, eps, minPts);

                LOG.info("Created " + firstClusters.size() + " clusters");


//                for (int j = i+1; j< similarityStrategies.size(); j++) {
                    SimilarityStrategyType str2 = similarityStrategies.get(0);
                    LOG.info("Clustering by " + str2.name() + " inside " + str.name());
                    String filename = String.format("%s-%s-%.4f", str.name(), str2.name(), eps);
                    int step = 0;
                    Util.writeClusteringStep(firstClusters, similarityMatrix, step, System.currentTimeMillis() - startTime,
                            filename);
                    step++;
                    CoalitionPool pool;
                    List<Cluster> onePointClusters = new ArrayList<>();
                    for (Cluster c : firstClusters) {
                        startTime = System.currentTimeMillis();
                        //inside each cluster, apply jobSize based clustering
                        List<Resource> currentResources = Resources.stream().filter(m -> c.getPoints().contains(firstMatrixIdMap.get(m.getId())))
                                .collect(Collectors.toList());
                        LOG.info("Clustering " + c.getId() + " of size " + c.getPoints().size()
                                + "for strategy: " + similarityStrategy.getClass().getName());

                        similarityStrategy = getSimilarityStrategy(str2, currentResources, attrDistr, resourceDistribution);

                        float[][] similarityMatrix2 = similarityStrategy.createSimilarityMatrix();
                        float eps2 = similarityStrategy.getEpsilon();


                        List<Cluster> clusterList = clusteringStrategy.createClusters(similarityMatrix2, eps2, minPts);

                        Map<Long, Integer> secondMatrixIdMap = similarityStrategy.getMatrixIdMap();

                        Util.writeClusteringStep(clusterList, similarityMatrix2, step, System.currentTimeMillis() - startTime, filename);
                        LOG.info("Created " + clusterList.size() + " clusters");
                        pool = createCoalitionFromClusters(clusterList, secondMatrixIdMap, currentResources, str2);

//                        coalitionList.stream().forEach(coal -> {
//                            coalitionTable.putIfAbsent(coal.getResources().size(), new ArrayList<>());
//                            coalitionTable.get(coal.getResources().size()).add(coal);
//                        });
                        double score = computeClustersScore(pool, str);
                        coalitionDivision.addCoalitionPool(pool);


                    }
//                    coalitionTable.putIfAbsent(1, new ArrayList<>());
//                    oneResourceCoals.stream().forEach(coal -> coalitionTable.get(1).add(coal));
//                    double divergence = computeClustersDivergence(pool);
//                    double distance = computeClustersBDistance(pool);
//                    Util.writeClusterScore(str.name(), str2.name(), eps, score, divergence, distance);

//                }
//            }
//        }
        return coalitionDivision;
    }

    private double computeClustersDivergence(Map<Integer, List<Coalition>> coalitionTable) {

        Histogram<Integer> clusterSizeHistogram = new Histogram<>();
        for(Integer key : coalitionTable.keySet()){
            for(int i = 0; i < coalitionTable.get(key).size(); i++){
                clusterSizeHistogram.add(key);
            }
        }

        Map<Integer, Double> jobPD = jobRequestHistogram.getProbabilityDistribution();
        Map<Integer, Double> clusterPD = clusterSizeHistogram.getProbabilityDistribution();
        return DistributionDistance.computeKLDivergence(jobPD, clusterPD);

    }

    private double computeClustersBDistance(Map<Integer, List<Coalition>> coalitionTable) {

        Histogram<Integer> clusterSizeHistogram = new Histogram<>();
        for(Integer key : coalitionTable.keySet()){
            for(int i = 0; i < coalitionTable.get(key).size(); i++){
                clusterSizeHistogram.add(key);
            }
        }

        Map<Integer, Double> jobPD = jobRequestHistogram.getProbabilityDistribution();
        Map<Integer, Double> clusterPD = clusterSizeHistogram.getProbabilityDistribution();
        return DistributionDistance.computeBhattacharyyaDistance(jobPD, clusterPD);

    }

    private CoalitionPool createCoalitionFromClusters(List<Cluster> clusters, Map<Long, Integer> matrixIdMap, List<Resource> Resources, SimilarityStrategyType str) {

        Resource[] idList = new Resource[Resources.size()];
        Resources.stream().forEach(m -> idList[matrixIdMap.get(m.getId())] = m);
        switch (str){
            case ATTRIBUTE:
                return createAttributeCoalitions(clusters, idList);
            case JOBSIZE:
                return createJobSizeCoalitions(clusters, idList);
            default:
                return new CoalitionPool();
        }

    }

    private CoalitionPool createJobSizeCoalitions(List<Cluster> clusters, Resource[] idList) {
//        List<Coalition> coalitionList = new ArrayList<>();
        CoalitionPool coalitionPool = new CoalitionPool();
        List<Resource> noise = clusters.stream()
                .filter(c -> c.getPoints().size() == 1)
                .map(c -> idList[c.getPoints().get(0)])
                .collect(Collectors.toList());

        double expectedCPU = 0.0;
        double expectedMem = 0.0;
        for(Cluster c : clusters) {
            if(c.getPoints().size() == 1){
                continue;
            }
            Histogram<Integer> clusterSizeHistogram = new Histogram<>();
            Coalition coal = new Coalition();
            coal.setResources(new ArrayList<>());
            for (Integer i : c.getPoints()) {
                Resource Resource = idList[i];
                Histogram<Long> longHistogram = ResourceDistribution.get(Resource.getId());
                if(expectedCPU < Resource.getCpu())
                    expectedCPU = Resource.getCpu();
                if(expectedMem < Resource.getMemory())
                    expectedMem = Resource.getMemory();
                if(longHistogram == null)
                    continue;
                for (Long l : longHistogram.getHistogram().keySet()) {
                    clusterSizeHistogram.getHistogram().putIfAbsent(Math.toIntExact(l), new AtomicInteger());
                    clusterSizeHistogram.getHistogram().get(Math.toIntExact(l)).addAndGet(longHistogram.getHistogram().get(l).get());
                    if (clusterSizeHistogram.getMax().get() < clusterSizeHistogram.getHistogram().get(Math.toIntExact(l)).get()) {
                        clusterSizeHistogram.getMax().set(clusterSizeHistogram.getHistogram().get(Math.toIntExact(l)).get());
                        clusterSizeHistogram.setMaxId(Math.toIntExact(l));
                    }
                }
            }
            int clusterSize = c.getPoints().size();
            int label = clusterSizeHistogram.getMaxId() != null ? clusterSizeHistogram.getMaxId() : clusterSize;
            while(c.getPoints().size() >  label/2){
                if(label < clusterSize){
                    //full coalition
                    Coalition coalition = new Coalition();
                    for(int ii = 0; ii < label; ii++){
                        Resource m = idList[c.getPoints().remove(0)];
                        coalition.getResources().add(m);
                    }
                    coalitionPool.addCoalition(coalition);
                }else if(noise.size() > label - clusterSize){
                    //get from noise
                    Coalition coalition = new Coalition();
                    while (c.getPoints().size() > 0)
                        coalition.getResources().add(idList[c.getPoints().remove(0)]);

                    while (coalition.getResources().size() < label){
                        coalition.getResources().add(noise.remove(0));
                    }
                    coalitionPool.addCoalition(coalition);

                }
            }
            if(c.getPoints().size() > 0){
                noise.addAll(c.getPoints().stream()
                        .map(p -> idList[p])
                        .collect(Collectors.toList()));
            }

        }

        coalitionPool.setExpectedCPU(expectedCPU);
        coalitionPool.setExpectedMemory(expectedMem);

        Coalition noiseCoal = new Coalition();
        noiseCoal.getResources().addAll(noise);
        coalitionPool.setNoise(noiseCoal);

        return coalitionPool;
    }

    private CoalitionPool createAttributeCoalitions(List<Cluster> clusters, Resource[] idList) {

        CoalitionPool coalitionPool = new CoalitionPool();
        Coalition noise = new Coalition();
        for(Cluster c : clusters) {
            if(c.getPoints().size() == 1){
                noise.getResources().add(idList[c.getPoints().get(0)]);
                continue;
            }
            Coalition coal = new Coalition();
            coal.setResources(new ArrayList<>());
            Map<String, List<String>> attributeValues = new HashMap<>();
            for (Integer i : c.getPoints()) {
                Resource m = idList[i];
                coal.getResources().add(m);
                for(String key: m.getAttributes().keySet()){
                    attributeValues.putIfAbsent(key, new ArrayList<>());
                    attributeValues.get(key).add(m.getAttributes().get(key));
                }
            }
            coal.setAttributeValues(attributeValues);
            coalitionPool.addCoalition(coal);
        }
        coalitionPool.setExpectedCPU(idList[clusters.get(0).getPoints().get(0)].getCpu());
        coalitionPool.setExpectedMemory(idList[clusters.get(0).getPoints().get(0)].getMemory());
        coalitionPool.setNoise(noise);
        return coalitionPool;
    }

    private double computeClustersScore(CoalitionPool coalitionPool, SimilarityStrategyType str) {
        JobScheduler jobScheduler = new JobScheduler();
        AtomicLong sentJobs = new AtomicLong(0);
        AtomicLong scheduledJobs = new AtomicLong(0);

        JobPlanner jobPlanner = new JobPlanner();

        evaluationSet.stream().forEach(job -> {
            int size = job.getTaskHistory().size();
            sentJobs.getAndIncrement();
            ScheduledJob scheduledJob = null;

            switch (str){
                case ATTRIBUTE:
                    scheduledJob = jobPlanner.scheduledJobByAttribute(job, coalitionPool);
                    break;
                case JOBSIZE:
                    scheduledJob = jobPlanner.scheduleJobUsingSize(job, coalitionPool, true);
            }


            if (scheduledJob != null) {
                scheduledJobs.getAndIncrement();
            }

        });

        return ((double)scheduledJobs.get())/sentJobs.get();
    }



    private SimilarityStrategy getSimilarityStrategy(SimilarityStrategyType str, List<Resource> Resources,
                                                     Histogram<String> attrDistr,
                                                     Map<Long, Histogram<Long>> ResourceDistribution){
        SimilarityStrategy similarityStrategy;


        switch (str){
            case ATTRIBUTE:
                similarityStrategy = new AttributeSimilarityStrategy(attrDistr, Resources);
                break;
            case PROPERTY:
                similarityStrategy = new PropertiesSimilarityStrategy(Resources);
                break;
            case JOBSIZE:
                similarityStrategy = new JobSizeSimilarityStrategy(ResourceDistribution, Resources);
                break;
            default:
                similarityStrategy = new PropertiesSimilarityStrategy(Resources);
                break;

        }
        return similarityStrategy;
    }


    public void addSimilarityStrategyStep(SimilarityStrategyType e){
        similarityStrategies.add(e);
    }
}
