package ro.ieat.soare.clustering.ensemble;

import ro.ieat.soare.clustering.similarity.AttributeSimilarityStrategy;
import ro.ieat.soare.clustering.similarity.JobSizeSimilarityStrategy;
import ro.ieat.soare.clustering.similarity.PropertiesSimilarityStrategy;
import ro.ieat.soare.clustering.Cluster;
import ro.ieat.soare.clustering.startegies.ClusteringStrategy;
import ro.ieat.soare.math.statistics.Histogram;
import ro.ieat.soso.core.coalitions.Resource;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

/**
 * Created by adispataru on 26-Jun-16.
 */
public class SimpleClusterEnsemble implements ClusteringEnsemble{

    private static final Logger LOG = Logger.getLogger("SimpleClusterEnsemble");
    private ClusteringStrategy clusteringStrategy;

    public SimpleClusterEnsemble(ClusteringStrategy clusteringStrategy){
        this.clusteringStrategy = clusteringStrategy;
    }

    public List<Cluster> createClustersBasedOnJobSize(Map<Long, Histogram<Long>> ResourcesDistribution, List<Resource> Resources){

        LOG.info("Computing similarity");
        int length = Resources.size();
        LOG.info("Matrix Size: " + length);
        List<Long> idList = new ArrayList<>(length);
//        matrixIdMap = new HashMap<>(length);
        for(Resource m : Resources){
            idList.add(m.getId());
        }

        JobSizeSimilarityStrategy similarityStrategy = new JobSizeSimilarityStrategy(ResourcesDistribution, Resources);
        float[][] similarityMatrix = similarityStrategy.createSimilarityMatrix();

        double avgSim = similarityStrategy.getAverageSimilarity();
        float eps = (float) (avgSim + 0.01*avgSim);
//        float eps = (float) 0.75;
        LOG.info("Average Similarity: " + avgSim + "\nEpsilon: " + eps);
        int minPts = 2;
        List<Cluster> result = clusteringStrategy.createClusters(similarityMatrix, eps, minPts);
        LOG.info("Created " + result.size() + "clusters");

        //cluster
        return result;
    }

    public List<Cluster> createClusterBasedOnAttributes(Histogram<String> attrsDistribution, List<Resource> Resources){

        int length = Resources.size();
        List<Long> idList = new ArrayList<>(length);
        int counter = 0;

        AttributeSimilarityStrategy similarityStrategy = new AttributeSimilarityStrategy(attrsDistribution, Resources);
        float[][] similarityMatrix = similarityStrategy.createSimilarityMatrix();

        double avgSim = similarityStrategy.getAverageSimilarity();
        float eps = (float) (avgSim + 0.3 * avgSim);
        LOG.info(String.format("Average similarity:%.4f \nEpsilon: %.4f ", avgSim, eps));
        int minPts = 2;
        List<Cluster> result = clusteringStrategy.createClusters(similarityMatrix, eps, minPts);
        LOG.info("Created " + result.size() + "clusters");

        //cluster
        return result;
    }

    public List<Cluster> createClusterBasedOnProperties(List<Resource> Resources){

        int length = Resources.size();
        List<Long> idList = new ArrayList<>(length);
        int counter = 0;

        PropertiesSimilarityStrategy similarityStrategy = new PropertiesSimilarityStrategy(Resources);
        float[][] similarityMatrix = similarityStrategy.createSimilarityMatrix();

        double avgSim = similarityStrategy.getAverageSimilarity();
        float eps = (float) 0.99;
        LOG.info(String.format("Average similarity:%.4f \nEpsilon: %.4f ", avgSim, eps));
        int minPts = 2;
        List<Cluster> result = clusteringStrategy.createClusters(similarityMatrix, eps, minPts);
        LOG.info("Created " + result.size() + "clusters");

        //cluster

        return result;
    }
}
