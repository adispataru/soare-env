package ro.ieat.soare.clustering.ensemble;

import ro.ieat.soso.core.coalitions.Coalition;

import java.lang.invoke.WrongMethodTypeException;
import java.util.*;

/**
 * Created by adispataru on 05-Jul-16.
 */
public class CoalitionPool {
    private Map<Integer, List<Coalition>> coalitionMap;
    private List<Coalition> coalitions;
    private Coalition noise;
    private boolean sizeMap;
    private int totalSize;
    private double expectedCPU;
    private double expectedMemory;

    public CoalitionPool(){
        coalitionMap = new HashMap<>();
        coalitions = new ArrayList<>();
        sizeMap = true;
        totalSize = 0;
        expectedCPU = 0.0;
        expectedMemory = 0.0;
    }

    public CoalitionPool(boolean sizeMap){
        if(sizeMap)
            coalitionMap = new HashMap<>();
        coalitions = new ArrayList<>();
    }

    public void addCoalition(Coalition c){
        coalitions.add(c);
        totalSize+= c.getResources().size();
        if(sizeMap) {
            coalitionMap.putIfAbsent(c.getResources().size(), new ArrayList<>());
            coalitionMap.get(c.getResources().size()).add(c);
        }
    }

    public List<Coalition> getPoolOfSize(int size){
        if(!sizeMap)
            throw new WrongMethodTypeException("Cannot retrieve pool size for pool without index");
        return coalitionMap.get(size);
    }

    public int getMaxPoolSize(){
        return Collections.max(coalitionMap.keySet());
    }
    public List<Coalition> getAll(){
        return coalitions;
    }

    public Coalition getNoise() {
        return noise;
    }

    public void setNoise(Coalition noise) {
        this.noise = noise;
    }

    public int getTotalSize() {
        return totalSize;
    }

    public double getExpectedCPU() {
        return expectedCPU;
    }

    public void setExpectedCPU(double expectedCPU) {
        this.expectedCPU = expectedCPU;
    }

    public double getExpectedMemory() {
        return expectedMemory;
    }

    public void setExpectedMemory(double expectedMemory) {
        this.expectedMemory = expectedMemory;
    }
}
