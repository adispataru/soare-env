package ro.ieat.soare.resources;

import java.util.*;

import static org.apache.coyote.http11.Constants.a;

/**
 * Created by adispataru on 5/12/2016.
 */
public class ResourceJobHistory {
    public Long ResourceId;
    public List<Integer> jobSizes;

    public ResourceJobHistory(Long ResourceId){
        this.ResourceId = ResourceId;
        this.jobSizes = new ArrayList<>();
    }

    public Integer getPrefferedSize(){
        Map<Integer, Integer> map = new TreeMap<>();
        for (int i : jobSizes) {
            Integer count = map.get(i);
            map.put(i, count != null ? count+1 : 0);
        }
        return Collections.max(map.entrySet(),
                (o1, o2) -> o1.getValue().compareTo(o2.getValue())).getKey();
    }

    public Integer getMaxSize() {
        return Collections.max(jobSizes, Integer::compareTo);
    }
}
