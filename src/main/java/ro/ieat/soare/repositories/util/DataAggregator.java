package ro.ieat.soare.repositories.util;

import ie.ucc.cl.sosm.model.ResourceOffer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.rx.Stream;
import reactor.rx.Streams;
import ro.ieat.soare.math.statistics.Histogram;
import ro.ieat.soare.repositories.JobRepository;
import ro.ieat.soare.repositories.ResourceRepository;
import ro.ieat.soare.repositories.TaskUsageRepository;
import ro.ieat.soso.core.coalitions.Resource;
import ro.ieat.soso.core.config.Configuration;
import ro.ieat.soso.core.jobs.Job;
import ro.ieat.soso.core.jobs.TaskUsage;

import java.util.*;

import static ro.ieat.soare.repositories.util.TaskUsageCombiner.LOG;
import static ro.ieat.soare.repositories.util.TaskUsageCombiner.computeTaskUsage;

/**
 * Created by adispataru on 24-Jun-16.
 */
@Service
public class DataAggregator {

    @Autowired
    JobRepository jobRepository;

    @Autowired
    ResourceRepository resourceRepository;

    @Autowired
    TaskUsageRepository taskUsageRepository;


    public Map<Long, Histogram<Long>> createJobSizeHistogramPerMachine(Long startTime, Long endTime) {
        Map<Long, Histogram<Long>> resourceMapping = new TreeMap<>();

        Streams.from(jobRepository.findBySubmitTimeBetween(startTime, endTime)).consume(job -> {
            int size = job.getTaskHistory().size();
            Streams.from(job.getTaskHistory().values()).consume(taskHistory -> {
                Long mId = taskHistory.getMachineId();
                resourceMapping.putIfAbsent(mId, new Histogram<>());
                resourceMapping.get(mId).add((long) size);

            });
        });
        return resourceMapping;
    }

    public Histogram<Integer> createJobSizeHistogram(Long startTime, Long endTime) {
        Histogram<Integer> result = new Histogram<>();

        Streams.from(jobRepository.findBySubmitTimeBetween(startTime, endTime)).consume(job -> {
            if(job.getTaskHistory().get(0L) != null) {
                result.add(job.getTaskHistory().size());
            }

        });
        return result;
    }



    public Histogram<String> createAttributeHistogram(Long startTime, Long endTime) {
        Histogram<String> attributes = new Histogram<>();
//        Map<String, Histogram<String>> values = new HashMap<>();
        List<Job> jobs = jobRepository.findBySubmitTimeBetween(startTime, endTime);

        Streams.from(jobs).consume(job -> {
            Streams.from(job.getTaskHistory().values()).consume(th -> {
                Streams.from(th.getConstraints()).consume(tc -> {
                    attributes.add(tc.getAttributeName());
//                    values.putIfAbsent(tc.getAttributeName(), new Histogram<String>());
//                    values.get(tc.getAttributeName()).add(tc.getAttributeValue());
                });
            });
        });
        return attributes;
    }

    public List<TaskUsage> computeTasksUsage(Long startTime, Long endTime){
        List<TaskUsage> result = new ArrayList<>();
        Map<Long, Resource> resourceMap = new TreeMap<>();
        resourceRepository.findAll().forEach(r -> resourceMap.put(r.getId(), r));

        Stream<TaskUsage> tasksStream = Streams.from(taskUsageRepository.findByStartTimeGreaterThanAndEndTimeLessThan(startTime - 1, endTime + 1));
        tasksStream.filter(t -> t.getResourceId() != 0).groupBy(TaskUsage::getResourceId).flatMap(longTaskUsageGroupedStream ->
                longTaskUsageGroupedStream.buffer().map(
                        (taskUsages -> computeTaskUsage(startTime, taskUsages, resourceMap.get(taskUsages.get(0).getResourceId())))
                )).consume(result::add);

        return result;
    }

    public List<Resource> computeResourceUsage(Long startTime, Long endTime){
        List<Resource> result = new ArrayList<>();
        Map<Long, Resource> resourceMap = new TreeMap<>();
        resourceRepository.findAll().forEach(r -> resourceMap.put(r.getId(), r));

        Stream<TaskUsage> tasksStream = Streams.from(taskUsageRepository.findByStartTimeGreaterThanAndEndTimeLessThan(startTime - 1, endTime + 1));
        tasksStream.filter(t -> t.getResourceId() != 0).groupBy(TaskUsage::getResourceId).flatMap(longTaskUsageGroupedStream ->
                longTaskUsageGroupedStream.buffer().map(
                        (taskUsages -> computeTaskUsage(startTime, taskUsages, resourceMap.get(taskUsages.get(0).getResourceId())))
                )).consume(t -> {
                    resourceMap.get(t.getResourceId()).setUsagePrediction(t);
                    result.add(resourceMap.get(t.getResourceId()));
        });

        return result;
    }

    /**
     * Method for computing resource usage in case of jobs scheduled on different machines than in the trace
     * @param startTime  the start time of the tracing period
     * @param endTime the end time of the tracing period
     * @param schedule a map with the resource offer containing a task machine mapping (for each job)
     * @return the set of task usages per machine.
     */
    public List<TaskUsage> computeTasksUsageWithOwn(Long startTime, Long endTime, Map<Long, ResourceOffer> schedule){
        List<TaskUsage> result = new ArrayList<>();
        Map<Long, Resource> resourceMap = new TreeMap<>();
        resourceRepository.findAll().forEach(r -> resourceMap.put(r.getId(), r));

        Stream<TaskUsage> tasksStream = Streams.from(taskUsageRepository.findByStartTimeGreaterThanAndEndTimeLessThan(startTime - 1, endTime + 1));
        tasksStream.filter(t -> t.getResourceId() != 0)
                .groupBy(t -> schedule.containsKey(t.getJobId()) ?
                        schedule.get(t.getJobId()).getTaskResourceMapping().get(t.getTaskIndex()) : t.getResourceId())
                .flatMap(longTaskUsageGroupedStream ->
                longTaskUsageGroupedStream.buffer().map(
                        (taskUsages -> computeTaskUsage(startTime, taskUsages, resourceMap.get(taskUsages.get(0).getResourceId())))
                )).consume(result::add);

        return result;
    }

    public List<TaskUsage> computeTasksUsageForResources(Long startTime, Long endTime, List<Long> rIds){
        List<TaskUsage> result = new ArrayList<>();
        Map<Long, Resource> resourceMap = new TreeMap<>();
        resourceRepository.findAll().forEach(r -> resourceMap.put(r.getId(), r));

        Stream<TaskUsage> tasksStream = Streams.from(taskUsageRepository.findByResourceIdInAndStartTimeGreaterThanAndEndTimeLessThan(rIds, startTime - 1, endTime + 1));
        tasksStream.filter(t -> t.getResourceId() != 0).groupBy(TaskUsage::getResourceId).flatMap(longTaskUsageGroupedStream ->
                longTaskUsageGroupedStream.buffer().map(
                        (taskUsages -> computeTaskUsage(startTime, taskUsages, resourceMap.get(taskUsages.get(0).getResourceId())))
                )).consume(result::add);

        LOG.info("Usages for two resources: " + result.size());

        return result;
    }



    /**
     *
     * @param startTime trace period start
     * @param endTime trace period end
     * @param availabilityThreshold percent of resource to be free in order to be considered available.
     * @return the list of available resources s.t.: r.consumed < &alpha; * r.prop;
     */
    public List<Resource> getAvailableResources(long startTime, long endTime, double availabilityThreshold){
        List<Resource> result = new ArrayList<>();
        Map<Long, Resource> resourceMap = new TreeMap<>();
        List<Long> q = new ArrayList<>();
        q.add(5L);
        q.add(7L);
        LOG.info("Find in list: " + resourceRepository.findByIdIn(q).size());
        resourceRepository.findAllTurnedOnAtTime(endTime).forEach(r -> resourceMap.put(r.getId(), r));

        final double a = availabilityThreshold;
        Stream<TaskUsage> tasksStream = Streams.from(taskUsageRepository.findByStartTimeGreaterThanAndEndTimeLessThan(startTime - 1, endTime + 1));
        tasksStream.filter(t -> t.getResourceId() != 0).groupBy(TaskUsage::getResourceId)
                .flatMap(longTaskUsageGroupedStream -> longTaskUsageGroupedStream.buffer()
                        .map((taskUsages -> computeTaskUsage(startTime, taskUsages, resourceMap.get(taskUsages.get(0).getResourceId())))))
                .filter(t -> available(t, resourceMap.get(t.getResourceId()), a))
                .map(t -> resourceMap.get(t.getResourceId())).consume(result::add);

        LOG.info("Available machines: " + result.size());

        return result;
    }

    private boolean available(TaskUsage t, Resource r, double a) {
        return t.getCpu() <= a * r.getCpu() && t.getMemory() <= a * r.getMemory();
    }
}
