package ro.ieat.soare.repositories.util;

import ro.ieat.soso.core.coalitions.Resource;
import ro.ieat.soso.core.config.Configuration;
import ro.ieat.soso.core.jobs.TaskUsage;

import java.util.List;
import java.util.logging.Logger;

/**
 * Created by adispataru on 06-Aug-16.
 */
public class TaskUsageCombiner {
    public static final Logger LOG = Logger.getLogger("TaskUsageCombiner");

    public static TaskUsage computeTaskUsage(Long startTime, List<TaskUsage> usageList, Resource r){
        final int sampleSize = 300;

        startTime /= Configuration.TIME_DIVISOR;
        TaskUsage[] total = new TaskUsage[sampleSize];
        for(int i = 0; i < sampleSize; i++){
            total[i] = new TaskUsage();
            total[i].setResourceId(usageList.get(0).getResourceId());
        }
        Double cpuCapacity = r.getCpu();
        Double memCapacity = r.getMemory();
        long overcommited = 0;
        long mOvercommited = 0;
        for(TaskUsage taskUsage : usageList){
//            long sch = getTimeToStartByJobIdAndScheduleType(taskUsage.getJobId(), type, scheduledJobs);
            long sch = 0;
//            long jobStart = getJobScheduleTime(jobs, taskUsage.getJobId());
            long jobStart = 0;

//             mindbogglingly offset computation 'cause I can!
//            long offset = (sch != 0 && jobStart != 0) ? (sch - jobStart) / Configuration.TIME_DIVISOR : 0;
            long offset = 0;

            if(Math.abs(offset) > sampleSize) {
                LOG.info("job " + taskUsage.getJobId() + " has offset " + offset);
                LOG.info("job start" + jobStart + "; scheduled start " + sch);
                continue;
            }
            long thisTaskStart = taskUsage.getStartTime() / Configuration.TIME_DIVISOR - startTime;
            long thisTaskEnd = taskUsage.getEndTime() / Configuration.TIME_DIVISOR - startTime;
            int over = 0;
            int mOver = 0;
            for(long i = thisTaskStart + offset; i < thisTaskEnd + offset && i < sampleSize; i++){
                total[(int) i].addTaskUsage(taskUsage);
                if(total[(int) i].getCpu() > cpuCapacity)
                    over += 1;
                if(total[(int) i].getMemory() > memCapacity)
                    mOver += 1;
            }

            overcommited += over;
            mOvercommited  += mOver;

        }


        TaskUsage result = new TaskUsage();
        for(int i = 0; i < sampleSize; i++){
            result.addTaskUsage(total[i]);
            if(result.getResourceId() == null)
                result.setResourceId(total[i].getResourceId());
        }
        result.divide(sampleSize);
//        int i = usageList.size() - 1;
//        long ocommited = 0;
//        while (result.getCpu() > cpuCapacity && i >= 0) {
//
////            if (usageList.get(i).getCpu() > 0.1) {
//                result.substractTaskUsage(usageList.get(i));
//                ocommited++;
////            }
//
//            i--;
//        }

        if(overcommited > 0) {
            LOG.info("Overcommited on cpu: " + overcommited + " on resource: " + r.getId() + " having " + usageList.size() + " tasks");
        }
        if(mOvercommited > 0) {
            LOG.info("Overcommited on memory: " + mOvercommited + " on resource: " + r.getId() + " having " + usageList.size() + " tasks");
        }
//        avgLoad += result.getCpu()/cpuCapacity;
        return result;
    }
}
