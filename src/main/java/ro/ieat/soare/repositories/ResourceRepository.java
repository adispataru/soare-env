package ro.ieat.soare.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;
import ro.ieat.soso.core.coalitions.Resource;

import java.util.List;

/**
 * Created by adrian on 10.01.2016.
 * For maintaining ro.ieat.soare.coalitions.
 */
public interface ResourceRepository extends MongoRepository<Resource, Long>, ResourceRepositoryCustom{
        public List<Resource> findAllTurnedOnAtTime(long timestamp);
        public List<Resource> findByIdIn(List<Long> ids);

}
