package ro.ieat.soare.repositories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import reactor.rx.Streams;
import ro.ieat.soso.core.coalitions.Interval;
import ro.ieat.soso.core.coalitions.Resource;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by adispataru on 23-Jun-16.
 */
public class ResourceRepositoryImpl implements ResourceRepositoryCustom {
    @Autowired
    ResourceRepository ResourceRepository;

    public List<Resource> findAllTurnedOnAtTime(long timestamp){
        return ResourceRepository.findAll().stream().filter(m -> {
            for(Interval i : m.getAvailabilityInterval()){
                if(i.contains(timestamp))
                    return true;
                if(i.getEnd() > timestamp){
                    break;
                }
            }
            return false;
        }).collect(Collectors.toList());

    }

}
