package ro.ieat.soare.repositories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.ieat.soso.core.coalitions.Resource;
import ro.ieat.soso.core.config.Configuration;
import ro.ieat.soso.core.jobs.Job;
import ro.ieat.soso.core.jobs.TaskConstraint;
import ro.ieat.soso.core.jobs.TaskHistory;
import ro.ieat.soso.core.jobs.TaskUsage;
import ro.ieat.soso.core.mappers.*;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import java.util.logging.Logger;

/**
 * Created by adrian on 22/06/16.
 */
@Service
public class DBPopulator {

    private static final Logger LOG = Logger.getLogger(DBPopulator.class.getName());

    @Autowired
    ResourceRepository resourceRepository;

    @Autowired
    JobRepository jobRepository;

    @Autowired
    TaskUsageRepository taskUsageRepository;

    public void createFiles(long initStart, long initEnd) throws IOException, InterruptedException{
        Long maxTime = Long.MAX_VALUE / Configuration.TIME_DIVISOR;
        Map<Long, Resource> ResourceMap = new TreeMap<>();
        MachineEventsMapper.map(new FileReader(Configuration.MACHINE_EVENTS), 0, maxTime, ResourceMap);
        FileWriter f = new FileWriter("./machines.dat");
        for (Long key : ResourceMap.keySet()){
            Resource r = ResourceMap.get(key);
            f.write(String.format("%d %f %f\n",r.getId(), r.getCpu(), r.getMemory()));
        }
        f.close();
        f = new FileWriter("./jobs.dat");

        Map<Long, Job> jobMap = new TreeMap<Long, Job>();
        for(int i = 0 ; i < 4; i++){
            String jobEvents = Configuration.JOB_EVENTS_PATH + String.format("part-%05d-of-00500.csv", i);
            String taskEvents = Configuration.TASK_EVENTS_PATH + String.format("part-%05d-of-00500.csv", i);
            String taskConstraints = "./data/task_constraints/" + String.format("part-%05d-of-00500.csv", i);
            File jef = new File(jobEvents);
            File tef = new File(taskEvents);
            if(!jef.exists() || !tef.exists())
                break;
            LOG.info("Reading from part " + i);
            LOG.info("Jobs...");
            JobEventsMapper.map(new FileReader(jef), jobMap, initStart, maxTime);
            LOG.info("Tasks...");
            TaskEventsMapper.map(new FileReader(tef), jobMap, initStart, maxTime);

        }
        LOG.info("Saving and cleaning up finished jobs...");
        Iterator<Map.Entry<Long, Job>> iterator = jobMap.entrySet().iterator();
        List<Job> sorted = new ArrayList<>();
        while(iterator.hasNext()){
            Job j = iterator.next().getValue();
            boolean inserted = false;
            for(int i = 0; i < sorted.size(); i++){
                if(sorted.get(i).getScheduleTime() > j.getScheduleTime()){
                    sorted.add(i, j);
                    inserted = true;
                    break;
                }
            }
            if(!inserted){
                sorted.add(j);
            }
        }

        for(Job j : sorted) {
            TaskHistory taskHistory = j.getTaskHistory().get(0L);
            if (j.getStatus().equals("finish") || j.getStatus().equals("kill") || j.getStatus().equals("fail")) {
                if (taskHistory != null)
                    f.write(String.format("%d %d %d %d %f %f\n", j.getJobId(), j.getScheduleTime() / Configuration.TIME_DIVISOR,
                            (j.getFinishTime() - j.getScheduleTime()) / Configuration.TIME_DIVISOR, j.getTaskHistory().size(),
                            taskHistory.getRequestedCPU(), taskHistory.getRequestedMemory()));
            }
        }
        f.close();

    }


    public String populateDB(long initStart, long initEnd) throws IOException, InterruptedException {
        //        Configuration.JOB_EVENTS_PATH = "/home/adrian/work/ieat/CloudLightning/soso-predictor/" + Configuration.JOB_EVENTS_PATH;
//        Configuration.TASK_EVENTS_PATH = "/home/adrian/work/ieat/CloudLightning/soso-predictor/" + Configuration.TASK_EVENTS_PATH;
//        Configuration.TASK_USAGE_PATH = "/home/adrian/work/ieat/CloudLightning/soso-predictor/" + Configuration.TASK_USAGE_PATH;
//        Configuration.Resource_EVENTS = "/home/adrian/work/ieat/CloudLightning/soso-predictor/" + Configuration.Resource_EVENTS;

        Long maxTime = Long.MAX_VALUE / Configuration.TIME_DIVISOR;

        long time = System.currentTimeMillis();


        if(resourceRepository.count() == 0) {
            //Make use of Resource_events file to populate RepositoryPool;
            LOG.info("Starting Resource mapping");
            LOG.info("Generating Resources based on events file");
            Map<Long, Resource> ResourceMap = new TreeMap<>();
            MachineEventsMapper.map(new FileReader(Configuration.MACHINE_EVENTS), 0, maxTime, ResourceMap);

            MachineAttributesReader reader = new MachineAttributesReader();
            reader.map(new FileReader("./data/machine_attributes/part-00000-of-00001.csv"),
                    ResourceMap, initStart, 20000 * Configuration.TIME_DIVISOR);

            resourceRepository.save(ResourceMap.values());
            LOG.info("Done.");
        }
        else{
            LOG.info("Resources data exists in MongoDB");
        }
        LOG.info("Job repo size " + jobRepository.count());
        if(jobRepository.count() < 1) {

            LOG.info("Entering");
            Map<Long, Job> jobMap = new TreeMap<Long, Job>();
            for(int i = 0 ; i < 4; i++){
                String jobEvents = Configuration.JOB_EVENTS_PATH + String.format("part-%05d-of-00500.csv", i);
                String taskEvents = Configuration.TASK_EVENTS_PATH + String.format("part-%05d-of-00500.csv", i);
                String taskConstraints = "./data/task_constraints/" + String.format("part-%05d-of-00500.csv", i);
                File jef = new File(jobEvents);
                File tef = new File(taskEvents);
                File tcf = new File(taskConstraints);
                if(!jef.exists() || !tef.exists() || !tcf.exists())
                    break;
                LOG.info("Reading from part " + i);
                LOG.info("Jobs...");
                JobEventsMapper.map(new FileReader(jef), jobMap, initStart, maxTime);
                LOG.info("Tasks...");
                TaskEventsMapper.map(new FileReader(tef), jobMap, initStart, maxTime);
                LOG.info("Tasks constraints...");
                TaskConstraintReader.map(new FileReader(tcf), jobMap, initStart, maxTime);
                LOG.info("Tasks usage...");



                LOG.info("Saving and cleaning up finished jobs...");
                Iterator<Map.Entry<Long, Job>> iterator = jobMap.entrySet().iterator();
                long totalNormalized = 0;
                while(iterator.hasNext()){
                    Job j = iterator.next().getValue();

                    if(j.getTaskHistory().size() < 50000) {
                        if(j.getTaskHistory().get(0L) != null) {
                            if(j.getTaskHistory().get(0L).getConstraints().size() > 1500){
                                for(TaskHistory th : j.getTaskHistory().values()){
                                    if(th.getTaskIndex() != 0L){
                                        th.setConstraints(new ArrayList<>());
                                    }
                                }
                                totalNormalized += 1;
                            }
                            jobRepository.save(j);
                        }
                    }else{
                        LOG.severe("Not saving job " + j.getJobId() + " because it has " + j.getTaskHistory().size() + " tasks.");
                        //iterator.remove();
                    }
                    if(j.getStatus().equals("finish") || j.getStatus().equals("kill") || j.getStatus().equals("fail"))
                        iterator.remove();
                }
                LOG.info("Total normalized this round: " + totalNormalized);
            }
        }else {
            LOG.info("Jobs data existent in mongo.");
            //Not needed anymore
//            LOG.info("Setting assigned Resource id to 0. ");
//            for(TaskUsage usage : taskUsageMappingRepository.findAll().stream().
//                    filter(t -> t.getAssignedResourceId() != 0).collect(Collectors.toList())){
//                usage.setAssignedResourceId(0L);
//                taskUsageMappingRepository.save(usage);
//            }
            LOG.info("Done. ");
        }

        if(taskUsageRepository.count() < 1){
            for(int i = 0 ; i < 4; i++) {
                String taskUsages = Configuration.TASK_USAGE_PATH + String.format("part-%05d-of-00500.csv", i);
                File tuf = new File(taskUsages);
                List<TaskUsage> taskUsageList = TaskUsageMapper.map(new FileReader(tuf), initStart, initEnd);

                LOG.info("Done reading.");
                LOG.info("Task usage records: " + taskUsageList.size());
                LOG.info("First task time: " + taskUsageList.get(0).getEndTime());
                LOG.info("Last task time: " + taskUsageList.get(taskUsageList.size() - 1).getEndTime());

                LOG.info("Saving task usages..");
                for (TaskUsage t : taskUsageList) {
                    taskUsageRepository.save(t);
                }
            }
        }
        return "Done.";

    }
}
