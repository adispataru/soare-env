package ro.ieat.soare.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;
import ro.ieat.soso.core.jobs.TaskUsage;

import java.util.List;

/**
 * Created by adispataru on 4/16/2016.
 */
public interface TaskUsageRepository extends MongoRepository<TaskUsage, Long> {
    List<TaskUsage> findByStartTimeGreaterThanAndEndTimeLessThan(Long startTime, Long endTime);
    List<TaskUsage> findByResourceIdAndEndTimeLessThan(Long resourceId, Long endTime);
    List<TaskUsage> findByResourceIdAndStartTimeGreaterThanAndEndTimeLessThan(Long resourceId, Long startTime, Long endTime);
    List<TaskUsage> findByResourceIdInAndStartTimeGreaterThanAndEndTimeLessThan(List<Long> resourceIds, Long startTime, Long endTime);
}
