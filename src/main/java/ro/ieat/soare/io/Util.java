package ro.ieat.soare.io;

import ie.ucc.cl.sosm.components.PRouter;
import ie.ucc.cl.sosm.components.SOSMComponent;
import ie.ucc.cl.sosm.components.SOSMFramework;
import ie.ucc.cl.sosm.model.ResourcePrescription;
import ro.ieat.soare.clustering.Cluster;
import ro.ieat.soare.clustering.ensemble.CoalitionDivision;
import ro.ieat.soare.clustering.ensemble.CoalitionPool;
import ro.ieat.soare.math.statistics.Histogram;
import ro.ieat.soso.core.coalitions.Coalition;
import ro.ieat.soso.core.coalitions.Resource;
import ro.ieat.soso.core.jobs.TaskUsage;

import java.io.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.Logger;

/**
 * Created by adispataru on 24-Jun-16.
 */
public class Util {

    public static final Logger LOG = Logger.getLogger("io.Util");
    static boolean printed = false;

    public static float[][] readPrecomputedSimilarityMatrix(File f, int size) {
        LOG.info("Reading similarity from file...");
        float[][] result = new float[size][];
        DataInputStream dis = null;
        try {
            dis = new DataInputStream(new FileInputStream(f));
            for(int i = 0; i < size; i++){
                result[i] = new float[size];
                for(int j = 0; j < size; j++){
                    result[i][j] = dis.readFloat();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            if(dis != null)
                try {
                    dis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }
        LOG.info("Done!");
        return result;
    }

    public static void writeComputedSimilarityMatrix(float[][] similarityMatrix, String similarityFilepath){
        LOG.info("Writing similarity to file...");
        DataOutputStream dos = null;
        try {
            dos = new DataOutputStream(new FileOutputStream(similarityFilepath));
            for (float[] aSimilarityMatrix : similarityMatrix) {
                for (int j = 0; j < similarityMatrix.length; j++) {
                    dos.writeFloat(aSimilarityMatrix[j]);

                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            if (dos != null){
                try {
                    dos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        LOG.info("Done!");
    }

//    public static void writeClusterToFile(Cluster c, Map<Long, Histogram<Long>> ResourceMapping) {
//        System.out.println("Cluster " + c.getId());
//        System.out.println("Size " + c.getPoints().size());
//        File f = new File("./clusters.data");
//        boolean exists = f.exists();
//        FileWriter fileWriter = null;
//        try {
//            fileWriter = new FileWriter(f, true);
//            if(!exists){
//                fileWriter.write("%cluster_id,histogram_key,histogram_count,...\n");
//            }
//
//            Map<Long, Long> appearances = new HashMap<>();
//            for(int i = 0 ; i < c.getPoints().size(); i++) {
//                Long firstPoint = (Long) c.getPoints().get(i);
//                Histogram<Long> histogram = ResourceMapping.get(firstPoint);
//                if(histogram == null)
//                    continue;
//                for(Long l : histogram.getHistogram().keySet()){
//                    appearances.putIfAbsent(l, 0L);
//                    appearances.put(l, appearances.get(l) + 1);
//                }
//
//            }
//            for(Long l : appearances.keySet()){
//                fileWriter.write(String.format("%d,%d,%d,%d\n", c.getId(), c.getPoints().size(), l, appearances.get(l)));
//            }
//
//
//
//        } catch (IOException e) {
//            e.printStackTrace();
//        }finally {
//            if (fileWriter != null)
//                try {
//                    fileWriter.close();
//                } catch (IOException e) {
//
//                }
//        }
//
//    }

    public static void writeResourceDistribution(Map<Long, Histogram<Long>> ResourceMapping){
        File f = new File("./Resource-distrib.data");
        boolean exists = f.exists();
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter(f, true);
            if(!exists){
                fileWriter.write("%Resource_id,histogram_key,histogram_count\n");
            }

            int counter = 0;
            for(Long id : ResourceMapping.keySet()){
                Map<Long, Double> distribution = ResourceMapping.get(id).getProbabilityDistribution();
                for(Long key : distribution.keySet()) {
                    fileWriter.write(String.format("%d,%d,%.4f\n", counter, key, distribution.get(key)));
                }
                counter++;
            }

        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            if (fileWriter != null)
                try {
                    fileWriter.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }
    }


    public void writePropClustersToFile(List<Cluster> clusters, List<Resource> Resources) {

        File f = new File("./prop-clusters.data");
        boolean exists = f.exists();
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter(f, true);
            if(!exists){
                fileWriter.write("%cluster_id,size,cpu,memory\n");
            }

            for(Cluster c : clusters){
                fileWriter.write(String.format("%d,%d", c.getId(), c.getPoints().size()));

                Resource m = Resources.get(c.getPoints().get(0));
                fileWriter.write(String.format(",%.4f,%.4f\n", m.getCpu(), m.getMemory()));

            }



        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            if (fileWriter != null)
                try {
                    fileWriter.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }
    }

    public static void writeClusteringStep(List<Cluster> clusters, float[][] similarityMatrix, int step, long computingtime, String strategy) {


        File f = new File("./clustering-data/human/" + strategy + ".log");
        File f2 = new File("clustering-data/" + strategy + ".out");
        boolean exists = f.exists();
        boolean exists2 = f2.exists();
        FileWriter fileWriter = null;
        FileWriter fileWriter2 = null;
        try {
            fileWriter = new FileWriter(f, true);
            fileWriter2 = new FileWriter(f2, true);
            if(!exists){
                fileWriter.write("%Human readable file describing clustering process\n");
            }

            if(!exists2){
                fileWriter2.write("%Data file describing clustering process\n");
            }

            fileWriter.write(step + "th step\n");
            fileWriter.write("We have " + clusters.size() + " clusters computed in " + computingtime + " ms\n");
            Histogram<Integer> clusteSizeHistogram = new Histogram<>();
            Map<Integer, Double> clusterSizeAvgSim = new HashMap<>();
            for(Cluster c : clusters){
                clusteSizeHistogram.add(c.getPoints().size());
                double avgSim = 0.0;
                int sims = 0;
                for(Integer i : c.getPoints()){
                    for(Integer j : c.getPoints()){
                        avgSim += similarityMatrix[i][j];
                        sims++;
                    }
                }
                avgSim /= sims;

                if(c.getPoints().size() > 2) {
                    fileWriter.write("Cluster " + c.getId() + "\n");
                    fileWriter.write("Size: " + c.getPoints().size());
                    fileWriter.write("Average similarity: " + avgSim + "\n");
                }
                clusterSizeAvgSim.putIfAbsent(c.getPoints().size(), .0);
                Double a = clusterSizeAvgSim.get(c.getPoints().size()) + avgSim;
                clusterSizeAvgSim.put(c.getPoints().size(), a);

            }

            for(Integer key : clusteSizeHistogram.getHistogram().keySet()){
                Integer occ = clusteSizeHistogram.getHistogram().get(key).get();
                fileWriter2.write(String.format("%d,%d,%d,%.4f\n", step, key, occ, clusterSizeAvgSim.get(key)/occ));
            }


        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            if (fileWriter != null)
                try {
                    fileWriter.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            if (fileWriter2 != null)
                try {
                    fileWriter2.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }
    }

    public static void writeClusterScore(String str, String str2, float eps, double score, double divergence, double distance) {

        File f = new File("clusters-score.dat");
        boolean exists = f.exists();
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter(f, true);
            if(!exists){
                fileWriter.write("%str1,str2,eps,score,divergence,Bdistance\n");
            }
            fileWriter.write(String.format("%s,%s,%.4f,%.4f,%.4f,%.4f\n", str, str2, eps, score, divergence, distance));
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            if(fileWriter != null){
                try {
                    fileWriter.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void writeCoalitionStatus(Long endTime, Map<Integer, Double> coalitionsLoad, Map<Integer, Double> coalitionsMaxLoad) {
        File f = new File("coalition-status.dat");
        boolean exists = f.exists();
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter(f, true);
            if(!exists){
                fileWriter.write("%id, avgLoad, maxLoad\n");
            }
            for(Integer i : coalitionsLoad.keySet()) {
                fileWriter.write(String.format("%d,%d,%.4f,%.4f\n", endTime, i, coalitionsLoad.get(i), coalitionsMaxLoad.get(i)));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            if(fileWriter != null){
                try {
                    fileWriter.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void writeCoalitionStatus(long time, CoalitionDivision cDivision) {
        int i = 0;

        File f = new File("coalition-status.dat");
        boolean exists = f.exists();
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter(f, true);
            if(!exists){
                fileWriter.write("%time, pool, id, size,poolCapacity\n");
            }
            int j = 0;
            for(CoalitionPool cp : cDivision.getPools()) {
                Histogram<Integer> coalHist = new Histogram<>();
                int size = 0;

                for (Coalition c : cp.getAll()) {
                    if (c.getId() == 0)
                        c.setId(++i);
                    coalHist.add(c.getResources().size());
                    size += c.getResources().size();
                }
                for(Integer x : coalHist.getHistogram().keySet()) {
                    fileWriter.write(String.format("%d,%d,%d,%d\n", time, j, x, coalHist.getHistogram().get(x).get()));
                }
                Coalition c = cp.getNoise();
                size += c.getResources().size();
                fileWriter.write(String.format("%d,%d,%d,%d,%.2f,%.2f\n", time, j,c.getResources().size(), 0, cp.getExpectedCPU(), cp.getExpectedMemory()));

//                System.out.println(String.format("%d,%d,%.4f,%.4f", j, size, cp.getExpectedCPU(), cp.getExpectedMemory()));

                j++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            if(fileWriter != null){
                try {
                    fileWriter.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void writeSuccessInformation(Long endTime, AtomicLong sentTasks, AtomicLong scheduledTasks, AtomicLong sentJobs, AtomicLong scheduledJobs) {
        File f = new File("./sosm-success.dat");
        boolean exists = f.exists();
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter(f, true);
            if(!exists){
                fileWriter.write("%time, sentT, schedT, sentJ, schedJ\n");
            }
            int j = 0;
            fileWriter.write(String.format("%d,%d,%d,%d,%d\n", endTime, sentTasks.get(), scheduledTasks.get(), sentJobs.get(), scheduledJobs.get()));
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            if(fileWriter != null){
                try {
                    fileWriter.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void writeJobSizeHistogram(Histogram<Integer> jobSizeOutputHistogram) {
        File f = new File("./job-histogram.dat");
        boolean exists = f.exists();
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter(f, true);
            if(!exists){
                fileWriter.write("%size, #occurrences\n");
            }
            for(Integer size: jobSizeOutputHistogram.getHistogram().keySet()){
                fileWriter.write(String.format("%d,%d\n",size, jobSizeOutputHistogram.getHistogram().get(size).get()));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            if(fileWriter != null){
                try {
                    fileWriter.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void writeQueryStatus(long time, long qCoalitions, long qResources, long sJobs, long sTasks) {
        File f = new File("./queries.dat");
        boolean exists = f.exists();
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter(f, true);
            if(!exists){
                fileWriter.write("%time, #coals, #Resources, #jobs\n");
            }

            fileWriter.write(String.format("%d,%d,%d,%d,%d\n",time, qCoalitions, qResources,sJobs, sTasks));

        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            if(fileWriter != null){
                try {
                    fileWriter.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void writeSOSMLayout(Long endTime, List<PRouter> pRouters) {
        File f = new File("./sosm.dat");
        boolean exists = f.exists();
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter(f, true);
            if(!exists){
                fileWriter.write("%from,to,resources,components\n");
            }

            for(PRouter p : pRouters) {
                fileWriter.write(String.format("%d,%s,%s,%d,%d\n",endTime, "CM", p.getName(), p.getResourceSize(), p.getCooperative().size()));
                for (SOSMComponent s : p.getCooperative()) {
                    fileWriter.write(String.format("%d,%s,%s,%d,%d\n",endTime, p.getName(), s.getName(), s.getResourceSize(), s.getCooperative().size()));
                    for (SOSMComponent v : s.getCooperative()) {
//                        if(v instanceof VRackManager){
//                            VRackManager vr = (VRackManager) v;
                            fileWriter.write(String.format("%d,%s,%s,%d,%d\n",endTime, s.getName(), v.getName(), v.getResourceSize(), v.getCooperative().size()));
//                        }else {
//                            fileWriter.write(String.format("%s,%s\n", s.getName(), v.getName()));
//                        }
                    }
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            if(fileWriter != null){
                try {
                    fileWriter.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void writeResourceUsage(List<TaskUsage> taskUsageList) {
        File f = new File("./usage-machine.dat");
        boolean exists = f.exists();
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter(f, true);
            if(!exists){
                fileWriter.write("rId, cpu, maxCpu, mem, maxMem\n");
            }

            for(TaskUsage taskUsage : taskUsageList){
                fileWriter.write(String.format("%d,%.4f,%.4f,%.4f,%.4f\n",
                    taskUsage.getResourceId(), taskUsage.getCpu(), taskUsage.getMaxCpu(), taskUsage.getMemory(),
                    taskUsage.getMaxMemory()));
            }


        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            if(fileWriter != null){
                try {
                    fileWriter.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void writeSOSMFrameworkMetrics(long time, SOSMFramework framework) {
        File f = new File("./sosm-metrics.dat");
        boolean exists = f.exists();
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter(f, true);
            if(!exists){
                fileWriter.write("%time,name,SI,Task Throughput,Power Efficiency,Computational Efficiency,Managemenet Efficiency, Cumulative CPU, Cumulative Memory\n");
            }

            for(PRouter p : framework.getpRouterList()) {
//                fileWriter.write(String.format("%s,%s,%d,%d\n", "CM", p.getName(), p.getResourceSize(), p.getCooperative().size()));
                double[] metrics = p.getMetrics();
                fileWriter.write(String.format("%d,%s,%f,%f,%f,%f,%f,%f,%f\n",time, p.getName(), p.computeAndGetSuitabilityIndex(), metrics[0], metrics[1], metrics[2], metrics[3],
                        p.getCumulativeCapacity()[0], p.getCumulativeCapacity()[1]));
                for (SOSMComponent s : p.getCooperative()) {
//                    fileWriter.write(String.format("%s,%s,%d,%d\n", p.getName(), s.getName(), s.getResourceSize(), s.getCooperative().size()));
                    metrics = s.getMetrics();
                    fileWriter.write(String.format("%d,%s,%f,%f,%f,%f,%f,%f,%f\n",time, s.getName(), s.computeAndGetSuitabilityIndex(), metrics[0], metrics[1], metrics[2], metrics[3],
                            s.getCumulativeCapacity()[0], s.getCumulativeCapacity()[1]));
                    for (SOSMComponent v : s.getCooperative()) {
//                        if(v instanceof VRackManager){
//                            VRackManager vr = (VRackManager) v;
                        metrics = v.getMetrics();
                        fileWriter.write(String.format("%d,%s,%f,%f,%f,%f,%f,%f,%f\n",time, v.getName(), v.computeAndGetSuitabilityIndex(), metrics[0], metrics[1], metrics[2], metrics[3],
                                v.getCumulativeCapacity()[0], v.getCumulativeCapacity()[1]));
//                        }else {
//                            fileWriter.write(String.format("%s,%s\n", s.getName(), v.getName()));
//                        }
                    }
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            if(fileWriter != null){
                try {
                    fileWriter.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void writePrescriptionPath(String from, String to, double maxSI, ResourcePrescription rp) {
        File f = new File("./sosm-events.dat");
        boolean exists = f.exists();
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter(f, true);
            if(!exists){
                fileWriter.write("%time, from, to, why\n");
            }


            fileWriter.write(String.format("%d,%s,%s,%.4f\n", rp.getJob().getSubmitTime(), from, to, maxSI));

        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            if(fileWriter != null){
                try {
                    fileWriter.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void writeLoad(long time, List<Resource> load) {
        File f = new File("./load.dat");
        boolean exists = f.exists();
        double avg = 0, min = Double.MAX_VALUE, max = Double.MIN_VALUE;
        int total = 0;

        for(Resource r : load){
            double threshold = 0.1 * r.getCpu();
            double cpuUtil = r.getUsagePrediction().getCpu() / r.getCpu();
            if(cpuUtil > threshold) {
                total++;
                avg += cpuUtil;
                if (min > cpuUtil)
                    min = cpuUtil;
                if (max < cpuUtil)
                    max = cpuUtil;
            }
        }

        if(total < load.size()){
            System.out.println(total + " resources with utilization > 0.1*cpu out of " + load.size());
        }
        avg /= total;

        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter(f, true);
            if(!exists){
                fileWriter.write("%time, #machines, avg, min, max\n");
            }

            fileWriter.write(String.format("%d,%d,%.4f,%.4f,%.4f\n", time, total, avg, min, max));

        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            if(fileWriter != null){
                try {
                    fileWriter.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
