package ro.ieat.soare;

import ie.ucc.cl.sosm.SOSMExperiments;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import reactor.Environment;
import ro.ieat.soare.clustering.CoalitionClassifier;
import ro.ieat.soare.io.Util;
import ro.ieat.soare.math.statistics.Histogram;
import ro.ieat.soare.playing.InitialTaskUsagePublisher;
import ro.ieat.soare.repositories.DBPopulator;
import ro.ieat.soare.repositories.util.DataAggregator;
import ro.ieat.soso.core.coalitions.Resource;
import ro.ieat.soso.core.jobs.TaskUsage;

import java.util.List;

/**
 * Created by adispataru on 4/15/2016.
 */
@Configuration
@EnableAutoConfiguration
@EnableMongoRepositories

@ComponentScan({"ie.ucc.cl.sosm","ro.ieat.soare.playing", "ro.ieat.soare.clustering", "ro.ieat.soare.repositories", "ro.ieat.soare.scheduling", "ro.ieat.soare"})
public class App implements CommandLineRunner{

    private static final int numberOfResources = 10;
    public static final int historySize = 4;

    @Bean
    Environment env(){
        return Environment.initializeIfEmpty().assignErrorJournal();
    }

//    @Bean
//    EventBus createEventBus(Environment env){
//        return EventBus.create(env, Environment.THREAD_POOL);
//    }

    @Autowired
    private InitialTaskUsagePublisher publisher;

    @Autowired
    DBPopulator dbPopulator;

    @Autowired
    DataAggregator dataAggregator;

    @Autowired
    SOSMExperiments experiments;

//    @Autowired
//    private CoalitionReceiver coalitionReceiver;

    @Autowired
    private CoalitionClassifier coalitionClassifier;

    public void run(String... strings) throws Exception {


//        coalitionClassifier.instantiateClassifier(2700L);
//        publisher.publishInitialTaskUsage(2700L);
//        for(long t = 3300; t < 6300; t+= 300) {
//            System.out.println("Time " + t);
//            publisher.publishInitialTaskUsage(t);
//
//        publisher.scheduleJobs(3000L);
//        dbPopulator.populateDB(-1L, Long.MAX_VALUE / ro.ieat.soso.core.config.Configuration.TIME_DIVISOR);
//        dbPopulator.createFiles(-1L, Long.MAX_VALUE / ro.ieat.soso.core.config.Configuration.TIME_DIVISOR);
//        coalitionClassifier.instantiateAttrClassifier(2700L);
//        coalitionClassifier.instantiateJobSizeClassifier(2700L);
//        coalitionClassifier.instantiateMultiStepClustering(2700L, 15000L);
//        coalitionClassifier.bootStrapSOSM(2700L);

//        List<TaskUsage> taskUsageList = dataAggregator.computeTasksUsageForResources(2400L * ro.ieat.soso.core.config.Configuration.TIME_DIVISOR,
//                2700L * ro.ieat.soso.core.config.Configuration.TIME_DIVISOR, null);
//        Util.writeResourceUsage(taskUsageList);
//        List<Resource> availableResources = dataAggregator.getAvailableResources(2400L * ro.ieat.soso.core.config.Configuration.TIME_DIVISOR,
//                2700L * ro.ieat.soso.core.config.Configuration.TIME_DIVISOR, 0.2);
//        experiments.experiment();
//        Histogram<Integer> jsh = dataAggregator.createJobSizeHistogram(-1L, Long.MAX_VALUE / ro.ieat.soso.core.config.Configuration.TIME_DIVISOR);
//        Util.writeJobSizeHistogram(jsh);

        long time = 900L * ro.ieat.soso.core.config.Configuration.TIME_DIVISOR;
        long finish = 20000 * ro.ieat.soso.core.config.Configuration.TIME_DIVISOR;
        while (time <= finish) {
            List<Resource> load = dataAggregator.computeResourceUsage(time - 300 * ro.ieat.soso.core.config.Configuration.TIME_DIVISOR, time);
            Util.writeLoad(time, load);
            time += 300 * ro.ieat.soso.core.config.Configuration.TIME_DIVISOR;
        }

    }


    public static void main(String[] args) throws InterruptedException {
        ApplicationContext app = SpringApplication.run(App.class, args);
        //app.getBean(CountDownLatch.class).await(1, TimeUnit.SECONDS);
        app.getBean(Environment.class).shutdown();
    }

}
