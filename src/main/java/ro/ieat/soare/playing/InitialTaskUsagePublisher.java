package ro.ieat.soare.playing;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.Environment;
import reactor.rx.Stream;
import reactor.rx.Streams;
import ro.ieat.soare.clustering.CoalitionClassifier;
import ro.ieat.soare.repositories.JobRepository;
import ro.ieat.soare.repositories.ResourceRepository;
import ro.ieat.soare.repositories.TaskUsageRepository;
import ro.ieat.soare.scheduling.JobScheduler;
import ro.ieat.soso.core.coalitions.Coalition;
import ro.ieat.soso.core.coalitions.Resource;
import ro.ieat.soso.core.config.Configuration;
import ro.ieat.soso.core.jobs.ScheduledJob;
import ro.ieat.soso.core.jobs.TaskUsage;

import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.Logger;

import static ro.ieat.soare.repositories.util.TaskUsageCombiner.computeTaskUsage;

/**
 * Created by adispataru on 4/15/2016.
 */
@Service
public class InitialTaskUsagePublisher {

    @Autowired
    TaskUsageRepository taskUsageRepository;

    @Autowired
    ResourceRepository ResourceRepository;

    @Autowired
    CoalitionClassifier coalitionClassifier;

    @Autowired
    JobRepository jobRepository;


    private static Logger LOG = Logger.getLogger("Publisher");
    private AtomicLong counter = new AtomicLong();
    private Map<Long, List<Coalition>> coalitionsTable = new TreeMap<>();
    final Map<Long, Resource> resources = new TreeMap<>();
    private Double avgLoad;
    private JobScheduler jobScheduler = new JobScheduler();

    public void publishInitialTaskUsage(long time) throws InterruptedException{
        long lowTime = (time -  300 )* Configuration.TIME_DIVISOR - 1;
        time = time * Configuration.TIME_DIVISOR + 1;

        long start = System.currentTimeMillis();

        final Map<Long, TaskUsage> usages = new TreeMap<>();
        final double threshold = 0.25;
        avgLoad = 0.0;

        Streams.from(ResourceRepository.findAll()).consume(m -> resources.put(m.getId(), m));

        int totalResources = resources.size();

        Set<Resource> free = new HashSet<>();
        List<Coalition> incompleteCoalitions = new ArrayList<>();
        Stream<TaskUsage> tasksStream = Streams.from(taskUsageRepository.findByStartTimeGreaterThanAndEndTimeLessThan(lowTime, time));
        tasksStream.filter(t -> t.getResourceId() != 0).groupBy(TaskUsage::getResourceId).flatMap(longTaskUsageGroupedStream ->
                longTaskUsageGroupedStream.buffer().map((taskUsages -> computeTaskUsage(lowTime, taskUsages, resources.get(taskUsages.get(0).getResourceId())))))
                .filter(taskUsage ->
                                taskUsage.getCpu() <  resources.get(taskUsage.getResourceId()).getCpu() - threshold)
                .groupBy(taskUsage ->
                        coalitionClassifier.classifyResource(taskUsage.getResourceId()))
                .consume(longTaskUsageGroupedStream -> {
                    Long label = longTaskUsageGroupedStream.key();
                    if(label.equals(0L)){
                        longTaskUsageGroupedStream.map(taskUsage -> resources.remove(taskUsage.getResourceId()))
                                .buffer().consume(free::addAll);
                    } else {
                        longTaskUsageGroupedStream.map(taskUsage -> resources.remove(taskUsage.getResourceId()))
                                .buffer(label, 100, TimeUnit.MILLISECONDS).consume(ResourceList -> {
                            Coalition c = new Coalition();
                            c.setId(0);
                            c.setConfidenceLevel(1.0);
                            if (label.equals((long) ResourceList.size())) {
                                c.setResources(ResourceList);
                                sendCoalition(c);
                            } else if (ResourceList.size() > label / 2) {
                                c.setId(1);
                                c.setResources(ResourceList);
                                c.setRackId(label);
                                incompleteCoalitions.add(c);

                            } else {
                                free.addAll(ResourceList);
                            }
                        });
                    }

                });


        FileWriter f = null;
        try {
            f = new FileWriter("./load.data", true);
            f.write(time + " " + avgLoad/totalResources + "\n");
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            if (f != null)
                try {
                    f.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }



        free.addAll(resources.values());

        Streams.from(free).consume(m -> {
            if (incompleteCoalitions.size() > 0){
                Coalition coalition = incompleteCoalitions.get(0);
                coalition.getResources().add(m);
//                LOG.info("Has :" + coalition.getResources().size() + " out of " + coalition.getRackId());
                if(coalition.getResources().size() >= coalition.getRackId()){
                    sendCoalition(coalition);
                    incompleteCoalitions.remove(0);
                }
            } else {
                Coalition c = new Coalition();
                c.setId(2);
                c.setConfidenceLevel(1.0);
                c.setResources(new ArrayList<>());
                c.getResources().add(m);
                sendCoalition(c);

            }
        });


        //TODO: REMINDER!!! To ease the job when working with scheduling algorithms, you may stream and modify a task
        //usage Resource id with the allocated Resource id;
        long elapsed = System.currentTimeMillis() - start;

//        latch.await();
        LOG.info("Elapsed time = " + elapsed + " ms for " + counter + "coalitions ");
//        LOG.info("Average time per Resource = " + elapsed/(double) usages.size());

    }

    public void scheduleJobs(Long endTime){
        Long startTime = (endTime - Configuration.STEP)  * Configuration.TIME_DIVISOR;
        endTime *= Configuration.TIME_DIVISOR;

        long start = System.currentTimeMillis();
        AtomicLong scheduledJobs = new AtomicLong();
        AtomicLong sentJobs = new AtomicLong();
        Streams.from(jobRepository.findBySubmitTimeBetween(startTime, endTime)).consume(job -> {
            Long size = (long) job.getTaskHistory().size();
            sentJobs.getAndIncrement();
            ScheduledJob scheduledJob = null;
            if(coalitionsTable.size() > 0) {
                Long maxSize = Collections.max(coalitionsTable.keySet());
                while (!coalitionsTable.containsKey(size) && size < maxSize)
                    size += 1;
                for (Coalition c : coalitionsTable.get(size)) {
                    if ((scheduledJob = jobScheduler.scheduleJob(job, c)) != null) {
                        //TODO Uncomment this when testing
//                        coalitionsTable.get(size).remove(c);
                        break;
                    }
                }
                if (scheduledJob != null) {
                    scheduledJobs.getAndIncrement();
                }
            }


        });
        LOG.info("Scheduled " + scheduledJobs + " out of " + sentJobs);

    }




    private void checkDuplicates(Coalition c) {
        if(c.getResources().size() > 1) {
            Double mincpu = Double.MAX_VALUE;
            LOG.info("Checking coalition " + c.getId() + " with " + c.getResources().size() + " resources");
            int duplicates = 0;
            for (int i = 0; i < c.getResources().size() - 1; i++){
                for(int j = i + 1; j < c.getResources().size(); j++){
                    if (Objects.equals(c.getResources().get(i).getId(), c.getResources().get(j).getId()))
                        duplicates++;
                }
            }
            if(duplicates > 0) {
                LOG.info(String.format("id: %d\nsize: %d\neta: %d\nmin_cpu: %.4f\nduplicates: %d\n",
                        c.getId(), c.getResources().size(), c.getCurrentETA(), mincpu, duplicates));
            }
        }
    }

    private void sendCoalition(Coalition c) {
        counter.getAndIncrement();
        c.setId(counter.get());
        checkDuplicates(c);
        Long size = (long) c.getResources().size();
        coalitionsTable.putIfAbsent(size, new ArrayList<>());
        coalitionsTable.get(size).add(c);
    }

    private Stream<TaskUsage> getTaskUsagesForResource(Long ResourceId, Long startTime, Long endTime){
        return Streams.defer(() ->
                Streams.just(taskUsageRepository.findByResourceIdAndStartTimeGreaterThanAndEndTimeLessThan(ResourceId, startTime, endTime)))
                .timeout(2, TimeUnit.SECONDS)
                .flatMap(Streams::from)
                .subscribeOn(Environment.workDispatcher());
    }


    //TODO Record overcommit to compare with own.
//    private TaskUsage computeTaskUsage(Long startTime, List<TaskUsage> usageList){
//        final int sampleSize = 300;
//
//        startTime /= Configuration.TIME_DIVISOR;
//        TaskUsage[] total = new TaskUsage[sampleSize];
//        for(int i = 0; i < sampleSize; i++){
//            total[i] = new TaskUsage();
//            total[i].setResourceId(usageList.get(0).getResourceId());
//        }
//        Double cpuCapacity = resources.get(usageList.get(0).getResourceId()).getCpu();
//        Double memCapacity = resources.get(usageList.get(0).getResourceId()).getMemory();
//        long overcommited = 0;
//        long mOvercommited = 0;
//        for(TaskUsage taskUsage : usageList){
////            long sch = getTimeToStartByJobIdAndScheduleType(taskUsage.getJobId(), type, scheduledJobs);
//            long sch = 0;
////            long jobStart = getJobScheduleTime(jobs, taskUsage.getJobId());
//            long jobStart = 0;
//
////             mindbogglingly offset computation 'cause I can!
////            long offset = (sch != 0 && jobStart != 0) ? (sch - jobStart) / Configuration.TIME_DIVISOR : 0;
//            long offset = 0;
//
//            if(Math.abs(offset) > sampleSize) {
//                Logger.getLogger("Combiner").info("job " + taskUsage.getJobId() + " has offset " + offset);
//                Logger.getLogger("Combiner").info("job start" + jobStart + "; scheduled start " + sch);
//                continue;
//            }
//            long thisTaskStart = taskUsage.getStartTime() / Configuration.TIME_DIVISOR - startTime;
//            long thisTaskEnd = taskUsage.getEndTime() / Configuration.TIME_DIVISOR - startTime;
//            int over = 0;
//            int mOver = 0;
//            for(long i = thisTaskStart + offset; i < thisTaskEnd + offset && i < sampleSize; i++){
//                total[(int) i].addTaskUsage(taskUsage);
//                if(total[(int) i].getCpu() > cpuCapacity)
//                    over += 1;
//                if(total[(int) i].getMemory() > memCapacity)
//                    mOver += 1;
//            }
//
//            overcommited += over;
//            mOvercommited  += mOver;
//
//        }
//
//
//        TaskUsage result = new TaskUsage();
//        for(int i = 0; i < sampleSize; i++){
//            result.addTaskUsage(total[i]);
//            if(result.getResourceId() == null)
//                result.setResourceId(total[i].getResourceId());
//        }
//        result.divide(sampleSize);
//        int i = usageList.size() - 1;
//        long ocommited = 0;
////        while (result.getCpu() > cpuCapacity && i >= 0) {
////
//////            if (usageList.get(i).getCpu() > 0.1) {
////                result.substractTaskUsage(usageList.get(i));
////                ocommited++;
//////            }
////
////            i--;
////        }
//
//        if(overcommited > 0) {
//            LOG.info("Overcommited: " + overcommited);
//        }
//        avgLoad += result.getCpu()/cpuCapacity;
//        return result;
//    }

    private  Resource addTaskUsageToResource(Long key, TaskUsage t) {
        return null;
    }
}
