package ro.ieat.soare.math.statistics.distance;

import ro.ieat.soare.math.statistics.Histogram;

import java.util.Map;

/**
 * Created by adrian on 22/06/16.
 */
public class AttributeDistance {

    public static float computeAttributesSimilarity(Map<String, String> attrs1, Map<String, String> attrs2,
                                                    Histogram<String> requested){

        double distance, sumNumer = 0.0;
        Map<String, Double> pd = requested.getProbabilityDistribution();
        for(String event: pd.keySet()){
            double p2Event = 0.0 ;
            if(attrs1.get(event) != null && attrs1.get(event) != null)
                if (attrs1.get(event).equals(attrs2.get(event)))
                    p2Event = 1.0;

            double p = pd.get(event);
            sumNumer += Math.sqrt(p * p2Event);
        }
        distance = Math.abs(Math.log(sumNumer));
        return (float) DistributionDistance.applyGaussianKernel(distance);

    }

}
