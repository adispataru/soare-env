package ro.ieat.soare.math.statistics.distance;

import ie.ucc.cl.sosm.model.Characteristics;
import ro.ieat.soare.math.statistics.Histogram;
import ro.ieat.soso.core.coalitions.Resource;

import java.util.Map;

/**
 * Created by adrian on 22/06/16.
 */
public class PropertyDistance {

    public static double computePropertiesDistance(Resource m, Resource m2){
        float sum = 0;

        sum += Math.abs(m.getCpu() - m2.getCpu());
        sum += Math.abs(m.getMemory() - m2.getMemory());


        return sum/2;
    }

    public static double computeCharacteristicsDistance(Characteristics m, Characteristics m2){

        if(m.getCpu() > m2.getCpu() || m.getMemory() > m2.getMemory())
            return Double.NEGATIVE_INFINITY;

        float sum = 0;

        sum += Math.abs(m.getCpu() - m2.getCpu());
        sum += Math.abs(m.getMemory() - m2.getMemory());


        return sum/2;
    }

}
