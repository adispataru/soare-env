package ro.ieat.soare.math.statistics.distance;

import ro.ieat.soare.math.statistics.ProbabilityDistribution;

import java.util.Map;

/**
 * Created by adispataru on 6/2/2016.
 */
public class DistributionDistance {
    public static final double log2 = Math.log(2);

    public static double computeBhattacharyyaDistance(Map<?, Double> p1, Map<?, Double> p2){

        double distance, sumNumer = 0.0;

        for(Object event: p1.keySet()){
            double p2Event = p2.get(event) != null ? p2.get(event) : 0.0 ;

            sumNumer += Math.sqrt(p1.get(event) * p2Event);
        }
        distance = Math.abs(Math.log(sumNumer));
        return distance;

    }

    public static double computeKLDivergence(Map<?, Double> p1, Map<?, Double> p2) {


        double klDiv = 0.0;

        for (Object i : p1.keySet()) {
//            if(p2.get(i) == 0.0)
//                continue;
            double p2Event = p2.get(i) != null ? p2.get(i) : 0.0 ;
            klDiv += p1.get(i) * Math.log( p1.get(i) / p2Event );
        }

        return klDiv / log2; // moved this division out of the loop -DM
    }

    public static double applyGaussianKernel(double half) {
        double sigma = 0.6;
        return Math.exp(-1 * (Math.pow(half, 2) / Math.pow(sigma, 2)));
    }
}
