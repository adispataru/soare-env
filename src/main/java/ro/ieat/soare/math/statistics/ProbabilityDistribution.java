package ro.ieat.soare.math.statistics;

import java.util.Map;
import java.util.Set;

/**
 * Created by adispataru on 6/2/2016.
 */
public class ProbabilityDistribution<K> {
    private Map<K, Double> distribution;

    public ProbabilityDistribution(Map<K, Double> dist){
        this.distribution = dist;
    }

    public double get(K k){
        return distribution.get(k) != null ? distribution.get(k): 0.0;
    }

    public Set<K> getKeySet(){
        return distribution.keySet();
    }

    public int getSize() {
        return distribution.size();
    }

    public String toString(){
        StringBuilder sb = new StringBuilder();
        for(K key : distribution.keySet()){
            sb.append(String.format("%s: %.4f; ", key, distribution.get(key)));
        }
        return sb.toString();
    }
}
