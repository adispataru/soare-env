package ro.ieat.soare.math.statistics;

import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by adispataru on 6/2/2016.
 */
public class Histogram<K> {
    private Map<K, AtomicInteger> histogram;
    private AtomicInteger total;
    private AtomicInteger max;
    private K maxId;
    Map<K, Double> probabilityDistribution;
    private final Object mutex = new Object();

    public Histogram(){
        histogram = new TreeMap<>();
        total = new AtomicInteger(0);
        max = new AtomicInteger(0);
        maxId = null;
    }

    public Map<K, AtomicInteger> getHistogram() {
        return histogram;
    }

    public void add(K element){
        if(histogram.get(element) == null){
            histogram.put(element, new AtomicInteger(1));
        }else{
            histogram.get(element).getAndIncrement();
        }
        if(max.get() < histogram.get(element).get()){
            max.set(histogram.get(element).get());
            maxId = element;
        }
        total.getAndIncrement();
        probabilityDistribution = null;
    }

    public Map<K, Double> getProbabilityDistribution(){
        synchronized (mutex) {
            if (probabilityDistribution == null) {
                probabilityDistribution = new TreeMap<>();
                for (K k : histogram.keySet()) {
                    probabilityDistribution.put(k, 1.0 * histogram.get(k).get() / total.get());
                }
            }
        }

        return probabilityDistribution;
    }

    public String toString(){
        StringBuilder sb = new StringBuilder();
        for(K key : histogram.keySet()){
            sb.append(String.format("%s,%d; ", key, histogram.get(key).get()));
        }
        return sb.toString();
    }

    public AtomicInteger getMax() {
        return max;
    }

    public void setMax(AtomicInteger max) {
        this.max = max;
    }

    public K getMaxId() {
        return maxId;
    }

    public void setMaxId(K maxId) {
        this.maxId = maxId;
    }

}
