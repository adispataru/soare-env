package ro.ieat.soare.scheduling;

import org.springframework.web.client.RestTemplate;
import ro.ieat.soso.core.coalitions.Coalition;
import ro.ieat.soso.core.coalitions.Resource;
import ro.ieat.soso.core.events.ScheduleEvent;
import ro.ieat.soso.core.jobs.Job;
import ro.ieat.soso.core.jobs.ScheduledJob;
import ro.ieat.soso.core.jobs.TaskHistory;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Logger;

/**
 * Created by adispataru on 5/19/2016.
 */

public class JobScheduler {

    private Double THRESHOLD = 0.2;
    RestTemplate template = new RestTemplate();

    private static final Logger LOG =  Logger.getLogger("JobScheduler");

    public ScheduledJob scheduleJob(Job job, Coalition coalition){

        ScheduleEvent scheduleEvent=new ScheduleEvent();

        scheduleEvent.setCoalition(coalition);
        scheduleEvent.setJob(job);
        scheduleEvent.setStart(job.getScheduleTime());
        scheduleEvent.setEnd(job.getFinishTime());

        return scheduleJobOnResources(scheduleEvent, scheduleEvent.getStart(), scheduleEvent.getEnd());
    }

    ScheduledJob scheduleJobOnResources(ScheduleEvent scheduleEvent, long start, long end){
        //Schedule randomly

        Job job =scheduleEvent.getJob();
        List<Long> selectedResources = new ArrayList<Long>();
        Coalition coalition = scheduleEvent.getCoalition();

        boolean assignedJob = true;

        Map<Long, Long> taskResourceMapping = new TreeMap<Long, Long>();
        int i = 0;
        for(TaskHistory task : job.getTaskHistory().values()) {

            //UsageWritable scheduledTask = task.getUsageMap().get(task.getUsageMap().size() - 1);

            boolean assignedTask = false;
            for (Resource m : coalition.getResources()) {

                if (selectedResources.contains(m.getId()))
                    continue;

                if (Math.abs(m.getCpu() - task.getRequestedCPU()) > 0.00001) {
                    selectedResources.add(m.getId());
                    assignedTask = true;

                    taskResourceMapping.put(task.getTaskIndex(), m.getId());


                    break;
                }

            }
            if(!assignedTask) {
                LOG.info("Failed at task number " + i);
                assignedJob = false;
                break;
            }

            i++;

        }
        if(!assignedJob){

           LOG.info("Could not assign job:"+job);
            return null;
        }
        ScheduledJob s = new ScheduledJob();
        s.setJobId(job.getJobId());
        s.setTaskMachineMapping(taskResourceMapping);
        s.setTimeToStart(scheduleEvent.getStart());
        s.setFinishTime(scheduleEvent.getEnd());
        s.setJob(job);

        s.setCoalitionId(coalition.getId());

        if(coalition.getScheduledJobs() == null)
            coalition.setScheduledJobs(new ArrayList<ScheduledJob>());
        coalition.addScheduledJob(s);

        return s;
    }
}
