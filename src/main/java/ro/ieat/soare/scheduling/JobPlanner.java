package ro.ieat.soare.scheduling;

import ie.ucc.cl.sosm.components.SOSMComponent;
import ro.ieat.soare.clustering.ensemble.CoalitionPool;
import ro.ieat.soare.io.Util;
import ro.ieat.soso.core.coalitions.Coalition;
import ro.ieat.soso.core.coalitions.Resource;
import ro.ieat.soso.core.jobs.Job;
import ro.ieat.soso.core.jobs.ScheduledJob;
import ro.ieat.soso.core.jobs.TaskConstraint;
import ro.ieat.soso.core.jobs.TaskHistory;

import java.util.*;

/**
 * Created by adispataru on 05-Jul-16.
 */
public class JobPlanner {

    private long qCoalitions;
    private long qResources;
    private long sJobs;
    private long sTasks;

    public JobPlanner(){
        qCoalitions = 0;
        qResources = 0;
        sJobs = 0;
        sTasks = 0;
    }

    public void reset(){
        qCoalitions = 0;
        qResources = 0;
        sJobs = 0;
        sTasks = 0;
    }

    public void write(long time){
        Util.writeQueryStatus(time, qCoalitions, qResources, sJobs, sTasks);
    }

    public ScheduledJob scheduleJobUsingSize(Job job, CoalitionPool coalitionPool, boolean descend){

        sJobs++;
        sTasks += job.getTaskHistory().size();
        int jobSize = job.getTaskHistory().size();
        int coalitionSize = jobSize;
        int maxSize = coalitionPool.getMaxPoolSize();
        List<Coalition> coalitions = coalitionPool.getPoolOfSize(coalitionSize);
        while (coalitions == null && 0 <= coalitionSize && coalitionSize <= maxSize){
            if(descend)
                coalitionSize--;
            else
                coalitionSize++;
            coalitions = coalitionPool.getPoolOfSize(coalitionSize);
        }
        ScheduledJob s = null;
        for(Coalition coalition : coalitions) {
            qCoalitions++;
            List<Long> assigned = new ArrayList<>();
            List<Long> noiseAssigned = new ArrayList<>();
            Long[] taskResourceArray = new Long[job.getTaskHistory().size()];
            for(TaskHistory th : job.getTaskHistory().values()){
                Long mId = scheduleTask(th, coalition, assigned);
                if(mId > 0){
                    assigned.add(mId);
                    taskResourceArray[Math.toIntExact(th.getTaskIndex())] = mId;
                }else{
                    mId = scheduleTask(th, coalitionPool.getNoise(), noiseAssigned);
                    if(mId > 0){
                        noiseAssigned.add(mId);
                        taskResourceArray[Math.toIntExact(th.getTaskIndex())] = mId;
                    }else{
                        return null;
                    }
                }
            }
            if(assigned.size() + noiseAssigned.size() == jobSize){
                //job is successfully scheduled
                s = new ScheduledJob();
                s.setJobId(job.getJobId());
                Map<Long, Long> taskResourceMapping = new HashMap<>();
                Iterator<Resource> iterator = coalitionPool.getNoise().getResources().iterator();
                while(iterator.hasNext()){
                    Resource m = iterator.next();
                    if(noiseAssigned.contains(m.getId())) {
                        coalition.getResources().add(m);
                        iterator.remove();
                    }
                }

                for(int i = 0; i < taskResourceArray.length; i++){
                    taskResourceMapping.put((long) i, taskResourceArray[i]);
                }
                s.setTaskMachineMapping(taskResourceMapping);

                s.setCoalitionId(coalition.getId());
                break;

            }
        }

//        //remove coalition
//        if(s!= null){
//            Iterator<Coalition> iterator = coalitions.iterator();
//            while(iterator.hasNext()){
//                Coalition c = iterator.next();
//                if(c.getId() == s.getCoalitionId()){
//                    iterator.remove();
//                    break;
//                }
//            }
//        }
        return s;
    }

    public ScheduledJob scheduledJobByAttribute(Job job, CoalitionPool coalitionPool){

        List<Long> assigned = new ArrayList<>();
        List<Long> noiseAssigned = new ArrayList<>();
        Long[] taskResourceArray = new Long[job.getTaskHistory().size()];
        sJobs++;
        sTasks += job.getTaskHistory().size();
        for(TaskHistory t : job.getTaskHistory().values()){
            for (Coalition coalition : coalitionPool.getAll()){
                if(satisfies(coalition, t)){
                    qCoalitions++;
                    //dive in
                    Long mId = scheduleTask(t, coalition, assigned);
                    if(mId > 0){
                        assigned.add(mId);
                        taskResourceArray[Math.toIntExact(t.getTaskIndex())] = mId;
                    }else{
                        mId = scheduleTask(t, coalitionPool.getNoise(), noiseAssigned);
                        if(mId > 0){
                            noiseAssigned.add(mId);
                            taskResourceArray[Math.toIntExact(t.getTaskIndex())] = mId;
                        }else{
                            //TODO Do not return, but ask other coalitions for borrowing.
                            return null;
                        }
                    }
                }
            }
        }
        if(assigned.size() + noiseAssigned.size() == job.getTaskHistory().size()){
            //job is successfully scheduled
            ScheduledJob s = new ScheduledJob();
            s.setJobId(job.getJobId());
            Map<Long, Long> taskResourceMapping = new HashMap<>();
            for(int i = 0; i < taskResourceArray.length; i++){
                taskResourceMapping.put((long) i, taskResourceArray[i]);
            }
            s.setTaskMachineMapping(taskResourceMapping);
            return s;

        }
        return null;
    }

    private Long scheduleTask(TaskHistory th, Coalition coalition, List<Long> assigned) {
        Long result = -1L;
        for(Resource m : coalition.getResources()){
            if(assigned.contains(m.getId()))
                continue;
            qResources++;
            if(satisfies(m, th))
                return m.getId();
        }
        return result;
    }

    private boolean satisfies(Coalition coalition, TaskHistory th){
        for(TaskConstraint tc : th.getConstraints()){
            boolean s = false;
            for(String value: coalition.getAttributeValues().get(tc.getAttributeName())){
                if(satisfies(value, tc)) {
                    s = true;
                    break;
                }
            }
            if(!s)
                return false;

        }
        return true;
    }

    public static boolean componentSatisfies(SOSMComponent component, TaskHistory th){
        //preprocessing for constraints over the same attribute
        Map<String, List<TaskConstraint>> attributeConstraints = new HashMap<>();
        for(TaskConstraint tc : th.getConstraints()){
            attributeConstraints.putIfAbsent(tc.getAttributeName(), new ArrayList<>());
            attributeConstraints.get(tc.getAttributeName()).add(tc);
        }

        for(String name : attributeConstraints.keySet()){

            Map<String, List<String>> cummulativeAttributes = component.getCummulativeAttributes();
            if(cummulativeAttributes.get(name) == null) {
                System.out.println(component.getName() + " doesn't have attribute " + name);
                return false;
            }
            boolean satifiesAll = false;
            for(String value: cummulativeAttributes.get(name)){
                boolean satifies = true;
                for(TaskConstraint tc : attributeConstraints.get(name)) {
                    if (!satisfies(value, tc)) {
                        satifies = false;
                        break;
                    }
                }
                if(satifies)
                    satifiesAll = true;
            }
            if(!satifiesAll)
                return false;
        }
        return true;
    }

    private static boolean satisfies(String mValue, TaskConstraint tc){
        if(mValue == null){
            return false;
        }
        String value = tc.getAttributeValue();
        int op = tc.getComparisonOperator();
        switch (op){
            case 0:
                //equal ; return false if is not equal
                if(!mValue.equals(value))
                    return false;
                break;
            case 1:
                //not equal; return false if is equal
                if(mValue.equals(value))
                    return false;
                break;
            case 2:
                //less than; return false if geq
                if(Integer.parseInt(value) <= Integer.parseInt(mValue))
                    return false;
                break;
            case 3:
                //greater than
                if(Integer.parseInt(value) >= Integer.parseInt(mValue))
                    return false;
                break;
        }
        return true;
    }

    public static boolean satisfies(Resource m, TaskHistory th) {
        for(TaskConstraint tc : th.getConstraints()){
            String name = tc.getAttributeName();
            String mValue = m.getAttributes().get(name);
            if(!satisfies(mValue, tc))
                return false;
        }
        return true;
    }

}
