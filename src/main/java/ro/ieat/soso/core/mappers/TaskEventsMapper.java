package ro.ieat.soso.core.mappers;


import ro.ieat.soso.core.jobs.Job;
import ro.ieat.soso.core.jobs.TaskHistory;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by adrian on 18.11.2015.
 * This class has the sole purpose of reading the task_events files from google cluster data traces and map them on
 * the internal representation.
 */
public class TaskEventsMapper {

    public static void map(FileReader fileReader, Map<Long, Job> result, long startTime, long endTime) throws IOException, InterruptedException {

        BufferedReader br = new BufferedReader(fileReader);
        String line;
        while ((line = br.readLine()) != null) {
            String[] tokens = line.split(",");
            long timestamp = Long.parseLong(tokens[0]);
            long jobId = Long.parseLong(tokens[2]);
            long taskIndex = Long.parseLong(tokens[3]);
            long machineId = 0;
            double rcpu = 0;
            double rmem = 0;
            double rdisk = 0;
            boolean parallel = false;
            if(!tokens[4].equals(""))
                machineId = Long.parseLong(tokens[4]);
            int event = Integer.parseInt(tokens[5]);

            if(tokens.length > 10) {
                if (!tokens[9].equals(""))
                    rcpu = Double.parseDouble(tokens[9]);
                if (!tokens[10].equals(""))
                    rmem = Double.parseDouble(tokens[10]);
                if (!tokens[11].equals(""))
                    rdisk = Double.parseDouble(tokens[11]);
                if (!tokens[12].equals("true"))
                    parallel = true;
            }

            if(timestamp < startTime * 1000000)
                continue;
            if(timestamp > endTime * 1000000)
                return;

            String status = " ";
            long submitTime = 0;
            long scheduleTime = 0;
            long finishTime = 0;
            if(result.get(jobId).getTaskHistory() == null)
                result.get(jobId).setTaskHistory(new TreeMap<>());

            TaskHistory task = result.get(jobId).getTaskHistory().get(taskIndex);
            if(task == null) {
                task = new TaskHistory(taskIndex, submitTime, scheduleTime,
                        finishTime, status);
            }

            switch (event) {
                case 0:
                    task.setSubmitTime(timestamp);
                    task.setRequestedCPU(rcpu);
                    task.setRequestedMemory(rmem);
                    task.setRequestedDisk(rdisk);
                    task.setParallel(parallel);
                    break;
                case 1:
                    task.setScheduleTime(timestamp);
                    break;
                default:
                    task.setFinishTime(timestamp);
                    switch (event) {
                        case 2:
                            status = "evict";
                            break;
                        case 3:
                            status = "fail";
                            break;
                        case 4:
                            status = "finish";
                            break;
                        case 5:
                            status = "kill";
                            break;
                        case 6:
                            status = "lost";
                            break;
                        default:
                            status = "update";
                    }
            }

            task.setStatus(status);
            if(machineId != 0)
                task.setMachineId(machineId);

            result.get(jobId).getTaskHistory().put(task.getTaskIndex(), task);

        }
        br.close();
        fileReader.close();
    }
}
