#!/usr/bin/env bash
base=$(pwd)
mkdir "$base/data"
## declare an array variable
declare -a arr=("job_events" "task_events" "task_usage" "task_constraints")

## now loop through the above array
url="gs://clusterdata-2011-2"

mkdir "$base/data/machine_events"
if [ ! -e "$base/data/machine_events/part-00000-of-00001.csv" ]
then
  gsutil cp "$url/machine_events/part-00000-of-00001.csv.gz" "$base/data/machine_events/"
  cd "$base/data/machine_events"
  gzip -d *.gz
fi

if [ ! -e "$base/data/machine_attributes/part-00000-of-00001.csv" ]
then
  gsutil cp "$url/machine_attributes/part-00000-of-00001.csv.gz" "$base/data/machine_attributes/"
  cd "$base/data/machine_attributes"
  gzip -d *.gz
fi

cd $base

for folder in "${arr[@]}"
do
   mkdir "$base/data/$folder"
   for i in 0 1 2 3 4
   do
        filename="part-0000$i-of-00500.csv"
        if [ -e "$base/data/$folder/$filename" ]
        then
          echo "$filename exists."
        else
          gsutil cp "$url/$folder/${filename}.gz" "$base/data/$folder/"
        fi
   done
   cd "$base/data/$folder"
   gzip -d *.gz
   cd $base
   echo "Finished $folder."
done
